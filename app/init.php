<?php

$protocol = 'https://';
$host = $_SERVER['HTTP_HOST'];
define('HOST', $protocol . $host);
define('ROOT', $_SERVER['DOCUMENT_ROOT']);

require ROOT."/vendor/autoload.php";
require ROOT."/config.php";
require ROOT."/app/helpers/functions.php";
