<?php

namespace Scholarly;

class Teacher extends Employee {

	protected $period_id;

	public function __construct($empid = null) {
		parent::__construct();
		if ($empid) {
			$this->empid = $empid;
		}
	}

	public function isAvailableBetween($starts, $ends, $day) {
		$where = [
			'empid' => $this->empid,
			'day_of_week' => $day
		];

		$fetched_data = $this->db->getData('routine_map', $where);

		$time_span = array_fill(0,86400,0);

		foreach ($fetched_data as $data) {
		 	for ($i = $data['s_time']; $i <= $data['e_time']; $i++) {
		 		$time_span[$i] = 1; 
		 	}
		 } 

		 for ($i = $starts; $i < $ends; $i++) {
		 	if ($time_span[$i] == 1) {
		 		return false;
		 	}
		 }
		 return true;
	}

	public function alreadyRecordedFor($period_id) {
		$where = [
			'period_id' => $period_id,
			'day' => Date('N'),
			'date' => Date('dmY'),
			'empid' => $this->empid
		];
		if ($this->db->getCount('attendance_record', $where)) {
			return true;
		}
		return $this->db->getCount('attendance_record', $where);
	}

	public function recordAttendanceof($student_id, $status, $weightage) {
		$set = [
			'period_id' => $this->period_id,
			'day' => Date('N'),
			'date' => Date('dmY'),
			'empid' => $this->empid,
			'student_id' => $student_id,
			'status' => $status,
			'weightage' => $weightage
		];
		$this->db->insertData('attendance_record', $set);
		return true;
	}

	public function setPeriodForAttendance($period_id) {
		$this->period_id = $period_id;
	}
}