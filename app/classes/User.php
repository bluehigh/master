<?php

namespace Scholarly;

use Debojyoti\PdoConnect\Handler;

/*

	User class

*/

abstract class User
{
	public $uid;
	public $name;
	public $mobile;
	public $email;
	public $address;
	public $status;
	public $password;

	protected $user_table;

	protected $data;
	// protected $type;
	protected $db;

	public $counter=1;

	public function __construct() {

	}

	public function setDb($db) {
		$this->db = $db;
	}

	public function setData($id,$pass) {
		$this->uid = $id;
		$this->password = $pass;
	}

	public function register($email,$pass) {
		if($this->exists()) {
			return false;
		}
		$this->setData($email,$pass);
		$this->create();
	}

	protected function exists() {
		$this->all_data = $this->fetchData();
		if($this->all_data)
			return true;
	}

	//	Not in use
	protected function passMatches() {
		if($this->all_data['Password'] == $this->password)
			return true;
	}

	public function checkPass($password) {
		if ($this->password == $password) {
			return true;
		}
	}

	protected function fetchData() {
		$data = $this->db->getData($this->user_table,['User_ID'=>$this->uid]);
		if(count($data)) {
			return $data['0'];
		}
	}
	protected function assignProperties() {
		$this->uid = $this->all_data['uid'];
		$this->name = $this->all_data['name'];
		$this->mobile = $this->all_data['mobile'];
		$this->address = $this->all_data['address'];
		$this->email = $this->all_data['email'];
		$this->status = $this->all_data['status'];
		$this->pass = $this->all_data['password'];
	}

	protected function create() {
		$set = ['uid'=> $this->uid,
				'email'=>$_POST['Email'],
				'password'=>$_POST['Password'],
				'mobile'=>$_POST['Phone_Number'],
				'name'=>$_POST['Name'],
				'address'=>$_POST['Address'],
				'status'=>$_POST['Status']];
		$this->db->insert('user',$set);
	}

	public function submitOtp($otp){
		//	If all_data is not fetched previously
		if(!isset($this->all_data)) {
			//	If user does not exist, return false
			if(!$this->exists()) {
				return false;
			}
		}

		if($this->all_data["Otp"]==$otp) {
			return true;
		}
	}

	public function changepassword($new_password){
		if($this->db->updateData($this->user_table,["User_ID"=>$this->uid],["Password"=>$new_password])) {
			$this->clearOtp();
			return true;
		}
	}

	# Protected functions
	protected static function genOtp() {
 		//otp GENERATE
 		$string = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
 	    $string_shuffled = str_shuffle($string);
 	    return substr($string_shuffled, 1, 7);
	 }

	protected function setOtp($otp) {
		if($this->db->updateData($this->user_table,["User_ID"=>$this->uid],["Otp"=>$otp])) {
				return true;
		}
	}

	protected function sendOtpToMail($otp) {
		$to      = $this->all_data['Email'];
		$subject = 'the OTP';
		$message = $otp;
		$headers = 'From: webmaster@example.com' . "\r\n" .
	    'Reply-To: webmaster@example.com' . "\r\n" .
	    'X-Mailer: PHP/' . phpversion();

	    // mail() will not work in wamp/xamp/lamp
		// mail($to, $subject, $message, $headers);
		return true;
	}

	protected function clearOtp() {
		if($this->db->updateData($this->user_table,['User_ID'=>$this->uid],["Otp"=>null])) {
			return true;
		}
	}

	# Public abstract functions
	abstract public function login($password);
	abstract public function setUID();

	abstract protected function resetPassword($email);
 }
