<?php

namespace Scholarly;

class Semester {
	private $sem_id;
	private $sem_no;
	private $data;

	private static $db;

	public function __construct($sem_id = null)  {
	    if ($sem_id) {
	        $this->sem_id = $sem_id;
	    }
	}

	public function fetchDetails() {
	    $where = ['sem_id'=>$this->sem_id];
	    $sem = self::$db->getData('sem_map', $where);
	    if(count($sem)) {
	        $this->data = $sem[0];
	        $this->sem_no = $this->data['sem_no'];
	        return true;
	    }
	}

	public function getSubIds($sub_code = null) {
		if ($sub_code) {
		    $where = [
		        'sem_id' => $this->sem_id,
		        'sub_code' => $sub_code
		    ];
		} else {
		    $where = [
		        'sem_id' => $this->sem_id
		    ];
		}
		$fetched_sub_ids = self::$db->getData('subject_map', $where);
		// Format array properly
		$formatted_array = [];
		foreach ($fetched_sub_ids as $key => $value) {
		    $formatted_array[] = $value['sub_id']; 
		}
		return $formatted_array;
	}

	public function getCourseCode() {
		
		// Get current course_id
		$where = ['sem_id' => $this->sem_id];
		$get = ['course_id'];
		$current_course_id = self::$db->getData('sem_map', $where, $get)[0]['course_id'];
		
		//	Then Get current course_code
		$where = ['course_id' => $current_course_id];
		$get = ['course_code'];
		$current_course_code = self::$db->getData('course_map', $where, $get)[0]['course_code'];

		return $current_course_code;
	}

	public function subExists($sub) {

		//	Check by sub name
		$where = [
			'sub_name' => $sub->sub_name,
			'sem_id' => $this->sem_id,
			'sub_type' => $sub->sub_type
		];
		if (self::$db->getCount('subject_map', $where)) {
			return true;
		}
	}

	public function editSub($sub) {
	    $set = [
	        'sub_name' => $sub->sub_name,
	        'points' => $sub->points
	    ];
	    $where = [
	        'sub_id' => $sub->sub_id
	    ];
	    self::$db->updateData('subject_map',  $where, $set);
	    return true;
	}

	public function addSub($sub) {
		$set = [
			'sub_name' => $sub->sub_name,
			'sub_code' => $sub->sub_code,
			'points' => $sub->points,
			'sub_type' => $sub->sub_type,
			'status' => $sub->status,
			'sem_id' => $this->sem_id
		];
		self::$db->insertData('subject_map', $set);
		return true;
	}

	public function getNo() {
	    return $this->sem_no;
	}

	public function getId() {
	    return $this->sem_id;
	}

	public function setNo($sem_no) {
		$this->sem_no = $sem_no;
	}

	public function getCourseId() {
		return $this->data['course_id'];
	}

	public static function setDb($db) {
	    self::$db = $db;
	}
}
