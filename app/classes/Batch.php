<?php

namespace Scholarly;

class Batch {
	public $batch_id;
	public $starting_year;
	public $ending_year;

	private static $db;

	public function __construct($batch_id = null)  {
	    if ($batch_id) {
	        $this->batch_id = $batch_id;
	    }
	}

	public function fetchDetails() {
	    $where = ['batch_id'=>$this->batch_id];
	    $batch = self::$db->getData('batch_map', $where);
	    if(count($batch)) {
	        $this->data = $batch[0];
	        $this->batch_id = $this->data['batch_id'];
	        $this->starting_year = $this->data['starting_year'];
	        $this->ending_year = $this->data['ending_year'];
	        return true;
	    }
	}

	public function getAllStudentIds($student_id = null) {
		if ($student_id) {
		    $where = [
		        'batch_id' => $this->batch_id,
		        'student_id' => $student_id
		    ];
		} else {
		    $where = [
		        'batch_id' => $this->batch_id
		    ];
		}
		$fetched_sub_ids = self::$db->getData('students', $where);
		// Format array properly
		$formatted_array = [];
		foreach ($fetched_sub_ids as $key => $value) {
		    $formatted_array[] = $value['student_id']; 
		}
		return $formatted_array;
	}

	public function addStudent($student) {
		$string = '0123456789';
 	    $string_shuffled = str_shuffle($string);
 	    $password = substr($string_shuffled, 1, 7); 
		$set = [
			'student_id' => $student->data['student_id'],
			'name' => $student->data['name'],
			'u_roll' => $student->data['u_roll'],
			'u_reg' => $student->data['u_reg'],
			'email' => $student->data['email'],
			'phone' => $student->data['phone'],
			'address' => $student->data['address'],
			'status' => $student->data['status'],
			'course_id' => $student->data['course_id'],
			'batch_id' => $student->data['batch_id'],
			'starting_year' => $student->data['starting_year'],
			'password' => $password
		];
		self::$db->insertData('students', $set);
		return true;
	}

	public function editStudent($student) {
		$where = [
			'student_id' => $student->data['student_id']
		];
		$set = [
			'name' => $student->data['name'],
			'email' => $student->data['email'],
			'phone' => $student->data['phone'],
			'address' => $student->data['address'],
			'status' => $student->data['status']
		];
		self::$db->updateData('students', $where, $set);
		return true;
	}

	public function getCourseId() {
		$where = [ 'batch_id' => $this->batch_id];
		return self::$db->getData('batch_map', $where)[0]['course_id'];
	}

	/*public function subExists($sub) {

		//	Check by sub name
		$where = [
			'sub_name' => $sub->sub_name,
			'sem_id' => $this->sem_id
		];
		if (self::$db->getCount('subject_map', $where)) {
			return true;
		}
	}*/

	/*public function editSub($sub) {
	    $set = [
	        'sub_name' => $sub->sub_name,
	        'points' => $sub->points
	    ];
	    $where = [
	        'sub_id' => $sub->sub_id
	    ];
	    self::$db->updateData('subject_map',  $where, $set);
	    return true;
	}*/

	/*public function addSub($sub) {
		$set = [
			'sub_name' => $sub->sub_name,
			'sub_code' => $sub->sub_code,
			'points' => $sub->points,
			'sub_type' => $sub->sub_type,
			'status' => $sub->status,
			'sem_id' => $this->sem_id
		];
		self::$db->insertData('subject_map', $set);
		return true;
	}*/

	public static function setDb($db) {
	    self::$db = $db;
	}
}