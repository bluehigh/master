<?php

namespace Scholarly;

class Course 
{
	private $course_id;
	private $data;
	private $name;
	private $code;
	private $span;
	private $status;

	private static $db;

	public function __construct($course_id = null) {
		if ($course_id) {
			$this->course_id = $course_id;
		}
	}

	public function fetchDetails() {
		$where = [
			'course_id' => $this->course_id
		];
		$this->data = self::$db->getData('course_map', $where)[0];
		$this->name = $this->data['course_name'];
		$this->code = $this->data['course_code'];
		$this->span = $this->data['course_span'];
		$this->status = $this->data['status'];
		return true;
	}

	public function getSemIds($sem_id = null) {
		if ($sem_id) {
		    $where = [
		        'course_id' => $this->course_id,
		        'sem_id' => $sem_id
		    ];
		} else {
		    $where = [
		        'course_id' => $this->course_id
		    ];
		}
		$fetched_sem_ids = self::$db->getData('sem_map', $where);
		// Format array properly
		$formatted_array = [];
		foreach ($fetched_sem_ids as $key => $value) {
		    $formatted_array[] = $value['sem_id']; 
		}
		return $formatted_array;
	}

	public function addSem($sem) {
		$set = [
			'course_id' => $this->course_id,
			'sem_no' => $sem->getNo()
		];
		self::$db->insertData('sem_map', $set);
		return true;
	}

	public function getBatchIds($starting_year = null) {
		if ($starting_year) {
		    $where = [
		        'course_id' => $this->course_id,
		        'starting_year' => $starting_year
		    ];
		} else {
		    $where = [
		        'course_id' => $this->course_id
		    ];
		}
		$fetched_sem_ids = self::$db->getData('batch_map', $where);
		// Format array properly
		$formatted_array = [];
		foreach ($fetched_sem_ids as $key => $value) {
		    $formatted_array[] = $value['batch_id']; 
		}
		return $formatted_array;
	}

	public function batchExists($batch) {
		$where = [
			'starting_year' => $batch->starting_year,
			'course_id' => $this->course_id
		];
		if(self::$db->getCount('batch_map', $where)) {
			return true;
		}
	}

	public function addBatch($batch) {
		$set = [
			'starting_year' => $batch->starting_year,
			'ending_year' => $batch->ending_year,
			'course_id' => $this->course_id
		];
		if(self::$db->insertData('batch_map', $set)) {
			return true;
		}
	}

	public function getAllEmpIds() {

	    $where = [
	        'course_id' => $this->course_id,
	    ];
		$fetched_emp_ids = self::$db->getData('teacher_course_map', $where);
		// Format array properly
		$formatted_array = [];
		foreach ($fetched_emp_ids as $key => $value) {
		    $formatted_array[] = $value['empid']; 
		}
		return $formatted_array;
	}

	public function addEmp($employee) {
		$set = [
			'course_id' => $this->course_id,
			'empid' => $employee->empid
		];
		self::$db->insertData('teacher_course_map', $set);
		return true;
	}

	public function removeEmp($employee) {
		$where = [
			'course_id' => $this->course_id,
			'empid' => $employee->empid
		];
		self::$db->deleteData('teacher_course_map', $where);
		return true;
	}

	public function hasHod() {
		$where = [
			'course_id' => $this->course_id,
		];
		$hod_id = self::$db->getData('course_map', $where)[0]['hod'];
		if($hod_id) {
			return true;
		}
	}

	public function getHodId() {
		$where = [
			'course_id' => $this->course_id,
		];
		$hod_id = self::$db->getData('course_map', $where)[0]['hod'];
		return $hod_id;
	}

	public function addHod($hod_id) {
		$where = [
			'course_id' => $this->course_id
		];
		$set = [
			'hod' => $hod_id
		];
		self::$db->updateData('course_map', $where, $set);
		return true;
	}

	public function updateHod($hod_id) {
		$where = [
			'course_id' => $this->course_id
		];
		$set = [
			'hod' => $hod_id
		];
		self::$db->updateData('course_map', $where, $set);
		return true;
	}

	public function countActiveStudents() {
		$where = [
			'course_id' => $this->course_id
		];
		return self::$db->getCount('students', $where);
	}

	public function getCurrentSemFor($batch) {
		$starting_year = $batch->starting_year;
		$current_year = date('Y'); // ex : 2018
		$current_month = date('m'); // ex : 7
		$current_sem = ($current_year - $starting_year)*2;
		if ($current_month > 6) {
			$current_sem++;
		}
		$where = [
			'course_id' => $this->course_id
		];
		$fetched_sem_id = self::$db->getData('sem_map', $where)[$current_sem-1]['sem_id'];
		return $fetched_sem_id;
	}

	public function getBatchIdFor($sem) {
		//	Calculate current_sem_sl_no
		$current_sem_sl_no = $this->getSemSlNo($sem->getId());

		// Calculate starting year
		$current_year = Date('Y');

		if ($current_sem_sl_no <= $this->span ) {
			
			if ($current_sem_sl_no%2) {
				// odd
				$year_to_subtract = ($current_sem_sl_no-1)/2;
			} else {
				// even
				$year_to_subtract = $current_sem_sl_no/2;
			}

			$starting_year = $current_year - $year_to_subtract;
			$where = [
				'course_id' => $this->course_id,
				'starting_year' => $starting_year
			];
			return self::$db->getData('batch_map', $where)[0]['batch_id'];
		}
	}

	public function getSemSlNo($sem_id) {
		$where = [
			'course_id' => $this->course_id,
		];

		$all_sem = self::$db->getData('sem_map', $where);

		$count = 1;

		foreach ($all_sem as $sem) {
			if ($sem['sem_id'] == $sem_id) {
				return $count;
			}
			$count++;
		}
	}

	public function getDeptId() {
		$where = [
			'course_id' => $this->course_id
		];
		return self::$db->getData('course_map', $where)[0]['dept_id'];
	}

	public function getName() {
		return $this->name;
	}

	public function getCode() {
		return $this->code;
	}

	public function getSpan() {
		return $this->span;
	}

	public function getId() {
		return $this->course_id;
	}

	public function getStatus() {
		return $this->status;
	}

	public function setName($name) {
		$this->name = $name;
		return true;
	}

	public function setCode($code) {
		$this->code = $code;
		return true;
	}

	public function setSpan($span) {
		$this->span = $span;
		return true;
	}

	public function setStatus($status) {
		$this->status = $status;
		return true;
	}

	public static function setDb($db) {
		self::$db = $db;
		return true;
	}	
}