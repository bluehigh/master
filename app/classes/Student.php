<?php
	
namespace Scholarly;

use Scholarly\User;
	
class Student extends User
{
	// public $details;
	public $YOP = 2018; 
	// public static $YOP = 20*100+date('y') + 4; 
	public $array;

	public $data;

	public function __construct($college_id=null) {
		if($college_id) {
			$this->uid = $college_id;
		}
		$this->user_table = 'students';
		parent::__construct();
	}

	public function login($password) {
		if ($password == $this->data['password']) {
			return true;
		}
	}


	public function fetchDetails() {
		$where = [
			'student_id' => $this->uid
		];
		$this->data = $this->db->getData('students', $where);
		if (count($this->data)) {
			$this->data = $this->data[0];
			return true;
		}
	}

	public function genIdFor($institute, $batch) {
		$student_id = $institute->getId().$batch->batch_id.(count($batch->getAllStudentIds())+1);
		$this->uid = $student_id;
		return $student_id;
	}

	//set UID of students

	public function setUID() {
		if($_POST['Status']=='Student' || $_POST['Status']=='student') {
			if($_POST['Dept']=='CSE') {
				$this->uid= $YOP.'1'.getJSON_data('1');
			} else if($_POST['Dept']=='ECE') {
				$this->uid= $YOP.'2'.getJSON_data('2');
			} else {
				$this->uid= $YOP.'3'.getJSON_data('3');
			}
		}
	}

	public function resetPassword($email) {
		if($this->exists()) {
			if($this->all_data['Email']!=$email) {
				return false;
			}
		    $otp = self::genOtp();
		    $this->setOtp($otp);		//set otp in database

		    if($this->sendOtpToMail($otp)) {
		    	return true;
		    }
		}
		//TODO limit otp
	}

	public function submitOtp($otp) {
		if($this->exists()) {
			if($this->all_data['Otp']==$otp) {
				return true;
			}
		}
	}
	
	public function getBatchId() {
		return $this->data['batch_id'];
	}

	public function exists() {
		if (count($this->data)) {
			return true;
		}
	}

	public function getName() {
		return $this->data['name'];
	}	

	public function activeBooksData() {
		$where = [
			'student_id' => $this->uid,
			'return_date' => null
		];
		return $this->db->getData('manage_books', $where);
	}

}