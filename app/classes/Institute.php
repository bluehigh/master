<?php

namespace Scholarly;

class Institute {

	private $db;
	private $id;

	public function __construct($id = null) {
        $this->id = $id;
	}
	# Inject Db dependency
	public function setDb($db) {
		$this->db = $db;
	}
	# Add new Institute
	public function add($data) {
		$this->id = $data['unique_id'] = $this->generateId($data);
		return $this->db->insertData('institution_general',$data);
	}
	# Set otp and send to mail (type = 1/2 = 1st/normal)
	public function sendVerificationCode($type) {
		switch ($type) {
			case 1:		// For first time
				// set verification code
				$otp = $this->setVerificationCode();
				$msg = "OTP : ".$otp;
				// Send mail
				self::sendEmail($this->data['email'],$msg);
				break;
		}
	}
    public function set($param) {
        // Process id
        if (is_numeric($param)) {
            $this->id = $param;
            $this->fetchDetails();
        } else {
            $this->code = $param;
            $this->fetchIdFromCode();
        }
    }

    public function checkPass($password) {
        if ($this->data['password'] == $password) {
            return true;
        }
    }
	# Return institution id
	public function getId() {
		return $this->id;	
	}
	# Check if email id exists in general db
	public function emailExists($email) {
		if ($this->db->getCount('institution_general',["email"=>$email])) {
			return true;
		}
	}
	# Check if registration id exists in general db
	public function registrationIdExists($registration_id) {
		if ($this->db->getCount('institution_general',["registration_id"=>$registration_id])) {
			return true;
		}
	}
	# Check if otp cell is null or not
	public function otpRequested() {
		if (!isset($this->data)) {
			$this->fetchDetails();
		}
		if($this->data['otp']) {
			return true;
		}
	}
	# Get some specific fields (params = [field1,field2] or 'field')
	public function get($fields) {
		$allowed_fields = ['institute_name','university_name','email','registration_id','code'];
		if (!isset($this->data)) {
			$this->fetchDetails();
		}
		$response = [];
		if(is_array($fields)) {
			if(array_diff($fields, $allowed_fields)) {
				foreach ($fields as $key => $value) {
					$response[$value] = $this->data[$value];
				}
			}
		} elseif (is_string($fields)) {
			if(in_array($fields, $allowed_fields)) {
				$response[$fields] = $this->data[$fields];
			}
		}
		return $response;
	}
	# Check if unique id exists
	public function exists() {
        if (isset($this->data)) {
            return true;
        }
		return $this->db->getCount('institution_general',['unique_id'=>$this->id]);
	}
	# Check if otp is proper
	public function verifyOtp($otp) {
		if (!isset($this->data)) {
			$this->fetchDetails();
		}
		if($otp == $this->data['otp']) {
			return true;
		}
	}
	# Set new password
	public function setPassword($new_password, $old_password = null) {
		$this->clearOtp();
		if($old_password) {
			if (!isset($this->data)) {
				$this->fetchDetails();
			}
			if ($old_password != $this->data['password']) {
				return false;
			}
		}
		$this->db->updateData('institution_general',['unique_id'=>$this->id],['password'=>$new_password]);
		return true;
	}
	# Login institute with password
	public function login($password) {
		if(isset($this->id)) {
			if (!isset($this->data)) {
				$this->fetchDetails();
			}
			if($password == $this->data['password']) {
				return true;
			}
		}
	}

    public function setCode($code) {
        if (!$this->codeExists()) {
            if ($this->codeIsUnique($code)) {
                $where = ['unique_id'=>$this->id];
                $set = ['code'=>$code];
                $this->db->updateData('institution_general', $where, $set);
                return true;
            }
        }
    }

    public function codeExists() {
        if ($this->data['code']) {
            return true;
        }
    }

    public function codeIsUnique($code) {
        $where = ['code'=>$code];
        if ($this->db->getCount('institution_general', $where)) {
            return false;
        } else {
            return true;
        }
    }

    public function employeeExists($employee) {
        $where = [
            'unique_id' => $this->id,
            'email' => $employee->email
        ];
        if ($this->db->getCount('emp_data', $where)) {
            return true;
        }

    }

    public function addEmployee($employee){
        $default_pass = md5(uniqid(rand(), true));
		$set=[
    		'empid'=>$employee->empid,
    		'unique_id' =>$this->id,
    		'name'=>$employee->name,
    		'email'=>$employee->email,
    		'dept_id'=>$employee->dept_id,
            'desig_id'=>$employee->desig_id,
            'emp_type'=>$employee->type,
    		'phone'=>$employee->phone,
    		'address'=>$employee->address,
            'password'=>$default_pass
    	];
		if($employee->insertIn2table('emp_data',$set)){
			$to = $employee->email;
			$subject = "My subject";
			$txt = "Hi,
			You've been registered as a employee .
			This is the generated password ".$default_pass."
			Please change your password after logging in.
			Thank You.
			";
			$headers = "From: webmaster@example.com";
			$employee->sendEmail($to,$subject,$txt,$headers);
			return true;
		}
    }

    public function getEmployeeIds($email = null){
    	if($email){
    		$where = [
    			'unique_id' => $this->id,
    			'email' => $email
    		];
    	}else{
    		$where = ['unique_id' => $this->id];
    	}
    	$get = ['empid'];
    	$emp_ids = $this->db->getData('emp_data',$where,$get);

    	$formatted_array = [];
    	foreach ($emp_ids as $key => $value) {
    		$formatted_array[]=$value['empid'];
    	}
    	return $formatted_array;
    }

    public function editEmployee($employee) {
        $where = [
            'unique_id' => $this->id,
            'empid' => $employee->empid
        ];
        $set = [
            'name'=>$employee->name,
            'email'=>$employee->email,
            'dept_id'=>$employee->dept_id,
            'desig_id'=>$employee->desig_id,
            'emp_type'=>$employee->type,
            'phone'=>$employee->phone,
            'address'=>$employee->address
        ];
        $this->db->updateData('emp_data', $where, $set);

        return true;
    }

    # Returns all designations ['desig_id'=>'desig']
    public function getAllDesignations() {
        $desigs = $this->db->getData('emp_legend');
        // Format array
        $formatted_array = [];
        foreach ($desigs as $desig) {
            $formatted_array[$desig['desig_id']] = $desig['desig']; 
        }
        return $formatted_array;
    }

    public function deRegister($employee) {
        $where = [
            'unique_id' => $this->id,
            'empid' => $employee->empid
        ];
        $set = [
            'status' => '0'
        ];

        $this->db->updateData('emp_data', $where, $set);
        return true;
    }

    public function getAllTeacherIds() {
        $where = [
            'unique_id' => $this->id,
            'emp_type' => 'T'
        ];
        $emp_ids = $this->db->getData('emp_data',$where);

        $formatted_array = [];
        foreach ($emp_ids as $key => $value) {
            $formatted_array[]=$value['empid'];
        }
        return $formatted_array;
    }

    public function getPrincipleId() {

        $where = [
            'unique_id' => $this->id,
            'desig_id' => '1'
        ];
        $data = $this->db->getData('emp_data', $where);

        if (count($data)) {
            return $data[0]['empid'];
        }

    }

    # If dept_id is passed, then it will return data of only that dept
    public function getDeptIds($dept_code = null) {
    	if ($dept_code) {
    		$where = [
    			'unique_id' => $this->id,
    			'dept_code' => $dept_code
    		];
    	} else {
    		$where = ['unique_id' => $this->id];
    	}
    	$get = ['dept_id'];
    	$dept_ids = $this->db->getData('dept_map', $where, $get);
    	// Format array to return
    	$formatted_array = [];
    	foreach ($dept_ids as $key => $value) {
    		$formatted_array[] = $value['dept_id']; 
    	}
    	return $formatted_array;
    }

    public function deptExists($dept) {
    	$where = [
    		'unique_id'=>$this->id,
    		'dept_code'=>$dept->getCode()
    	];
    	if($this->db->getCount('dept_map', $where)) {
    		return true;
    	}
    }

    public function addDept($dept) {
    	$set = [
    		'unique_id' => $this->id,
    		'dept_code' => $dept->getCode(),
    		'dept_name' => $dept->getName()
    	];
    	if($this->db->insertData('dept_map', $set)) {
    		return true;
    	}
    }

    public function editDept($dept) {
    	$where = [
    		'dept_id' => $dept->getId()
    	];
    	$set = [
    		'dept_code' => $dept->getCode(),
    		'dept_name' => $dept->getName()
    	];
    	if ($this->db->updateData('dept_map', $where, $set)) {
    		return true;
    	}
    }

    public function deleteDept($dept) {
    	$where = [
    		'dept_id' => $dept->getId()
    	];
    	if ($this->db->deleteData('dept_map', $where)) {
    		return true;
    	}
    }

    public function hasCode() {
    	if ($this->data['code']) {
    		return true;
    	}
    }

    public function updatePrinciple($new_emp_id) {
        # Remove previous principle
        $where = [
            'desig_id' => '1',
            'unique_id' => $this->id
        ];
        $set = [
            'desig_id' => '3'
        ];
        $this->db->updateData('emp_data', $where, $set);

        # Add new principle
        $where = [
            'empid' => $new_emp_id,
            'unique_id' => $this->id
        ];
        $set = [
            'desig_id' => '1'
        ];
        if ($this->db->updateData('emp_data', $where, $set)) {
            return true;
        }
    }

    public function principleExists() {
        $where = [
            'desig_id' => '1',
            'unique_id' => $this->id
        ];
        if ($this->db->getCount('emp_data', $where)) {
            return true;
        }
    }

    public function addPrinciple($new_emp_id) {
        $where = [
            'empid' => $new_emp_id,
            'unique_id' => $this->id
        ];
        $set = [
            'desig_id' => '1'
        ];
        if ($this->db->updateData('emp_data', $where, $set)) {
            return true;
        }
    }

	# Set otp = null
	private function clearOtp() {
		if($this->otpRequested()) {
			$this->db->updateData('institution_general',['unique_id'=>$this->id],['otp'=>null]);
			return true;
		}
	}
	# Set new otp
	private function setVerificationCode() {
		if (!isset($this->data)) {
			$this->fetchDetails();
		}
		$otp = self::genOtp();
		$this->db->updateData('institution_general',['unique_id'=>$this->id],['otp'=>$otp]);
		return $otp;
	}
	# Generate unique id while adding new institute
	private function generateId($data) {
		$sl_no = $this->db->getCount('institution_general')+1;
		$plan = $data['plan'];
		$year = substr(date("Y"), 2);
		return $plan."0".$sl_no;
	}
	# Fetch all details from general db
	public function fetchDetails() {
		$this->data = $this->db->getData('institution_general',['unique_id'=>$this->id])[0];
	}
	# Generate otp code
	private static function genOtp() {
 		//otp GENERATE
 		$string = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
 	    $string_shuffled = str_shuffle($string);
 	    return substr($string_shuffled, 1, 7);
	 }
	# Send msg to email
 	private static function sendEmail($email,$msg) {
	 	$to      = $email;
	 	$subject = 'the OTP';
	 	$message = $msg;
	 	$headers = 'From: webmaster@example.com' . "\r\n" .
	     'Reply-To: webmaster@example.com' . "\r\n" .
	     'X-Mailer: PHP/' . phpversion();

	     // mail() will not work in wamp/xamp/lamp
	 	// mail($to, $subject, $message, $headers);
	 	return true;
	}

    private function fetchIdFromCode() {
        $fetched_data = $this->db->getData('institution_general',['code'=>$this->code]);
        if ($fetched_data && count($fetched_data)) {
            $this->id = $fetched_data[0]['unique_id'];
            $this->data = $fetched_data[0];
            return true; 
        } 
    }


}