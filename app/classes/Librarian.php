<?php

namespace Scholarly;

class Librarian extends Employee {
	public function __construct($empid = null) {
		parent::__construct();
		if ($empid) {
			$this->empid = $empid;
		}
	}

	public function getBookIds(){
		$sess = $_SESSION['unique_id'];
		$where = ['unique_id' => $sess];

		$get = ['bookid'];
		$book_ids = $this->db->getData('books',$where,$get);

		$formatted_array = [];
		foreach ($book_ids as $key => $value) {
			$formatted_array[]=$value['bookid'];
		}
		return $formatted_array;
	}

	public function addBooks($book){
		$fields = [
			'bookid' => $this->genBookId($book),
			'unique_id' => $_SESSION['unique_id'],
			'name' => $book->name,
			'author' => $book->author,
			'category' =>$book->category,
			'tags' =>$book->tags,
		];

		if($this->insertIn2table('books',$fields)){
			return true;
		}

  }

	public function genBookId($book) {
		$book->bookid = time()+rand(10,1000);
		return $book->bookid;
	}

	public function manageBooks(){

	}

	public function insertIn2table($tname,$fields){
		if($this->db->insertData($tname,$fields)){
			return true;
		}
	}

	public function removeBook($book) {
		$where = [
			'bookid' => $book->bookid
		];
		$this->db->deleteData('books', $where);
		return true;
	}

	public function assignBook($student_id, $book_id) {
		$set = [
			'student_id' => $student_id,
			'bookid' => $book_id,
			'unique_id' => $_SESSION['unique_id'],
			'issue_date' => time(),
			'expected_return_date' => time() + 15638400
		];
		$this->db->insertData('manage_books', $set);
		return true;
	}

	public function unassign($bookid,$fine_amount) {
		$where = [
			'bookid' => $bookid,
			'return_date'=> null,
		];
		$set = [
			'return_date' => time(),
			'fine' => $fine_amount
		];
		$this->db->updateData('manage_books', $where, $set);
		return true;
	}

	public function viewBookTransactions($student_id){
		$where = [
			'student_id' => $student_id,
			'unique_id' => $_SESSION['unique_id'],
			'return_date'=> null
		];

		$bookTransactions = $this->db->getData('manage_books',$where);
		if(count($bookTransactions)){
			return $bookTransactions;
		} 
		return [];
	}
}
