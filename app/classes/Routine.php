<?php

namespace Scholarly;

class Routine 
{
	public $sem_id;
	public $sub_id;
	public $empid;
	public $day_of_week;
	public $s_time;
	public $e_time;
	public $data;

	private static $db;

	public function __construct() {

	}

	public function setSem($sem) {
		$this->sem_id = $sem->getId();
	}

	public function setDay($day) {
		$this->day_of_week = $day;
	}

	public function fetchDetails() {
		$where = [
			'sem_id' => $this->sem_id,
			'day_of_week' => $this->day_of_week
		];
		$fetched_data = self::$db->getData('routine_map', $where);
		
		$formatted_array = [];

		if (count($fetched_data)) {
			foreach ($fetched_data as $data) {
				$formatted_array[$data['s_time']] = $data;
			}
		}
		$this->data = $formatted_array;
		return $this->data;
	}

	public function addPeriod($data) {
		$set = [
			'sem_id' => $this->sem_id,
			'day_of_week' => $this->day_of_week,
			'empid' => $data['empid'],
			's_time' => $data['starts'],
			'e_time' => $data['ends'],
			'sub_id' => $data['sub_id']
		];
		self::$db->insertData('routine_map', $set);
		$where = $set;
		return self::$db->getData('routine_map', $where)[0]['period_id'];
	}

	public function fetchPeriod($period_id) {
		$where = [
			'period_id' => $period_id
		];
		return self::$db->getData('routine_map', $where)[0];
	}

	public function setSpan($starts, $ends) {
		$this->s_time = $starts;
		$this->e_time = $ends;
	}

	public function updatePeriod($data) {
		$where = [
			's_time' => $this->s_time,
			'e_time' => $this->e_time,
			'day_of_week' => $this->day_of_week,
			'sem_id' => $this->sem_id
		];
		$set = [
			'empid' => $data['empid'],
			'sub_id' => $data['sub_id']
		];
		self::$db->updateData('routine_map', $where, $set);
		$where = $set;
		return self::$db->getData('routine_map', $where)[0]['period_id'];
	}

	public function getTeacherId() {
		$where = [
			's_time' => $this->s_time,
			'e_time' => $this->e_time,
			'day_of_week' => $this->day_of_week,
			'sem_id' => $this->sem_id
		];
		return self::$db->getData('routine_map', $where)[0]['empid'];
	}

	public function clearAllPeriods() {
		$where = [
			'day_of_week' => $this->day_of_week,
			'sem_id' => $this->sem_id
		];
		self::$db->deleteData('routine_map', $where);
	}

	public function removePeriod() {
		$where = [
			's_time' => $this->s_time,
			'e_time' => $this->e_time,
			'day_of_week' => $this->day_of_week,
			'sem_id' => $this->sem_id
		];
		self::$db->deleteData('routine_map', $where);
	}

	public function getPeriodsOf($teacher) {
		$where = [
			'day_of_week' => $this->day_of_week,
			'empid' => $teacher->empid
		];
		return self::$db->getData('routine_map', $where);
	}

	public static function setDb($db) {
		self::$db = $db;
	}
}