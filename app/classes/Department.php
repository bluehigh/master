<?php

namespace Scholarly;

class Department {
    private $name;
    private $dept_id;
    private $code;

    private static $db;
    private $data;

    public function __construct($dept_id = null)  {
        if ($dept_id) {
            $this->dept_id = $dept_id;
        }
    }

    public function fetchDetails() {
        $where = ['dept_id'=>$this->dept_id];
        $dept = self::$db->getData('dept_map', $where);
        if(count($dept)) {
            $this->data = $dept[0];
            $this->name = $this->data['dept_name'];
            $this->code = $this->data['dept_code'];
            return true;
        }
    }

    public function getCourseIds($course_code = null) {
        if ($course_code) {
            $where = [
                'dept_id' => $this->dept_id,
                'course_code' => $course_code
            ];
        } else {
            $where = [
                'dept_id' => $this->dept_id
            ];
        }
        $fetched_course_ids = self::$db->getData('course_map', $where);
        // Format array properly
        $formatted_array = [];
        foreach ($fetched_course_ids as $key => $value) {
            $formatted_array[] = $value['course_id']; 
        }
        return $formatted_array;
    }

    public function addCourse($course) {
        $set = [
            'course_name' => $course->getName(),
            'course_code' => $course->getCode(),
            'course_span' => $course->getSpan(),
            'dept_id' => $this->dept_id
        ];
        self::$db->insertData('course_map', $set);
        return true;
    }

    public function editCourse($course) {
        $set = [
            'course_name' => $course->getName(),
            'course_code' => $course->getCode(),
            'course_span' => $course->getSpan(),
            'status' => $course->getStatus(),
            'dept_id' => $this->dept_id
        ];
        $where = [
            'course_id' => $course->getId()
        ];
        self::$db->updateData('course_map',  $where, $set);
        return true;
    }

    public function courseExists($course) {
        $where = [
            'course_code' => $course->getCode(),
            'dept_id' => $this->dept_id
        ];
        if (self::$db->getCount('course_map', $where)) {
            return true;
        }
    }

    public function getInstituteId() {
		$where = [
			'dept_id' => $this->dept_id
		];
		return self::$db->getData('dept_map', $where)[0]['unique_id'];
	}

    public function setName($name) {
        $this->name = $name;
    }

    public function setCode($code) {
        $this->code = $code;
    }

    public function getName() {
        return $this->name;
    }

    public function getId() {
        return $this->dept_id;
    }

    public function getCode() {
        return $this->code;
    }

    public static function setDb($db) {
        self::$db = $db;
    }

}