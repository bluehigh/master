<?php

namespace Scholarly;

class HOD extends Teacher {

	public function __construct($empid = null) {
		parent::__construct();
		if ($empid) {
			$this->empid = $empid;
		}
	}

	public function getAssignedCourseIds() {
		$where = [
			'hod' => $this->empid
		];
		$get = [
			'course_id'
		];
		$course_ids = $this->db->getData('course_map', $where, $get);
		$formatted_array = [];
		foreach ($course_ids as $course_id) {
			$formatted_array[] = $course_id['course_id'];
		}
		return $formatted_array;
	}
	
}