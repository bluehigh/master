<?php

namespace Scholarly;

class Subject {
	public $sub_id;
	public $sub_code;
	public $sub_name;
	public $sub_type;
	public $status;
	public $points;
	private $data;

	private static $db;

	public function __construct($sub_id = null)  {
	    if ($sub_id) {
	        $this->sub_id = $sub_id;
	    }
	}

	public function fetchDetails() {
	    $where = ['sub_id'=>$this->sub_id];
	    $sub = self::$db->getData('subject_map', $where);
	    if(count($sub)) {
	        $this->data = $sub[0];
	        $this->sub_id = $this->data['sub_id'];
	        $this->sub_code = $this->data['sub_code'];
	        $this->sub_name = $this->data['sub_name'];
	        $this->sub_type = $this->data['sub_type'];
	        $this->status = $this->data['status'];
	        $this->points = $this->data['points'];
	        return true;
	    }
	}
	
	public static function setDb($db) {
	    self::$db = $db;
	}
}
