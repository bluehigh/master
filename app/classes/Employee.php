<?php

namespace Scholarly;

class Employee extends User {
	public $empid;
	public $name;
	public $email;
	public $dept_id;
	public $desig_id;
	public $phone;
	public $address;
	public $password;
	public $type;

	public function __construct($empid = null) {
		//	Call Uses class object
		parent::__construct();
		if ($empid) {
			$this->empid = $empid;
		}
	}

	public function isTeacher(){
		if($this->type){
			return true;
		}
	}

	public function sendEmail($to,$subject,$txt,$headers){
		//mail($to,$subject,$txt,$headers);
	}

	# insert into specific Employee table
	public function insertIn2table($tname,$fields){
		if($this->db->insertData($tname,$fields)){
			return true;
		}
	}

	# Check if email id exists in general db
	public function emailExists($email) {
		if ($this->db->getCount('emp_data',["email"=>$email])) {
			return true;
		}
	}

	# Generate unique id while adding new Employee
	public function generateId($institute) {
		$sl_no = $this->db->getCount('emp_data')+1;
		$ins_id = $institute->getId();
		$year = substr(date("Y"), 2);
		return $ins_id.$year."0".$sl_no;
	}

	public function login($password) {
		if(isset($this->empid)){
			if($password==$this->data['password']){
				return true;
			}
		}

	}

	// private function employeeExists($empid){
	// 	if ($this->db->getCount('emp_login',["empid"=>$empid])) {
	// 		return true;
	// 	}
	// }

	# Fetch all details from emp_login
	public function fetchDetails() {
		$this->data = $this->db->getData('emp_data',['empid'=>$this->empid]);
		if (count($this->data)) {
			$this->data = $this->data[0];

			$this->name = $this->data['name'];
			$this->email = $this->data['email'];
			$this->dept_id = $this->data['dept_id'];
			$this->desig_id = $this->data['desig_id'];
			$this->phone = $this->data['phone'];
			$this->address = $this->data['address'];
			$this->password = $this->data['password'];
			$this->type = $this->data['emp_type'];
		}
	}

	public function fetchEmpDataDetails() {
		$this->data = $this->db->getData('emp_data',['empid'=>$this->empid])[0];
		return $this->data;
	}

	# Returns 1/2/3/4/5/6.. According to desig_id
	public function getAccessLevel() {
		if ($this->desig_id == 3) {
			// Check for 2 (HOD) in course_map table
			$where = [
				'hod' => $this->empid
			];
			if ($this->db->getCount('course_map', $where)) {
				return '2';
			} else {
				return $this->desig_id;
			}
		} else {
			return $this->desig_id;
		}
	}

	public function getInstituteId() {
		return $this->data['unique_id'];
	}

	public function setUID() {

	}

	public function exists() {
		if (count($this->data)) {
			return true;
		}
	}

	public function resetPassword($email){

	}
}
