<?php

namespace Scholarly;

class Library
{
	public $lib_id;
	public $unique_id;
	public $data;

	private static $db;

	public function __construct($unique_id) {
		$this->unique_id = $unique_id;	
	}

	public function fetchDetails() {
		$where = [
			'unique_id' => $this->unique_id
		];
		$fetched_data = self::$db->getData('library_details', $where);
		if (count($fetched_data)) {
			$this->data = $fetched_data;
			return $this->data;
		}
	}

	public function getBookIds(){
		$sess = $_SESSION['unique_id'];
		$where = ['unique_id' => $sess];

		$get = ['bookid'];
		$book_ids = self::$db->getData('books',$where,$get);

		$formatted_array = [];
		foreach ($book_ids as $key => $value) {
			$formatted_array[]=$value['bookid'];
		}
		return $formatted_array;
	}

	public function initializeLibrary() {
		$set = [
			'unique_id' => $this->unique_id,
			'fine_per_day' => 2,
			'return_period' => 180
		];
		self::$db->insertData('library_details', $set);
		return true;
	}

	public function updateFineAmount($new_amount) {
		$where = [
			'unique_id' => $this->unique_id
		];
		$set = [
			'fine_per_day' => $new_amount
		];
		self::$db->updateData('library_details', $where, $set);
		return true;
	}

	public function updatePeriod($new_period) {
		$where = [
			'unique_id' => $this->unique_id
		];
		$set = [
			'return_period' => $new_period
		];
		self::$db->updateData('library_details', $where, $set);
		return true;
	}

	public function viewBookTransactions($student_id){
		$where = [
			'student_id' => $student_id,
			'unique_id' => $_SESSION['unique_id'],
			'return_date'=> null
		];

		$bookTransactions = self::$db->getData('manage_books',$where);
		if(count($bookTransactions)){
			return $bookTransactions;
		} 
		return [];
	}

	public static function setDb($db) {
		self::$db = $db;
		return true;
	}	
}