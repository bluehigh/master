<?php
  namespace Scholarly;
  class Book{
    public $bookid;
    public $name;
    public $author;
    public $category;
    public $sub;

    public static $db;
    public function __construct($bkid = null) {
      if($bkid){
          $this->bookid = $bkid;
      }
  	}

    public static function setDb($db){
      self::$db=$db;
    }

    public function exists(){
      $where = [
          'bookid' => $this->bookid
      ];
      if (self::$db->getCount('books', $where)) {
        return true;
      }
    }

    public function fetchBookDetails(){
      $this->data = self::$db->getData('books',['bookid'=>$this->bookid])[0];
  		return $this->data;
    }

    public function available() {
      $where = [
          'bookid' => $this->bookid,
          'return_date' => null
      ];
      if (self::$db->getCount('manage_books', $where)) {
        return false;
      } else {
        return true;
      }
    }

    public function getCurrentStatus() {
      $where = [
        'bookid' => $this->bookid,
        'return_date' => null
      ];
      $fetched_data = self::$db->getData('manage_books', $where);
      if (count($fetched_data)) {
        return $fetched_data[0];
      }
    }
  }
