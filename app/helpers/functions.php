<?php

function isRegisterRequestProper() {
	if(isset($_POST['email'],$_POST['pass'])) {
		return true;
	}
}

function setSession($session_data) {
	if(count($session_data)) {
		foreach ($session_data as $key => $value) {
			$_SESSION[$key] = $value;
		}
	}
}

function sessionSetFor($user_type) {
	if(isset($_SESSION['user_type'])) {
		if($user_type!=$_SESSION['user_type']) {
			return false;
		}
		switch ($user_type) {
			case 'student':
				if(isset($_SESSION['student_id'],$_SESSION['password'])) {
					return true;
				}
				break;
			
			case 'employee':
				if(isset($_SESSION['empid'],$_SESSION['password'])) {
					return true;
				}
				break;
			case 'institute':
				if(isset($_SESSION['unique_id'],$_SESSION['password'])) {
					return true;
				}
		}
	}
}

function properRedirectTo($user_type) {
	if(sessionSetFor($user_type)) {
		switch ($user_type) {
			case 'student':
				header("Location: http://".$_SERVER['HTTP_HOST']."/student_dashboard/production/index.php");
				exit(0);
			case 'employee':
				header("Location: http://".$_SERVER['HTTP_HOST']."/employee_dashboard.php");
				exit(0);
			case 'institute':
				header("Location: http://".$_SERVER['HTTP_HOST']."/institute/dashboard/index.php");
				exit(0);
		}
	}
}

function config ($value = null) {
	if (!file_exists(ROOT.'/config.php')) {
		return false;
	} else {
		require_once ROOT.'/config.php';
	}
	if (!isset($value)) {
		$config = json_decode(CONFIG, true);
	} else {
		$config = $this->config();
		$values = explode('.', $value);
		for ($i=0; $i < count($values); $i++) { 
			$config = $config[$values[$i]];
		}
	}
	return $config;
}