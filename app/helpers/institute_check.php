<?php
// Get Started checks

use Scholarly\Institute;
use Debojyoti\PdoConnect\Handler;

$db = new Handler();

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

if (!$institute->hasCode()) {
	header('Location: '.HOST.'/institute/dashboard/setCode.php?unique_id='.$_SESSION['unique_id']);
}

