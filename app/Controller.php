<?php

namespace Scholarly;

class Controller
{
    private $count;
    private $db;

    public function __construct($parts) {
        $this->count = count($parts);
    }

    public function setDb($db) {
        $this->db = $db;
    }

    public function isRequestProper() {
        if($this->count) {
            return true;
        }
    }

    # Require a validinstitute object (that exists)
    public function preparePathFor($institute) {
        header('Location: http://'.$_SERVER['HTTP_HOST'].'/institute/index.php?unique_id='.$institute->getId());
        // return $_SERVER['DOCUMENT_ROOT']."/institute/index.php";

    }
}