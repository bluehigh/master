<?php

require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\Student;
use Scholarly\Department;
use Scholarly\Course;
use Scholarly\Batch;

/*
Controller to handle requests student login form actions 


	Expecting in $_POST[] :
		
		(college_id, password, login) 
		or
		(college_id, email, reset)
		or
		(college_id, otp, submit_otp) 
		or
		(college_id, otp, new_password, save_password)

*/

$db = new Handler();
Batch::setdb($db);
Course::setdb($db);
Department::setdb($db);

$response['proper'] = false;

session_start();

# Handle login request
if(isset($_POST['login'])) {

	$response['action'] = 'login';

	if(isset($_POST['college_id'],$_POST['password'])) {
		
		$response['proper'] = true;
		$student = new Student($_POST['college_id']);
		$student->setDb($db);
		$student->fetchDetails();

		$response['auth'] = false;

		if ($student->exists()) {
			if($student->login(($_POST['password']))) {
				
				$response['auth'] = true;

				// Get institute id
				$batch_id = $student->getBatchId();

				$batch = new Batch($batch_id);
				$batch->fetchDetails();
				$course_id = $batch->getCourseId();

				$course = new Course($course_id);
				$course->fetchDetails();
				$dept_id = $course->getDeptId();

				$dept = new Department($dept_id);
				$dept->fetchDetails();
				$unique_id = $dept->getInstituteId();

				//	Prepare session data
				$session_data = [ 
					'student_id' => $_POST['college_id'],
					'password' => $_POST['password'],
					'unique_id' => $unique_id,
					'user_type' => 'student'
				];
				//	Set session with the data
				setSession($session_data);
			}
		}
	}
	/*	Response structure 

		$response[
				  'action'=>'login',
				  'proper'=>true,
				  'auth'=>true
					]
	*/
}

# Handle password reset request
if(isset($_POST['reset'])) {

	$response['action'] = 'reset';

	if(isset($_POST['college_id'],$_POST['email'])) {
		
		$response['proper'] = true;
		$student = new Student($_POST['college_id']);

		$response['otp_sent'] = false;

		if($student->resetPassword($_POST['email'])) {
			$response['otp_sent'] = true;
		}
	}
	/*	Response structure 

		$response[
				  'action'=>'reset',
				  'proper'=>true,
				  'otp_sent'=>true
					]
	*/
}

# Handle submit_otp request
if(isset($_POST['submit_otp'])) {

	$response['action'] = 'submit_otp';

	if(isset($_POST['college_id'],$_POST['otp'])) {
		
		$response['proper'] = true;
		$student = new Student($_POST['college_id']);

		$response['allowed_to_change'] = false;

		if($student->submitOtp($_POST['otp'])) {
			$response['allowed_to_change'] = true;
		}
	}
	/*	Response structure 

		$response[
				  'action'=>'reset',
				  'proper'=>true,
				  'allowed_to_change'=>true
					]
	*/
}

# Handle save_password request
if(isset($_POST['save_password'])) {

	$response['action'] = 'save_password';

	if(isset($_POST['college_id'],$_POST['otp'],$_POST['new_password'])) {
		
		$response['proper'] = true;
		$student = new Student($_POST['college_id']);

		$response['allowed_to_change'] = false;

		if($student->submitOtp($_POST['otp'])) {
			$response['allowed_to_change'] = true;
			$response['password_changed'] = false;

			if($student->changePassword($_POST['new_password'])) {
				$response['password_changed'] = true;
			}
		}
	}
	/*	Response structure 

		$response[
				  'action'=>'save_password',
				  'proper'=>true,
				  'allowed_to_change'=>true,
				  'password_changed'=>true
					]
	*/
}

print_r(json_encode($response));