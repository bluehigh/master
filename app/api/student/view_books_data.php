<?php
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';
use Debojyoti\PdoConnect\Handler;
use Scholarly\Book;
use Scholarly\Student;
use Scholarly\Employee;
use Scholarly\Librarian;
use Scholarly\Library;

date_default_timezone_set("Asia/Kolkata");
session_start();

$db = new Handler();
Library::setDb($db);

$lib = new Library($_SESSION['unique_id']);
$lib->fetchDetails();

$response['auth'] = false;
if(isset($_SESSION)){

  if (true) {
    $response['auth'] = true;
    $response['proper'] = false;
    if(isset($_REQUEST['action'])) {
      switch ($_REQUEST['action']) {
       
        case 'view':
        $response['proper'] = true;
        $fetched_book_ids = $lib->getBookIds();
        $response['data'] = [];
        if(count($fetched_book_ids)){
          // print_r("more than 1 book exists");
          foreach ($fetched_book_ids as $bookid) {
            $book = new Book($bookid);
            $book->setDb($db);
            $book_details = $book->fetchBookDetails();
            $row['DT_RowId'] = 'row_'.$book_details['bookid'];
            $row['bookid'] = $book_details['bookid'];
            $row['name'] = $book_details['name'];
            $row['author'] = $book_details['author'];
            $row['category'] = $book_details['category'];
            $row['tags'] = $book_details['tags'];
            $row['generate'] = $book_details['bookid'];
            $response['data'][] = $row;
          }
        }
        break;

       
      }
    }
  }
}
print_r(json_encode($response));
