<?php
	// print_r(json_encode([
	// 	['name'=>'Bengal Institute of Technology','unique_id'=>'121'],
	// 	['name'=>'Bengal Institute of Technology','unique_id'=>'121']
	// ]));

	# Project init file 
	require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

	use Debojyoti\PdoConnect\Handler;
	use Debojyoti\DbSearch;

	date_default_timezone_set("Asia/Kolkata");

	$db = new Handler();
	$response = [];
	if (isset($_REQUEST['search_for']) && $_REQUEST['search_for'] != '') {
		$dbsearch = new DbSearch();
		$result = $dbsearch->fetch('institution_general',$_REQUEST['search_for'], ['pid', 'email', 'type', 'plan', 'otp', 'password']);
	
		foreach ($result as $key => $value) {
			$data['name'] = $value['institute_name'];
			$data['unique_id'] = $value['unique_id'];

			$response[] = $data;
		}
	}

	print_r(json_encode($response));