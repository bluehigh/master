<?php
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\User;
use Scholarly\Employee;
use Scholarly\Institute;
use Scholarly\Department;
use Scholarly\Student;
use Scholarly\Batch;

date_default_timezone_set("Asia/Kolkata");

$db = new Handler();
Batch::setDb($db);

session_start();

if (!isset($_SESSION['unique_id'])) {
	exit();
}
if (!isset($_GET['batch_id'])) {
	exit();
}

$institute_id = $_SESSION['unique_id'];

$institute = new Institute($institute_id);
$institute->setDb($db);
$institute->fetchDetails();

$current_batch = $_GET['batch_id'];
$batch = new Batch($current_batch);
$batch->fetchDetails();

if ($_REQUEST['action']) {
	switch ($_REQUEST['action']) {
		case 'view':
			$all_students = $batch->getAllStudentIds();

			$response['data'] = [];

			if (count($all_students)) {
				foreach ($all_students as $student_id) {
					$student = new Student($student_id);
					$student->setDb($db);
					$student->fetchDetails();

					$row['DT_RowId'] = 'row_'.$student->data['student_id'];
					$row['student']['id'] = $student->data['student_id'];
					$row['student']['name'] = $student->data['name'];
					$row['student']['u_roll'] = $student->data['u_roll'];
					$row['student']['u_reg'] = $student->data['u_reg'];
					$row['student']['email'] = $student->data['email'];
					$row['student']['phone'] = $student->data['phone'];
					$row['student']['address'] = $student->data['address'];
					$row['student']['status_id'] = $student->data['status'];
					if ($student->data['status']) {
					    $row['status']['name'] = 'Active';
					} else {
					    $row['status']['name'] = 'Inactive';
					}
					$response['data'][] = $row;
				}
			}
			# Prepare options for status
			$status_option = [
			    [
			        'label' => 'Active',
			        'value' => "1"
			    ],[
			        'label' => 'Inactive',
			        'value' => "0"
			    ]
			];
			$response['options']['student.status_id'] = $status_option;
			break;

		case 'create':
			$student = new Student();
			$student->setDb($db);

			$data = $_REQUEST['data'][0]['student'];

			$student->data['name'] = $data['name'];
			$student->data['u_roll'] = $data['u_roll'];
			$student->data['u_reg'] = $data['u_reg'];
			$student->data['email'] = $data['email'];
			$student->data['phone'] = $data['phone'];
			$student->data['address'] = $data['address'];
			$student->data['status'] = $data['status_id'];

			$student->data['student_id'] = $student->genIdFor($institute, $batch);

			$student->data['course_id'] = $batch->getCourseId();
			$student->data['batch_id'] = $batch->batch_id;

			$student->data['starting_year'] = $batch->starting_year;

			$batch->addStudent($student);

			$student->fetchDetails();

			$row['DT_RowId'] = 'row_'.$student->data['student_id'];
			$row['student']['id'] = $student->data['student_id'];
			$row['student']['name'] = $student->data['name'];
			$row['student']['u_roll'] = $student->data['u_roll'];
			$row['student']['u_reg'] = $student->data['u_reg'];
			$row['student']['email'] = $student->data['email'];
			$row['student']['phone'] = $student->data['phone'];
			$row['student']['address'] = $student->data['address'];
			$row['student']['status_id'] = $student->data['status'];
			if ($student->data['status']) {
			    $row['status']['name'] = 'Active';
			} else {
			    $row['status']['name'] = 'Inactive';
			}
			$response['data'][] = $row;

			break;

		case 'edit':
			$data = $_REQUEST['data'];
			$row_id = key($data); // eg : 'row_8'
			$data = $data[$row_id]['student'];
			$student_id = substr($row_id, 4); // starting at 4th index

			$student = new Student($student_id);
			$student->setDb($db);
			$student->fetchDetails();

			$student->data['name'] = $data['name'];
			$student->data['email'] = $data['email'];
			$student->data['phone'] = $data['phone'];
			$student->data['address'] = $data['address'];
			$student->data['status'] = $data['status_id'];

			$batch->editStudent($student);

			$student->fetchDetails();

			$row['DT_RowId'] = 'row_'.$student->data['student_id'];
			$row['student']['id'] = $student->data['student_id'];
			$row['student']['name'] = $student->data['name'];
			$row['student']['u_roll'] = $student->data['u_roll'];
			$row['student']['u_reg'] = $student->data['u_reg'];
			$row['student']['email'] = $student->data['email'];
			$row['student']['phone'] = $student->data['phone'];
			$row['student']['address'] = $student->data['address'];
			$row['student']['status_id'] = $student->data['status'];
			if ($student->data['status']) {
			    $row['status']['name'] = 'Active';
			} else {
			    $row['status']['name'] = 'Inactive';
			}
			$response['data'][] = $row;

			break;
	}
}

print_r(json_encode($response));




