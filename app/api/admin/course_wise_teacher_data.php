<?php
/*
    API to process all requests to manage teachers of the
    specified course from admin dashboard
*/

# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Department;
use Scholarly\Course;
use Scholarly\Employee;

date_default_timezone_set("Asia/Kolkata");

session_start();

$db = new Handler();
$response['auth'] = false;
Department::setDb($db);
Course::setDb($db);

# Check if course id is provided or not
if (!isset($_GET['course_id'])) {
  // If course_id is not provided, rediret to course management page
  header("Location: ".HOST."/institute/dashboard/courses.php");
}

# Check if the course_id is valid for that institute
$db = new Handler();
Department::setDb($db);
Course::setDb($db);
$current_course = false;

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$fetched_dept_ids = $institute->getDeptIds();

foreach ($fetched_dept_ids as $dept_id) {
  $dept = new Department($dept_id);
  $dept->fetchDetails();
  $fetched_course_ids = $dept->getCourseIds();

  foreach ($fetched_course_ids as $course_id) {
    if ($_GET['course_id'] == $course_id) {
      $current_course = $_GET['course_id']; 
      break 2;
    }
  }
}

$fetched_desigs = $institute->getAllDesignations();

$fetched_dept_ids = $institute->getDeptIds();
$dept_details = [];
foreach ($fetched_dept_ids as $dept_id) {
  $dept = new Department($dept_id);
  $dept->fetchDetails();
  $dept_details[$dept->getId()] = $dept->getName();
}

if ($current_course) { // If current_course is proper
    
    $course = new Course($current_course);
    $course->fetchDetails();

    if (isset($_SESSION)) {
        $response['auth'] = true;
        $response['proper'] = false;
        if(isset($_REQUEST['action'])) {

            switch ($_REQUEST['action']) {
                case 'view':
                    $response['proper'] = true;

                    $all_emp_id = $course->getAllEmpIds();                          
                    $response['data'] = [];
                    if (count($all_emp_id)) {
                        foreach ($all_emp_id as $empid) {
                            
                            $employee = new Employee($empid);
                            $employee->setDb($db);
                            $emp_details = $employee->fetchEmpDataDetails();
                            
                            // Prepare datatbles' rows
                            $row['DT_RowId'] = 'row_'.$emp_details['empid'];
                            $row['emp']['empid'] = $emp_details['empid'];
                            $row['emp']['name'] = $emp_details['name'];
                            $row['emp']['desig'] = $fetched_desigs[$emp_details['desig_id']];
                            $row['emp']['dept'] = $dept_details[$emp_details['dept_id']];
                            $response['data'][] = $row;
                        }
                    }

                    // Prepare options (list) of all available teachers
                    $all_emp = $institute->getAllTeacherIds();
                    foreach ($all_emp as $empid) {
                      if (!in_array($empid, $all_emp_id)) {
                        $employee = new Employee($empid);
                        $employee->setDb($db);
                        $details = $employee->fetchEmpDataDetails();

                        $option_row['label'] = $details['name'];
                        $option_row['value'] = $details['empid'];
                        $response['options']['emp_list.empid'][] = $option_row;
                      }
                    }
                    break;

                case 'create':
                    $data = $_REQUEST['data'][0]['emp_list'];
                    $empid = $data['empid'];

                    if ($empid) {
                      
                      $employee = new Employee($empid);
                      $employee->setDB($db);

                      $course->addEmp($employee);

                      $emp_details = $employee->fetchEmpDataDetails();
                      
                      // Prepare datatbles' rows
                      $row['DT_RowId'] = 'row_'.$emp_details['empid'];
                      $row['emp']['empid'] = $emp_details['empid'];
                      $row['emp']['name'] = $emp_details['name'];
                      $row['emp']['desig'] = $fetched_desigs[$emp_details['desig_id']];
                      $row['emp']['dept'] = $dept_details[$emp_details['dept_id']];
                      $response['data'][] = $row;                      
                    }                    

                    break;
                
                case 'remove':
                      $data = $_REQUEST['data'];
                      $row_id = key($data); // eg : 'row_8'
                      $empid = substr($row_id, 4); // starting at 4th index

                      $employee = new Employee($empid);
                      $employee->setDB($db);

                      $course->removeEmp($employee);
                      break;
            }
        }
    }
}

print_r(json_encode($response));
