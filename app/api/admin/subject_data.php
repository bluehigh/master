<?php
/*
    API to process all requests to manage semesters of the
    specified course from admin dashboard
*/

# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Department;
use Scholarly\Course;
use Scholarly\Semester;
use Scholarly\Subject;

date_default_timezone_set("Asia/Kolkata");

session_start();


# Check if semester id is provided or not
if (!isset($_GET['sem_id'])) {
    session_destroy();
    exit();
}

$db = new Handler();
$response['auth'] = false;
Department::setDb($db);
Course::setDb($db);
Semester::setDb($db);
Subject::setDb($db);

# Check if the sem_id is valid for that institute
$current_sem = false;

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$fetched_dept_ids = $institute->getDeptIds();

if (count($fetched_dept_ids)) {
  foreach ($fetched_dept_ids as $dept_id) {
    $dept = new Department($dept_id);
    $dept->fetchDetails();
    $fetched_course_ids = $dept->getCourseIds();

    if (count($fetched_course_ids)) {
      foreach ($fetched_course_ids as $course_id) {
        $course = new Course($course_id);
        $course->fetchDetails();

        $fetched_sem_ids = $course->getSemIds();
        if (count($fetched_sem_ids)) {
          foreach ($fetched_sem_ids as $sem_id) {
            if ($sem_id == $_GET['sem_id']) {
              $current_sem = $sem_id;
              break 3;
            }
          }
        }
      }
    }
  }
}


if ($current_sem) { // If current_course is proper
    if (isset($_SESSION)) {
        $response['auth'] = true;
        $response['proper'] = false;
        if(isset($_REQUEST['action'])) {

            switch ($_REQUEST['action']) {
                case 'view':
                    $response['proper'] = true;
                    $sem = new Semester($current_sem);
                    $sem->fetchDetails();

                    $fetched_sub_ids = $sem->getSubIds();
                    $response['data'] = [];

                    if (count($fetched_sub_ids)) {
                        foreach ($fetched_sub_ids as $sub_id) {
                            $sub = new Subject($sub_id);
                            $sub->fetchDetails();
                            // Prepare datatbles' rows
                            $row['DT_RowId'] = 'row_'.$sub->sub_id;
                            $row['sub']['sub_name'] = $sub->sub_name;
                            $row['sub']['sub_code'] = $sub->sub_code;
                            $row['sub']['sub_type'] = $sub->sub_type;
                            $row['sub']['points'] = $sub->points;
                            if ($sub->sub_type == 'P') {
                              $row['type']['name'] = 'Practical';
                            } else if ($sub->sub_type == 'T') {
                              $row['type']['name'] = 'Theory';
                            }
                            $response['data'][] = $row;
                        }
                    }
                    # Prepare options for type
                    $type_option = [
                        [
                            'label' => 'Practical',
                            'value' => "P"
                        ],[
                            'label' => 'Theory',
                            'value' => "T"
                        ]
                    ];
                    $response['options']['sub.sub_type'] = $type_option;
                    break;

                case 'create':
                    $data = $_REQUEST['data'][0]['sub'];

                    $required_fields = ['sub_name','sub_type','points'];
                    //  If required fields are present
                    if (!array_diff_key(array_flip($required_fields), $data)) {
                      $response['proper'] = true;
                      
                      $sem = new Semester($current_sem);
                      $sem->fetchDetails();
                      
                      $sub = new Subject();
                      $sub->sub_name = $data['sub_name'];
                      $sub->sub_type = $data['sub_type'];  
                      $sub->points = $data['points'];
                      $sub->status = '1';

                      //  Prepare subject code eg ( CS1T3 = cs+sem_no+theory+sub_no)
                      $sub->sub_code = strtoupper($sem->getCourseCode()).$sem->getNo().$data['sub_type'].(count($sem->getSubIds())+1);

                      if (!$sem->subExists($sub)) {
                        $sem->addSub($sub);

                        // Create a new sub object to get and use it's newly generated sub_id
                        $sub = new Subject($sem->getSubIds($sub->sub_code)[0]);
                        $sub->fetchDetails();
                        // Prepare new row for datatables
                        $row['DT_RowId'] = 'row_'.$sub->sub_id;
                        $row['sub']['sub_name'] = $sub->sub_name;
                        $row['sub']['sub_code'] = $sub->sub_code;
                        $row['sub']['sub_type'] = $sub->sub_type;
                        $row['sub']['points'] = $sub->points;
                        if ($sub->sub_type == 'P') {
                          $row['type']['name'] = 'Practical';
                        } else if ($sub->sub_type == 'T') {
                          $row['type']['name'] = 'Theory';
                        }
                        $response['data'][] = $row;
                      }
                    }
                    


                    break;

                case 'edit':
                    $data = $_REQUEST['data'];
                    $row_id = key($data); // eg : 'row_8'
                    $data = $data[$row_id]['sub'];
                    $sub_id = substr($row_id, 4); // starting at 4th index
                    $required_fields = ['sub_name','sub_type','points'];
                    if (!array_diff_key(array_flip($required_fields), $data)) {
                      $response['proper'] = true;

                      $sub = new Subject($sub_id);
                      $sub->fetchDetails();

                      $sub->sub_name = $data['sub_name'];
                      $sub->points = $data['points'];

                      $sem = new Semester($current_sem);
                      $sem->fetchDetails();
                      if ($sem->editSub($sub)) {
                        $row['DT_RowId'] = 'row_'.$sub->sub_id;
                        $row['sub']['sub_name'] = $sub->sub_name;
                        $row['sub']['sub_code'] = $sub->sub_code;
                        $row['sub']['sub_type'] = $sub->sub_type;
                        $row['sub']['points'] = $sub->points;
                        if ($sub->sub_type == 'P') {
                          $row['type']['name'] = 'Practical';
                        } else if ($sub->sub_type == 'T') {
                          $row['type']['name'] = 'Theory';
                        }
                        $response['data'][] = $row;
                      }



                    }
                    break;
            }
        }
    }
}

print_r(json_encode($response));
