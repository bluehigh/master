<?php
/*
    API to process all requests to manage semesters of the
    specified course from admin dashboard
*/

# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Department;
use Scholarly\Course;
use Scholarly\Semester;

date_default_timezone_set("Asia/Kolkata");

session_start();

$db = new Handler();
$response['auth'] = false;
Department::setDb($db);
Course::setDb($db);
Semester::setDb($db);

# Check if course id is provided or not
if (!isset($_GET['course_id'])) {
  // If course_id is not provided, rediret to course management page
  header("Location: ".HOST."/institute/dashboard/courses.php");
}

# Check if the course_id is valid for that institute
$db = new Handler();
Department::setDb($db);
Course::setDb($db);
$current_course = false;

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$fetched_dept_ids = $institute->getDeptIds();

foreach ($fetched_dept_ids as $dept_id) {
  $dept = new Department($dept_id);
  $dept->fetchDetails();
  $fetched_course_ids = $dept->getCourseIds();

  foreach ($fetched_course_ids as $course_id) {
    if ($_GET['course_id'] == $course_id) {
      $current_course = $_GET['course_id']; 
      break 2;
    }
  }
}


if ($current_course) { // If current_course is proper
    if (isset($_SESSION)) {
        $response['auth'] = true;
        $response['proper'] = false;
        if(isset($_REQUEST['action'])) {
            $institute = new Institute($_SESSION['unique_id']);
            $institute->setDb($db);
            $institute->fetchDetails();

            switch ($_REQUEST['action']) {
                case 'view':
                    $response['proper'] = true;
                    $course = new Course($current_course);
                    $course->fetchDetails();

                    $fetched_sem_ids = $course->getSemIds();
                    if (count($fetched_sem_ids)) {
                        foreach ($fetched_sem_ids as $sem_id) {
                            $sem = new Semester($sem_id);
                            $sem->fetchDetails();
                            // Prepare datatbles' rows
                            $row['DT_RowId'] = 'row_'.$sem->getId();
                            $row['sem_id'] = $sem->getId();
                            $row['sem_no'] = $sem->getNo();
                            $response['data'][] = $row;
                        }
                    }
                    break;
            }
        }
    }
}

print_r(json_encode($response));
