<?php


require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Scholarly\Institute;
use Debojyoti\PdoConnect\Handler;

session_start();

$response['proper'] = false;

# Handle login request
if(isset($_POST['login'])) {

	$response['action'] = 'login';

	if(isset($_POST['unique_id'],$_POST['password'])) {
		
		$response['proper'] = true;
		$institute = new Institute($_POST['unique_id']);
		
		$db = new Handler();
		$institute->setDb($db);

		$response['auth'] = false;

		if ($institute->exists()) {
			if($institute->login(($_POST['password']))) {
				
				$response['auth'] = true;

				//	Prepare session data
				$session_data = [ 
					'unique_id' => $_POST['unique_id'],
					'password' => $_POST['password'],
					'code' => $institute->get('code')['code'],
					'user_type' => 'institute',
	                'institute' => true
				];
				//	Set session with the data
				setSession($session_data);
			}
		}
	}
	/*	Response structure 

		$response[
				  'action'=>'login',
				  'proper'=>true,
				  'auth'=>true
					]
	*/
}
print_r(json_encode($response));