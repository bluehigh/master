<?php
/*
    API to view teachers of the
    specified dept from admin dashboard
*/

# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Department;
use Scholarly\Employee;

date_default_timezone_set("Asia/Kolkata");

session_start();

$db = new Handler();
$response['auth'] = false;
Department::setDb($db);

# Check if course id is provided or not
if (!isset($_GET['dept_id'])) {
  // If course_id is not provided, rediret to course management page
  exit();
}

$current_dept = false;

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$fetched_dept_ids = $institute->getDeptIds();

foreach ($fetched_dept_ids as $dept_id) {
  if ($dept_id == $_GET['dept_id']) {
    $current_dept = $_GET['dept_id'];
  }
}

$fetched_desigs = $institute->getAllDesignations();


if ($current_dept) { // If current_course is proper


    if (isset($_SESSION['institute'])) {
        $response['auth'] = true;
        $response['proper'] = false;
        if(isset($_REQUEST['action'])) {

            switch ($_REQUEST['action']) {
              case 'view':
                $response['proper'] = true;

                $fetched_emp_ids = $institute->getEmployeeIds();
                $response['data'] = [];
                if(count($fetched_emp_ids)){
                  foreach ($fetched_emp_ids as $emp_id) {
                    $employee = new Employee($emp_id);
                    $employee->setDb($db);
                    $emp_details = $employee->fetchEmpDataDetails();
                    if ($emp_details['status'] == 1 && $emp_details['dept_id'] == $current_dept && $emp_details['emp_type'] == 'T') {
                        $row['DT_RowId'] = 'row_'.$emp_details['empid'];
                        $row['emp']['empid'] = $emp_details['empid'];
                        $row['emp']['name'] = $emp_details['name'];
                        $row['emp']['email'] = $emp_details['email'];
                        $row['emp']['desig_id'] = $emp_details['desig_id'];
                        $row['emp']['phone'] = $emp_details['phone'];
                        $row['emp']['address'] = $emp_details['address'];

                        if ($emp_details['emp_type'] == 'T') {
                            $row['type']['name'] = 'Teacher';  
                        } else {
                          $row['type']['name'] = 'Non-Teaching Staff';
                        }

                        $row['desig']['name'] = $fetched_desigs[$emp_details['desig_id']];

                        $response['data'][] = $row;
                    }
                  }
                }

                // Prepare options for designations
                foreach ($fetched_desigs as $desig_id => $desig) {
                  if ($desig_id > 2) {
                    $option_row['label'] = $desig;
                    $option_row['value'] = $desig_id;
                    $response['options']['emp.desig_id'][] = $option_row;
                  }
                }
                break;

            }
        }
    }
}

print_r(json_encode($response));
