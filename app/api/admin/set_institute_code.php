<?php

/*
    API to set institute code for the first time 
*/

# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;

date_default_timezone_set("Asia/Kolkata");

$db = new Handler();

session_start();
$response['auth'] = false;

if (isset($_SESSION['institute'])) {
    $response['auth'] = true;
    if (isset($_REQUEST['action'], $_REQUEST['id'], $_REQUEST['code'])) {
        $institute = new Institute($_REQUEST['id']);
        $institute->setDb($db);
        $institute->fetchDetails();
        switch ($_REQUEST['action']) {
            case 'check':
                $response['available'] = false;
                if ($institute->codeIsUnique($_REQUEST['code'])) {
                    $response['available'] = true;
                }
                break;
            case 'set':
                $response['saved'] = false;
                if ($institute->setCode($_REQUEST['code'])) {
                    $response['saved'] = true;
                    $_SESSION['code'] = $_REQUEST['code'];
                }
                break;
        }
    }
}


print_r(json_encode($response));