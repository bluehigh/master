<?php
/*
    API to process all requests to manage batches of the
    specified course from admin dashboard
*/

# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Department;
use Scholarly\Course;
use Scholarly\Batch;

date_default_timezone_set("Asia/Kolkata");

session_start();

$db = new Handler();
$response['auth'] = false;
Department::setDb($db);
Course::setDb($db);
Batch::setDb($db);

# Check if course id is provided or not
if (!isset($_GET['course_id'])) {
  // If course_id is not provided, rediret to course management page
  header("Location: ".HOST."/institute/dashboard/courses.php");
}

# Check if the course_id is valid for that institute
$db = new Handler();
Department::setDb($db);
Course::setDb($db);
$current_course = false;

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$fetched_dept_ids = $institute->getDeptIds();

foreach ($fetched_dept_ids as $dept_id) {
  $dept = new Department($dept_id);
  $dept->fetchDetails();
  $fetched_course_ids = $dept->getCourseIds();

  foreach ($fetched_course_ids as $course_id) {
    if ($_GET['course_id'] == $course_id) {
      $current_course = $_GET['course_id']; 
      break 2;
    }
  }
}


if ($current_course) { // If current_course is proper
    if (isset($_SESSION)) {
        $response['auth'] = true;
        $response['proper'] = false;
        if(isset($_REQUEST['action'])) {

            switch ($_REQUEST['action']) {
                case 'view':
                    $response['proper'] = true;
                    $course = new Course($current_course);
                    $course->fetchDetails();

                    $fetched_batch_ids = $course->getBatchIds();
                     $response['data'] = [];
                    if (count($fetched_batch_ids)) {
                        foreach ($fetched_batch_ids as $batch_id) {
                            $batch = new Batch($batch_id);
                            $batch->fetchDetails();
                            // Prepare datatbles' rows
                            $row['DT_RowId'] = 'row_'.$batch->batch_id;
                            $row['starting_year'] = $batch->starting_year;
                            $row['ending_year'] = $batch->ending_year;
                            $row['manage'] = $batch->batch_id;
                            $response['data'][] = $row;
                        }
                    }
                    break;

                case 'create':
                    $data = $_REQUEST['data'][0];
                    $required_fields = ['starting_year'];
                    //  If required fields are present
                    if (!array_diff_key(array_flip($required_fields), $data)) {
                        $response['proper'] = true;
                        $batch = new Batch();
                        
                        $batch->starting_year = $data['starting_year'];
                        
                        $course = new Course($current_course);
                        $course->fetchDetails();
                        $span_in_year = $course->getSpan()/2;
                        $batch->ending_year = $data['starting_year']+$span_in_year;

                        if (!$course->batchExists($batch)) {
                          $course->addBatch($batch);

                          $batch = new Batch($course->getBatchIds($data['starting_year']));
                          $batch->fetchDetails();
                          
                          // Prepare datatbles' rows
                          $row['DT_RowId'] = 'row_'.$batch->batch_id;
                          $row['starting_year'] = $batch->starting_year;
                          $row['ending_year'] = $batch->ending_year;
                          $row['manage'] = $batch->batch_id;
                          $response['data'][] = $row;
                        }
                    }
                    break;
            }
        }
    }
}

print_r(json_encode($response));
