<?php
/*
    API to process all requests to manage courses from
    admin dashboard
*/

# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Department;
use Scholarly\Course;
use Scholarly\Semester;

date_default_timezone_set("Asia/Kolkata");

session_start();

$db = new Handler();
$response['auth'] = false;
Department::setDb($db);
Course::setDb($db);

if (isset($_SESSION)) {
    $response['auth'] = true;
    $response['proper'] = false;
    if(isset($_REQUEST['action'])) {
    	$institute = new Institute($_SESSION['unique_id']);
    	$institute->setDb($db);
    	$institute->fetchDetails();

    	switch ($_REQUEST['action']) {
    		case 'view':
    			$response['proper'] = true;
                // Get all dept ids
                $fetched_dept_ids = $institute->getDeptIds();
    			$response['data'] = [];
    			if (count($fetched_dept_ids)) {
    				foreach ($fetched_dept_ids as $dept_id) {
    					$dept = new Department($dept_id);
    					$dept->fetchDetails();
    					$fetched_course_ids = $dept->getCourseIds();
    					// Store to prepare options
    					$dept_name_array[] = $dept->getName(); 
    					$dept_id_array[] = $dept->getId(); 
    					if (count($fetched_course_ids)) {
    						foreach ($fetched_course_ids as $course_id) {
    							$course = new Course($course_id);
    							$course->fetchDetails();
    							$row['DT_RowId'] = 'row_'.$course_id;
    							$row['course']['course_code'] = $course->getCode();
    							$row['course']['course_name'] = $course->getName();
    							$row['course']['course_span'] = $course->getSpan();
    							$row['course']['dept_id'] = $dept_id;
                                $row['course']['status_code'] = $course->getStatus();
    							$row['course']['manage'] = $course->getId();
    							$row['dept']['name'] = $dept->getName();
                                if ($course->getStatus()) {
                                    $row['status']['name'] = 'Active';
                                } else {
                                    $row['status']['name'] = 'Inactive';
                                }
    							$response['data'][] = $row;
    						}
    					}
					}
					# Prepare options for dept
					for($i = 0; $i < count($dept_name_array); $i++) {
						$option_row['label'] = $dept_name_array[$i];
						$option_row['value'] = $dept_id_array[$i];
						$response['options']['course.dept_id'][] = $option_row;
					}
                    # Prepare options for status
                    $status_option = [
                        [
                            'label' => 'Active',
                            'value' => "1"
                        ],[
                            'label' => 'Inactive',
                            'value' => "0"
                        ]
                    ];
                    $response['options']['course.status_code'] = $status_option;
    			}
    			break;

    		case 'create':
    			$response['proper'] = true;
    			$data = $_REQUEST['data'][0]['course'];
    			$required_fields = ['course_name','course_code','course_span','dept_id'];
                //  If required fields are present
                if (!array_diff_key(array_flip($required_fields), $data)) {
                	$response['proper'] = true;
                	$course = new Course();
                	$course->setName($data['course_name']);
                	$course->setCode($data['course_code']);
                	$course->setSpan($data['course_span']);
                    $course->setStatus(1);

                	$dept = new Department($data['dept_id']);
                	$dept->fetchDetails();
                	if (!$dept->courseExists($course)) {
                		$dept->addCourse($course);
                        // Create a new course object to get and use it's newly generated course_id
                        $course = new Course($dept->getCourseIds($data['course_code'])[0]);
                        $course->fetchDetails();
                        // Add semesters
                        for($i = 1; $i <= $data['course_span']; $i++) {
                            $sem = new semester();
                            $sem->setNo($i);
                            $course->addSem($sem);
                        }
                        // Prepare new row for datatables
                		$row['DT_RowId'] = 'row_'.$course->getId();
                		$row['course']['course_code'] = $course->getCode();
                		$row['course']['course_name'] = $course->getName();
                		$row['course']['course_span'] = $course->getSpan();
                		$row['course']['dept_id'] = $dept->getId();
                        $row['course']['status_code'] = $course->getStatus();
                		$row['course']['manage'] = $course->getId();
                		$row['dept']['name'] = $dept->getName();
                        $row['status']['name'] = 'Active';
                		$response['data'][] = $row;
                	}
            	}
    			break;	

            case 'edit':
                $data = $_REQUEST['data'];
                $row_id = key($data); // eg : 'row_8'
                $data = $data[$row_id]['course'];
                $course_id = substr($row_id, 4); // starting at 4th index
                $required_fields = ['course_name','course_code','course_span','dept_id','status_code'];
                if (!array_diff_key(array_flip($required_fields), $data)) {
                    $response['proper'] = true;
                    $course = new Course($course_id);
                    $course->fetchDetails();
                    $course->setName($data['course_name']);
                    $course->setCode($data['course_code']);
                    $course->setSpan($data['course_span']);
                    $course->setStatus($data['status_code']);
                    $dept = new Department($data['dept_id']);
                    $dept->fetchDetails();
                    if ($dept->editCourse($course)) {
                        $row['DT_RowId'] = 'row_'.$course_id;
                        $row['course']['course_code'] = $course->getCode();
                        $row['course']['course_name'] = $course->getName();
                        $row['course']['course_span'] = $course->getSpan();
                        $row['course']['dept_id'] = $dept->getId();
                        $row['course']['status_code'] = $course->getStatus();
                        $row['course']['manage'] = $course_id;
                        $row['dept']['name'] = $dept->getName();
                        if ($course->getStatus()) {
                            $row['status']['name'] = 'Active';
                        } else {
                            $row['status']['name'] = 'Inactive';
                        }
                        $response['data'][] = $row;
                    }
                }
                break;
		}
    }
}

print_r(json_encode($response));