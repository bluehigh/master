<?php
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\User;
use Scholarly\Employee;
use Scholarly\Institute;
use Scholarly\Department;

date_default_timezone_set("Asia/Kolkata");

session_start();
$response['auth'] = false;

$db = new Handler();
Department::setDb($db);

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$fetched_desigs = $institute->getAllDesignations();

$fetched_dept_ids = $institute->getDeptIds();
$dept_details = [];
foreach ($fetched_dept_ids as $dept_id) {
  $dept = new Department($dept_id);
  $dept->fetchDetails();
  $dept_details[$dept->getId()] = $dept->getName();
}

  if(isset($_SESSION['institute'])){
    $response['auth'] = true;
    $response['proper'] = false;
    if(isset($_REQUEST['action'])){
      switch($_REQUEST['action']){
        case 'view' :
          $response['proper'] = true;

          $fetched_emp_ids = $institute->getEmployeeIds();
          $response['data'] = [];
          if(count($fetched_emp_ids)){
            foreach ($fetched_emp_ids as $emp_id) {
              $employee = new Employee($emp_id);
              $employee->setDb($db);
              $emp_details = $employee->fetchEmpDataDetails();
              if ($emp_details['status'] == 1) {
                  $row['DT_RowId'] = 'row_'.$emp_details['empid'];
                  $row['emp']['empid'] = $emp_details['empid'];
                  $row['emp']['name'] = $emp_details['name'];
                  $row['emp']['email'] = $emp_details['email'];
                  $row['emp']['emp_type'] = $emp_details['emp_type'];
                  $row['emp']['desig_id'] = $emp_details['desig_id'];
                  $row['emp']['dept_id'] = $emp_details['dept_id'];
                  $row['emp']['phone'] = $emp_details['phone'];
                  $row['emp']['address'] = $emp_details['address'];

                  if ($emp_details['emp_type'] == 'T') {
                      $row['type']['name'] = 'Teacher';  
                  } else {
                    $row['type']['name'] = 'Non-Teaching Staff';
                  }

                  $row['desig']['name'] = $fetched_desigs[$emp_details['desig_id']];
                  $row['dept']['name'] = $dept_details[$emp_details['dept_id']];

                  $response['data'][] = $row;
              }
            }
          }
          // Prepare options for emp_type
          $emp_type_options = [
              [
                  'label' => 'Teacher',
                  'value' => "T"
              ],[
                  'label' => 'Non-Teaching Staff',
                  'value' => "N"
              ]
          ];
          $response['options']['emp.emp_type'] = $emp_type_options;

          // Prepare options for designations
          foreach ($fetched_desigs as $desig_id => $desig) {
            if ($desig_id > 2) {
              $option_row['label'] = $desig;
              $option_row['value'] = $desig_id;
              $response['options']['emp.desig_id'][] = $option_row;
            }
          }

          // Prepare options for designations
          foreach ($dept_details as $dept_id => $dept_name) {
            $option_row['label'] = $dept_name;
            $option_row['value'] = $dept_id;
            $response['options']['emp.dept_id'][] = $option_row;
          }
          break;

        case 'create' :
          $data = $_REQUEST['data'][0]['emp'];
          $required_fields = ['name','email','emp_type','dept_id','desig_id','phone','address'];
          //  If required fields are present
          if (!array_diff_key(array_flip($required_fields), $data)) {
              $response['proper'] = true;
              //  Create employee object
              $employee = new Employee();
              $employee->setDb($db);
              //  Add employee data
              $response['success'] = false;

              $employee->empid = $employee->generateId($institute);
              $employee->name = $data['name'];
              $employee->email = $data['email'];
              $employee->dept_id = $data['dept_id'];
              $employee->desig_id = $data['desig_id'];
              $employee->type = $data['emp_type'];
              $employee->phone = $data['phone'];
              $employee->address = $data['address'];
              
              if (!$institute->employeeExists($employee)) {
                
                if ($institute->addEmployee($employee)) {
                  $response['success'] = true;
                  $current_employee = $institute->getEmployeeIds($data['email']);
                  $employee = new Employee($current_employee);
                  $employee->setdb($db);
                  $emp_details = $employee->fetchEmpDataDetails();
                  
                  $row['DT_RowId'] = 'row_'.$emp_details['empid'];
                  $row['emp']['empid'] = $emp_details['empid'];
                  $row['emp']['name'] = $emp_details['name'];
                  $row['emp']['email'] = $emp_details['email'];
                  $row['emp']['emp_type'] = $emp_details['emp_type'];
                  $row['emp']['desig_id'] = $emp_details['desig_id'];
                  $row['emp']['dept_id'] = $emp_details['dept_id'];
                  $row['emp']['phone'] = $emp_details['phone'];
                  $row['emp']['address'] = $emp_details['address'];

                  if ($emp_details['emp_type'] == 'T') {
                      $row['type']['name'] = 'Teacher';  
                  } else {
                    $row['type']['name'] = 'Non-Teaching Staff';
                  }

                  $row['desig']['name'] = $fetched_desigs[$emp_details['desig_id']];
                  $row['dept']['name'] = $dept_details[$emp_details['dept_id']];

                  $response['data'][] = $row;
                }
              }

          }
          break;

        case 'edit' : 
            $data = $_REQUEST['data'];
            $row_id = key($data); // eg : 'row_8'
            $data = $data[$row_id]['emp'];
            $empid = substr($row_id, 4); // starting at 4th index
            $required_fields = ['name','email','emp_type','dept_id','desig_id','phone','address'];
            if (!array_diff_key(array_flip($required_fields), $data)) {
                $response['proper'] = true;
                $employee = new Employee($empid);
                $employee->setDb($db);
                $employee->fetchDetails();

                $employee->name = $data['name'];
                $employee->email = $data['email'];
                $employee->dept_id = $data['dept_id'];
                $employee->desig_id = $data['desig_id'];
                $employee->type = $data['emp_type'];
                $employee->phone = $data['phone'];
                $employee->address = $data['address'];

                $institute->editEmployee($employee);

                $emp_details = $employee->fetchEmpDataDetails();
                
                $row['DT_RowId'] = 'row_'.$emp_details['empid'];
                $row['emp']['empid'] = $emp_details['empid'];
                $row['emp']['name'] = $emp_details['name'];
                $row['emp']['email'] = $emp_details['email'];
                $row['emp']['emp_type'] = $emp_details['emp_type'];
                $row['emp']['desig_id'] = $emp_details['desig_id'];
                $row['emp']['dept_id'] = $emp_details['dept_id'];
                $row['emp']['phone'] = $emp_details['phone'];
                $row['emp']['address'] = $emp_details['address'];

                if ($emp_details['emp_type'] == 'T') {
                    $row['type']['name'] = 'Teacher';  
                } else {
                  $row['type']['name'] = 'Non-Teaching Staff';
                }

                $row['desig']['name'] = $fetched_desigs[$emp_details['desig_id']];
                $row['dept']['name'] = $dept_details[$emp_details['dept_id']];

                $response['data'][] = $row;

            }
            break;
        
        case 'remove' :
            $data = $_REQUEST['data'];
            $row_id = key($data); // eg : 'row_8'
            $empid = substr($row_id, 4); // starting at 4th index

            $employee = new Employee($empid);

            $institute->deRegister($employee);
            break;
      }
    }

  }

print_r(json_encode($response));
