<?php
/*
    API to process all requests(Non-ajax) of institute registration
    and redirect them to proper page
*/

# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Library;

date_default_timezone_set("Asia/Kolkata");

$db = new Handler();
Library::setDb($db);

if(isset($_POST['action'])) {
    $data['datetime'] = date("d/m/Y h:i:sa");
    $data['action'] = $_POST['action'];
    $data['form_data'] = $_POST;
    $data = json_encode($data);
    # Write on log file
    if(!file_exists(ROOT."/logs")) {
        mkdir(ROOT."/logs", 077, true);
    }
    file_put_contents(ROOT."/logs/register-api-access.log",$data.PHP_EOL, FILE_APPEND | LOCK_EX);
    switch ($_POST['action']) {
        case "add_institute":
            $institute = new Institute();
            $institute->setDb($db);
            # Check if all required fields are present
            $expected_fields = ["institute_name","university_name","email","type","registration_id","plan"];
            $form_data = $_POST;
            unset($form_data['action']);
            unset($form_data['register']);
            $keys = array_keys($form_data);
            if(!array_diff($expected_fields, $form_data)) {
                header("Location: ".HOST);
                exit(0);            
            }
            # Process form data
            # Check if institute with same registration id already exists or not
            if($institute->registrationIdExists($form_data['registration_id'])) {
                // If exists, redirect to help section
                header("Location: ".HOST."/institute/help.php?registrationidexists=".$form_data['registration_id']);
                exit(0);
            }
            # Check if institute with same email id already exists or not
            if($institute->emailExists($form_data['email'])) {
                // If exists, redirect to help section
                header("Location: ".HOST."/institute/help.php?emailexists=".$form_data['email']);
                exit(0);
            }
            $institute->add($form_data);
            $institute->sendVerificationCode(1);    // For 1st time = 1
            $id = $institute->getId();
            $lib = new Library($id);
            $lib->initializeLibrary();
            header("Location: ".HOST."/institute/verify.php?unique_id=".$id);
            break;
        case "verify_institute":
            if(!isset($_POST['unique_id'], $_POST['otp'])) {
                # Redirect to homepage
                exit(0);
            }
            $form_data = $_POST;
            $institute = new Institute($form_data['unique_id']);
            $institute->setDb($db);
            # Check if unique id is valid
            if(!$institute->exists()) {
                # Redirect to homepage
                header("Location: ".HOST);
                exit(0);
            }
            # Check if otp requested
            if(!$institute->otpRequested()) {
                # Redirect to homepage
                header("Location: ".HOST);
                exit(0);
            } 
            # Verify OTP
            if(!$institute->verifyOtp($form_data['otp'])) {
                # Redirect to homepage
                header("Location: ".HOST);
                exit(0);
            }
            # Redirect to next form page
            $id = $institute->getId();
            header("Location: ".HOST."/institute/set_pass.php?unique_id=".$id."&otp=".$form_data['otp']);
            break;
        case "set_password":
            if(!isset($_POST['unique_id'], $_POST['password'], $_POST['otp'])) {
                # Redirect to homepage
                header("Location: ".HOST);
                exit(0);
            }
            $institute = new Institute($_POST['unique_id']);
            $institute->setDb($db);
            if(!$institute->exists()) {
                # Redirect to homepage
                header("Location: ".HOST);
                exit(0);
            }
            # Check if otp requested
            if(!$institute->otpRequested()) {
                # Redirect to homepage
                header("Location: ".HOST);
                exit(0);
            } 
            # Verify OTP
            if(!$institute->verifyOtp($_POST['otp'])) {
                # Redirect to homepage
                header("Location: ".HOST);
                exit(0);
            }
            # Set password
            $institute->setPassword($_POST['password']);
            # Clear otp field
            header("Location: ".HOST."/institute/login.php?unique_id=".$_POST['unique_id']);
            break;
    }
} elseif (isset($_GET['action'], $_GET['unique_id'])) {
    $institute = new Institute($_GET['unique_id']);
    $institute->setDb($db);
    switch ($_GET['action']) {
        case 'otp_verify':
            if($institute->otpRequested()) {
                $response = $institute->get('institute_name');
                echo json_encode($response);
            } 
            break;
    }
} else {
    # Redirect to homepage
    header("Location: ".HOST);
}