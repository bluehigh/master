<?php
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\User;
use Scholarly\Teacher;
use Scholarly\Institute;
use Scholarly\Department;
use Scholarly\Student;
use Scholarly\Semester;
use Scholarly\Course;
use Scholarly\Batch;
use Scholarly\Subject;
use Scholarly\Routine;

date_default_timezone_set("Asia/Kolkata");

$db = new Handler();
Batch::setDb($db);
Course::setDb($db);
Routine::setDb($db);
Subject::setDb($db);
Semester::setDb($db);
Department::setDb($db);

session_start();

if (!isset($_SESSION['unique_id'])) {
	exit();
}
if (!isset($_GET['day'])) {
	exit();
}
$institute_id = $_SESSION['unique_id'];

$institute = new Institute($institute_id);
$institute->setDb($db);
$institute->fetchDetails();

$all_dept_ids = $institute->getDeptIds();

if (count($all_dept_ids)) {
	foreach ($all_dept_ids as $dept_id) {
		$dept = new Department($dept_id);
		$dept->fetchDetails();
		$all_course_id = $dept->getCourseIds();

		if (count($all_course_id)) {
			foreach ($all_course_id as $course_id) {
				$course = new Course($course_id);
				$course->fetchDetails();

				$all_sem_id = $course->getSemIds();

				if (count($all_sem_id)) {
					foreach ($all_sem_id as $sem_id) {
						$sem = new Semester($sem_id);
						$sem->fetchDetails();

						$all_sub_id = $sem->getSubIds();

						if (count($all_sub_id)) {
							foreach ($all_sub_id as $sub_id) {
								$sub = new Subject($sub_id);
								$sub->fetchDetails();


								$sub_code_array[$sub_id] = $sub->sub_code;
								$sub_name_array[$sub_id] = $sub->sub_name;
							}
						}
					}
				}
			}
		}
	}
}

$teacher = new Teacher($_SESSION['empid']);
$teacher->setDb($db);
$teacher->fetchDetails();


if (isset($_REQUEST['action'])) {
	switch ($_REQUEST['action']) {
		case 'view':
			
			$routine = new Routine();
			$routine->setDay($_REQUEST['day']);
			$all_periods = $routine->getPeriodsOf($teacher);

			$response['data'] = [];

			if (count($all_periods)) {
				foreach ($all_periods as $period) {

					$row['DT_RowId'] = 'row_'.$period['s_time'];
					$row['class']['starts'] = $period['s_time'];
					$row['class']['ends'] = $period['e_time'];
					$row['class']['sub_code'] = $sub_code_array[$period['sub_id']];
					$row['class']['sub_name'] = $sub_name_array[$period['sub_id']];
					$row['class']['period_id'] = $period['period_id'];
					
					$response['data'][] = $row;
				}
			}
			break;
	}
}

print_r(json_encode($response));




