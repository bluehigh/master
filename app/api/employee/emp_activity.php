<?php
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\User;
use Scholarly\Employee;

date_default_timezone_set("Asia/Kolkata");

$response['proper'] = false;
$response['auth'] = false;

session_start();

$db = new Handler();

if(isset($_REQUEST['login'])){
  $response['action'] = 'login';
  if(isset($_POST['empid'],$_POST['password'])){
    $response['proper']=true;
    $employee = new Employee($_POST['empid']);

    $db = new Handler();
    $employee->setDb($db);
    $employee->fetchDetails();

    $response['auth'] = false;
    if ($employee->exists()) {
      if($employee->login($_POST['password'])){
        $response['auth']=true;

        //  Prepare session data
        $session_data = [
          'empid' => $_POST['empid'],
          'password' => $_POST['password'],
          'unique_id' => $employee->getInstituteId(),
          'user_type' => 'employee',
          'employee' => true
        ];
        //  Set session with the data
        setSession($session_data);
        if ($employee->isTeacher()) {
          $response['emp_type'] = 'teacher';
        }
      }
    }
  }
}

print_r(json_encode($response));

/*

*/
