<?php
/*
    API to process all requests to manage departments from
    hod dashboard
*/

# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Department;

date_default_timezone_set("Asia/Kolkata");

session_start();

$db = new Handler();
$response['auth'] = false;
Department::setDb($db);

if (isset($_SESSION['employee'])) {
    $response['auth'] = true;
    $response['proper'] = false;

    if(isset($_REQUEST['action'])) {
        $institute = new Institute($_SESSION['unique_id']);
        $institute->setDb($db);
        $institute->fetchDetails();

        switch ($_REQUEST['action']) {
            case 'view':
                $response['proper'] = true;
                // Get all dept ids
                $fetched_dept_ids = $institute->getDeptIds();
                $response['data'] = [];
                if (count($fetched_dept_ids)) {
                    foreach ($fetched_dept_ids as $dept_id) {
                        $dept = new Department($dept_id);
                        $dept->fetchDetails();
                        $row['DT_RowId'] = 'row_'.$dept_id;
                        $row['dept_code'] = $dept->getCode();
                        $row['dept_name'] = $dept->getName();
                        $row['course_count'] = count($dept->getCourseIds());
                        $row['manage'] = $dept_id;
                        $response['data'][] = $row;
                    }
                }
                break;
            
            case 'create':
                $data = $_REQUEST['data'][0];
                $required_fields = ['dept_name','dept_code'];
                //  If required fields are present
                if (!array_diff_key(array_flip($required_fields), $data)) {
                    $response['proper'] = true;
                    $dept = new Department();
                    $dept->setCode($data['dept_code']);
                    $dept->setName($data['dept_name']);
                    if (!$institute->deptExists($dept)) {
                        $institute->addDept($dept);
                        // Return the new row
                        $new_dept = $institute->getDeptIds($data['dept_code']);
                        if (count($new_dept)) {
                            $new_dept = $new_dept[0];
                            $dept = new Department($new_dept);
                            $dept->fetchDetails();
                            $response['data'][0]['DT_RowId'] = 'row_'.$new_dept;
                            $response['data'][0]['dept_code'] =  $dept->getCode();
                            $response['data'][0]['dept_name'] = $dept->getName();
                            $response['data'][0]['course_count'] = count($dept->getCourseIds());
                            $response['data'][0]['manage'] = $new_dept;
                        }
                    }
                }
                break;

            case 'edit': 
                // Single row edit
                $data = $_REQUEST['data'];
                $row = key($data); // eg : 'row_8'
                $data = $data[$row];
                $dept_id = substr($row, 4); // starting at 4th index
                $required_fields = ['dept_name','dept_code'];
                //  If required fields are present
                if (!array_diff_key(array_flip($required_fields), $data)) {
                    $dept = new Department($dept_id);
                    $dept->fetchDetails();
                    $dept->setName($data['dept_name']);
                    $dept->setCode($data['dept_code']);
                    if ($institute->editDept($dept)) {
                        $dept = new Department($dept_id);
                        $dept->fetchDetails();
                        $response['data'][0]['DT_RowId'] = 'row_'.$dept_id;
                        $response['data'][0]['dept_code'] =  $dept->getCode();
                        $response['data'][0]['dept_name'] = $dept->getName();
                        $response['data'][0]['course_count'] = count($dept->getCourseIds());
                        $response['data'][0]['manage'] = $dept_id;
                    }
                }
                break;

            case 'remove':
                // Single row delete
                $data = $_REQUEST['data'];
                $row = key($data); // eg : 'row_8'
                $data = $data[$row];
                $dept_id = substr($row, 4); // starting at 4th index
                $dept = new Department($dept_id);
                $dept->fetchDetails();
                $institute->deleteDept($dept);
                $response['data'] = [];
                break;
        }
    }    
}

print_r(json_encode($response));