<?php
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\User;
use Scholarly\Teacher;
use Scholarly\Institute;
use Scholarly\Department;
use Scholarly\Student;
use Scholarly\Semester;
use Scholarly\Course;
use Scholarly\Batch;
use Scholarly\Subject;
use Scholarly\Routine;

date_default_timezone_set("Asia/Kolkata");

$db = new Handler();
Batch::setDb($db);
Course::setDb($db);
Routine::setDb($db);
Subject::setDb($db);
Semester::setDb($db);
Department::setDb($db);

session_start();

if (!isset($_SESSION['unique_id'])) {
	exit();
}
if (!isset($_GET['batch_id'])) {
	exit();
}
if (!isset($_GET['day'])) {
	exit();
}
$institute_id = $_SESSION['unique_id'];

$institute = new Institute($institute_id);
$institute->setDb($db);
$institute->fetchDetails();

$current_batch = $_GET['batch_id'];
$batch = new Batch($current_batch);
$batch->fetchDetails();

$current_course_id = $batch->getCourseId();

$course = new Course($current_course_id);
$course->fetchDetails();

$current_sem_id = $course->getCurrentSemFor($batch);

$sem = new Semester($current_sem_id);
$sem->fetchDetails();

$sub_ids = $sem->getSubIds();

$sub_code_array = [];
$sub_name_array = [];

if (count($sub_ids)) {
	foreach ($sub_ids as $sub_id) {
		$sub = new Subject($sub_id);
		$sub->fetchDetails();
		$sub_name_array[$sub_id] = $sub->sub_name;
		$sub_code_array[$sub_id] = $sub->sub_code;
	}
}

$teacher_ids = $course->getAllEmpIds();

$teacher_name_array = [];

if (count($teacher_ids)) {
	foreach ($teacher_ids as $teacher_id) {
		$teacher = new Teacher($teacher_id);
		$teacher->setDb($db);
		$teacher->fetchDetails();
		$teacher_name_array[$teacher->empid] = $teacher->name;
	}
}

$response['sem_id'] = $current_sem_id;


if (isset($_REQUEST['action'])) {
	switch ($_REQUEST['action']) {
		case 'view':
			
			$routine = new Routine();
			$routine->setSem($sem);
			$routine->setDay($_REQUEST['day']);
			$all_routine_data = $routine->fetchDetails();

			$response['data'] = [];

			if (count($all_routine_data)) {
				foreach ($all_routine_data as $routine_data) {

					$row['DT_RowId'] = 'row_'.$routine_data['s_time'];
					$row['class']['starts'] = $routine_data['s_time'];
					$row['class']['ends'] = $routine_data['e_time'];
					$row['class']['sub_code'] = $sub_code_array[$routine_data['sub_id']];
					$row['class']['sub_name'] = $sub_name_array[$routine_data['sub_id']];
					$row['class']['empid'] = $routine_data['empid'];
					$row['emp']['name'] = $teacher_name_array[$routine_data['empid']];
					
					$response['data'][] = $row;
				}
			}
			# Prepare options for teachers
			foreach ($teacher_name_array as $key => $value) {
				$option_row['label'] = $value;
				$option_row['value'] = $key;
				$response['options']['class.empid'][] = $option_row;
			}
			# Prepare options for subjects
			foreach ($sub_code_array as $key => $value) {
				$option_row['label'] = $sub_name_array[$key]." (".$sub_code_array[$key].")";
				$option_row['value'] = $sub_code_array[$key];
				$response['options']['class.sub_code'][] = $option_row;
			}
			break;

		case 'create':
			
			$data = $_REQUEST['data'][0]['class'];

			$routine = new Routine();
			$routine->setSem($sem);
			$routine->setDay($_REQUEST['day']);

			$data['sub_id'] = array_search($data['sub_code'], $sub_code_array);

			$assigned_teacher = new Teacher($data['empid']);
			$assigned_teacher->setDb($db);
			$assigned_teacher->fetchDetails();

			if ($assigned_teacher->isAvailableBetween($data['starts'],$data['ends'],$_REQUEST['day'])) {
				$new_period_id = $routine->addPeriod($data);

				$fetched_period = $routine->fetchPeriod($new_period_id);

				$row['DT_RowId'] = 'row_'.$fetched_period['s_time'];
				$row['class']['starts'] = $fetched_period['s_time'];
				$row['class']['ends'] = $fetched_period['e_time'];
				$row['class']['sub_code'] = $sub_code_array[$fetched_period['sub_id']];
				$row['class']['sub_name'] = $sub_name_array[$fetched_period['sub_id']];
				$row['class']['empid'] = $fetched_period['empid'];
				$row['emp']['name'] = $teacher_name_array[$fetched_period['empid']];
				
				$response['data'][] = $row;
			} else {
				$response['error'] = $teacher_name_array[$data['empid']]." is not available in the selected time span";
				$response['data'] = [];
			}

			
			break;

		case 'edit':
			$data = $_REQUEST['data'];
			$row_id = key($data); // eg : 'row_8'
			$data = $data[$row_id]['class'];
			$s_time = substr($row_id, 4); // starting at 4th index

			$routine = new Routine();
			$routine->setSem($sem);
			$routine->setDay($_REQUEST['day']);
			$routine->setSpan($data['starts'],$data['ends']);
			
			$prev_teacher_id = $routine->getTeacherId();

			$data['sub_id'] = array_search($data['sub_code'], $sub_code_array);

			if ($prev_teacher_id == $data['empid']) {
				$period_id = $routine->updatePeriod($data);

				$fetched_period = $routine->fetchPeriod($period_id);

				$row['DT_RowId'] = 'row_'.$fetched_period['s_time'];
				$row['class']['starts'] = $fetched_period['s_time'];
				$row['class']['ends'] = $fetched_period['e_time'];
				$row['class']['sub_code'] = $sub_code_array[$fetched_period['sub_id']];
				$row['class']['sub_name'] = $sub_name_array[$fetched_period['sub_id']];
				$row['class']['empid'] = $fetched_period['empid'];
				$row['emp']['name'] = $teacher_name_array[$fetched_period['empid']];
				
				$response['data'][] = $row;
			} else {
				
				$assigned_teacher = new Teacher($data['empid']);
				$assigned_teacher->setDb($db);
				$assigned_teacher->fetchDetails();

				if ($assigned_teacher->isAvailableBetween($data['starts'],$data['ends'], $_REQUEST['day'])) {

					$period_id = $routine->updatePeriod($data);

					$fetched_period = $routine->fetchPeriod($period_id);

					$row['DT_RowId'] = 'row_'.$fetched_period['s_time'];
					$row['class']['starts'] = $fetched_period['s_time'];
					$row['class']['ends'] = $fetched_period['e_time'];
					$row['class']['sub_code'] = $sub_code_array[$fetched_period['sub_id']];
					$row['class']['sub_name'] = $sub_name_array[$fetched_period['sub_id']];
					$row['class']['empid'] = $fetched_period['empid'];
					$row['emp']['name'] = $teacher_name_array[$fetched_period['empid']];
					
					$response['data'][] = $row;
				} else {
					$response['error'] = $teacher_name_array[$data['empid']]." is not available in the selected time span";
					$response['data'] = [];
				}
			}

			break;

		case 'remove':
				$data = $_REQUEST['data'];
				$row_id = key($data); // eg : 'row_8'
				$data = $data[$row_id]['class'];
				$s_time = substr($row_id, 4); // starting at 4th index

				$routine = new Routine();
				$routine->setSem($sem);
				$routine->setDay($_REQUEST['day']);
				$routine->setSpan($data['starts'],$data['ends']);

				$routine->removePeriod();


		case 'clearall':
				$routine = new Routine();
				$routine->setSem($sem);
				$routine->setDay($_REQUEST['day']);

				$routine->clearAllPeriods();



	}
}

print_r(json_encode($response));




