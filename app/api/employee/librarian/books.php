<?php
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';
use Debojyoti\PdoConnect\Handler;
use Scholarly\Book;
use Scholarly\Student;
use Scholarly\Employee;
use Scholarly\Librarian;
use Scholarly\Library;

date_default_timezone_set("Asia/Kolkata");
session_start();

$db = new Handler();

$response['auth'] = false;
if(isset($_SESSION['employee'])){
  $librarian = new Librarian($_SESSION['empid']);
  $librarian->setDb($db);
  $librarian->fetchDetails();

  if ($librarian->getAccessLevel() == 7) {
    $response['auth'] = true;
    $response['proper'] = false;
    if(isset($_REQUEST['action'])) {
      switch ($_REQUEST['action']) {
        case 'create':
          $data = $_REQUEST['data'][0];
          $required_fields = ['name','author','category'];
          if(!array_diff_key(array_flip($required_fields),$data)){
            $response['proper'] = true;
            $book = new Book();
            Book::setDb($db);
            $book->name = $data['name'];
            $book->author = $data['author'];
            $book->category = $data['category'];
            $book->tags = $data['tags'];

            if($librarian->addBooks($book)){
              $response['success'] = true;
              $row['DT_RowId'] = 'row_'.$book->bookid;
              $row['bookid'] = $book->bookid;
              $row['name'] = $book->name;
              $row['author'] = $book->author;
              $row['category'] = $book->category;
              $row['tags'] = $book->tags;
              $response['data'][] = $row;
            }
          }
          break;
        case 'view':
        $response['proper'] = true;
        $fetched_book_ids = $librarian->getBookIds();
        $response['data'] = [];
        if(count($fetched_book_ids)){
          // print_r("more than 1 book exists");
          foreach ($fetched_book_ids as $bookid) {
            $book = new Book($bookid);
            $book->setDb($db);
            $book_details = $book->fetchBookDetails();
            $row['DT_RowId'] = 'row_'.$book_details['bookid'];
            $row['bookid'] = $book_details['bookid'];
            $row['name'] = $book_details['name'];
            $row['author'] = $book_details['author'];
            $row['category'] = $book_details['category'];
            $row['tags'] = $book_details['tags'];
            $row['generate'] = $book_details['bookid'];
            $response['data'][] = $row;
          }
        }
        break;

        case 'remove':
        $response['proper'] = true;
        $data = $_REQUEST['data'];
        $row_id = key($data); // eg : 'row_8'
        $data = $data[$row_id];
        $book = new Book(substr($row_id, 4));
        Book::setDb($db);
        $librarian->removeBook($book);
      }
    }
  }
}
print_r(json_encode($response));
