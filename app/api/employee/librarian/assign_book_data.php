<?php
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\User;
use Scholarly\Librarian;
use Scholarly\Student;
use Scholarly\Book;


date_default_timezone_set("Asia/Kolkata");

session_start();

$db = new Handler();
Book::setDb($db);

$response['error'] = false;

if (isset($_REQUEST['action'])) {
	switch($_REQUEST['action']) {
		case 'fetchStudent':
				if ($_REQUEST['student_id']) {
					$student = new Student($_REQUEST['student_id']);
					$student->setDb($db);
					$student->fetchDetails();
					if ($student->exists()) {
						$response['student']['name'] = $student->data['name'];
						$all_book_data = $student->activeBooksData();
						$response['student']['active_book_no'] = count($all_book_data);
						$response['student']['phone'] = $student->data['phone'];
					} else {
						$response['error'] = '1'; // no such student
					}
				}
				break;
		case 'fetchBook':
				if ($_REQUEST['bookid']) {
					$book = new Book($_REQUEST['bookid']);
					if ($book->exists()) {
						$book_data = $book->fetchBookDetails();
						if ($book->available()) {
							// Preapre book data
							$response['book']['bookid'] = $book_data['bookid'];
							$response['book']['name'] = $book_data['name'];
							$response['book']['author'] = $book_data['author'];
							$response['book']['category'] = $book_data['category'];
						} else {
							$response['error'] = '2'; // already taken
						}
					} else {
						$response['error'] = '1'; // no such book
					}
				}
				break;

		case 'assignBook':
		$response['success'] = '0';
				if ($_REQUEST['bookid'] && $_REQUEST['student_id']) {
					$book = new Book($_REQUEST['bookid']);
					$librarian = new Librarian($_SESSION['empid']);
					$librarian->setDb($db);
					$librarian->fetchDetails();
					$librarian->assignBook($_REQUEST['student_id'], $_REQUEST['bookid']);
					$response['success'] = '1';
				}
				break;

	}
}

print_r(json_encode($response));