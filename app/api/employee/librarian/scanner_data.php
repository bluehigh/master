<?php
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\User;
use Scholarly\Teacher;
use Scholarly\Institute;
use Scholarly\Department;
use Scholarly\Student;
use Scholarly\Semester;
use Scholarly\Course;
use Scholarly\Batch;
use Scholarly\Subject;
use Scholarly\Routine;

date_default_timezone_set("Asia/Kolkata");

$db = new Handler();
Batch::setDb($db);
Course::setDb($db);
Routine::setDb($db);
Subject::setDb($db);
Semester::setDb($db);
Department::setDb($db);

session_start();

$response['connected'] = false;

if (isset($_REQUEST['connected'])) {
	if (isset($_SESSION)) {
		if ($_SESSION['empid'] == $_REQUEST["empid"]) {
			if ($db->getCount('scanner_status', ['librarian_id' => $_SESSION['empid']])) {
				$db->updateData('scanner_status', ['librarian_id' => $_SESSION['empid']], ['timestamp'=>time()]);
			} else {
				$db->insertData('scanner_status', ['librarian_id' => $_SESSION['empid'], 'timestamp'=>time()]);
			}
		}
	}	
}
if (isset($_REQUEST['new_data'])) {
	if (isset($_SESSION)) {
		if ($_SESSION['empid'] == $_REQUEST["empid"]) {
			if ($db->getCount('scanner_status', ['librarian_id' => $_SESSION['empid']])) {
				$db->updateData('scanner_status', ['librarian_id' => $_SESSION['empid']], ['timestamp'=>time(),'result' => $_REQUEST['new_data'], 'data_time'=>time()]);
			} else {
				$db->insertData('scanner_status', ['librarian_id' => $_SESSION['empid'], 'timestamp'=>time(),'result' => $_REQUEST['new_data'], 'data_time'=>time()]);
			}
		}
	}	
}
if (isset($_REQUEST['pc_sync'])) {
	if (isset($_SESSION)) {
		if ($_SESSION['empid'] == $_REQUEST["empid"]) {
			if ($db->getCount('scanner_status', ['librarian_id' => $_SESSION['empid']])) {
				$data = $db->getData('scanner_status', ['librarian_id' => $_SESSION['empid']])[0];
				$time = $data['timestamp'];
				$data_time = $data['data_time'];
				$result = $data['result'];
				if ((time()-$time)<=4) {
					if ((time()-$data_time)<=10) {
						if ($result) {
							$response['data'] = $result;	
						}
					}
					$response['connected'] = true;
				}
			} else {
				$response['connected'] = false;
			}
		}
	}	
}
print_r(json_encode($response));
