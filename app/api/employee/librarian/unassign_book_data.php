<?php
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\User;
use Scholarly\Librarian;
use Scholarly\Student;
use Scholarly\Book;
use Scholarly\Library;


date_default_timezone_set("Asia/Kolkata");

session_start();

$db = new Handler();
Book::setDb($db);
Library::setDb($db);

$lib = new Library($_SESSION['unique_id']);
$lib->fetchDetails();

$response['error'] = false;

if (isset($_REQUEST['action'])) {
	switch($_REQUEST['action']) {
		case 'fetchBook':
				if ($_REQUEST['bookid']) {
					$book = new Book($_REQUEST['bookid']);
					if ($book->exists()) {
						$book_data = $book->fetchBookDetails();
						if (!$book->available()) {
							// Preapre book data
							$response['book']['bookid'] = $book_data['bookid'];
							$response['book']['name'] = $book_data['name'];
							$response['book']['author'] = $book_data['author'];
							$response['book']['category'] = $book_data['category'];
							// prepare fine and date details
							$latest_status = $book->getCurrentStatus();
							$response['book']['issue_date'] = date("d-M-Y", $latest_status['issue_date']);

							$returned_on = date('Y-m-d', time());
							$max_date = date('Y-m-d', $latest_status['expected_return_date']);
							$returned_on = strtotime($returned_on);
							$max_date = strtotime($max_date);
							$diff = $returned_on - $max_date;
							$fine_amount = 0;
							if ($diff>0) {
								$fine_amount = round($diff / (60 * 60 * 24)) * $lib->data['fine_per_day'];
							}	
							$response['book']['fine'] = $fine_amount;
						} else {
							$response['error'] = '2'; // not taken
						}
					} else {
						$response['error'] = '1'; // no such book
					}
				}
				break;

		case 'unassign':
		$response['success'] = '0';
				if ($_REQUEST['bookid'] && $_REQUEST['student_id']) {
					$book = new Book($_REQUEST['bookid']);
					$librarian = new Librarian($_SESSION['empid']);
					$librarian->setDb($db);
					$librarian->fetchDetails();
					$librarian->unassign($_REQUEST['bookid'], $_REQUEST['fine']);
					$response['success'] = '1';
				}
				break;

	}
}

print_r(json_encode($response));