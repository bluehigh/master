<?php
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\User;
use Scholarly\Librarian;
use Scholarly\Student;
use Scholarly\Book;


date_default_timezone_set("Asia/Kolkata");

session_start();

$db = new Handler();
Book::setDb($db);

$response['error'] = false;

if (isset($_REQUEST['action'])) {
	switch($_REQUEST['action']) {
		case 'view':
				if ($_REQUEST['studentID']) {
					$librarian = new Librarian($_SESSION['unique_id']);
          $student = new Student($_REQUEST['studentID']);
          $student->setDb($db);
          $student->fetchDetails();
          if ($student->exists()) {
            $response['studentName'] = $student->data['name'];
            $response['data'] = [];
            $librarian->setDb($db);
            $bookTransactions=$librarian->viewBookTransactions($_REQUEST['studentID']);
            if(count($bookTransactions)){
              foreach ($bookTransactions as $bktrans) {
                $book = new Book($bktrans['bookid']);
                $bookData = $book->fetchBookDetails();
                $col['DT_RowId'] = 'row_'.$bktrans['transactionid'];
                $col['transactionid'] = $bktrans['transactionid'];
                $col['student_id'] = $bktrans['student_id'];
                $col['bookname'] = $bookData['name'];

                $col['issue_date'] = date('m/d/Y', $bktrans['issue_date']);
                $col['return_date'] = $bktrans['return_date'];
                $col['expected_return_date'] = date('m/d/Y', $bktrans['expected_return_date']);
                $col['link'] = $bktrans['bookid'];
                $response['data'][] = $col;
              }
            }
          }


				}
				break;

	}
}

print_r(json_encode($response));
