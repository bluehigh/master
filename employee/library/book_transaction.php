<?php

# Project init file
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

if (!sessionSetFor('employee')) {
    session_destroy();
    header("Location: ".HOST."/employee/login.php");
}

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Librarian;

$db = new Handler();

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$librarian = new Librarian($_SESSION['empid']);
$librarian->setDb($db);
$librarian->fetchDetails();

if ($librarian->getAccessLevel() != 7) {
  header("Location: ".HOST."/employee/emp_route.php");
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Home </title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
    </script>
    <style type="text/css">
      #scanner_status {
        display: none;
        position: fixed;
        z-index: 9999999999999;
        bottom: 60px;
        right: 20px;
        width: 170px;
        padding-left: 17px;
        line-height: 40px;
        height: 40px;
        background-color: #00fb00a3;
        color: white;
        box-shadow: 5px 5px 10px rgba(0,0,0,0.2);
      }
      #book_card, #student_card {
        display: none;
      }
      #fetchbook, #fetch, #reset, #scan_a_book {
        display: none;
      }
      #no_student {
        display: none;
      }
      #book_error {
        color: red;
        display: none;
      }
    </style>
  </head>

  <body class="nav-md">
    <div id="scanner_status">
          <i class="fas fa-mobile-alt" style="font-size: 16px"></i> &nbsp;&nbsp;&nbsp; Connected
        </div>
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>

        <div class="right_col" role="main">
        <div class="">
            <div class="row">
              <div class="col-md-12">
                <div class="x_panel" >
                  <div class="x_title">
                    <h2><i class="fas fa-home"></i> &nbsp;&nbsp;Home</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div>

                        <div class="container-fluid">

                            <div class="row">
                              <div class="col-12">
                                <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                                    <div class="form-group" id="input_elements">
                                        <label class="col-sm-3 control-label">Student Id</label>

                                        <div class="col-sm-9">
                                          <div class="input-group">
                                            <input type="text" class="form-control" id="student_id">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-primary" id="scan">Scan</button>
                                                <button type="button" class="btn btn-primary" id="fetch">Fetch Student Details</button>
                                            </span>
                                          </div>
                                        </div>
                                        
                                      </div>
                                  </form>
                                  <div class="col-md-12" style="text-align: center">
                                      <button type="button" class="btn btn-primary" id="reset">Scan again</button>
                                  </div>
                              </div>

                              <div class="container-fluid">
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="row" style="text-align: center">
                                      <h5 id="no_student" style="color: red">Not a valid id</h5>
                                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 profile_details" style="text-align: left" id="student_card">
                                          <div class="well profile_view">
                                            <div class="col-sm-12">
                                              <div class="left col-xs-8">
                                                <h2 id="student_name">Nicole Pearson</h2>
                                                <p><strong>Curremtly having: <span id="active_book_count"></span> books</strong> </p>
                                                <ul class="list-unstyled">
                                                  <li><i class="fa fa-phone"></i> Phone #:<span id="student_phone"></span> </li>
                                                </ul>
                                              </div>
                                              <div class="right col-xs-4 text-center">
                                                <img src="images/avatar-male.png" alt="" class="img-circle img-responsive">
                                              </div>
                                            </div>
                                            <div class="col-xs-12 bottom text-center">
                                              
                                            </div>
                                          </div>
                                        </div>
                                         
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="col-12">
                                <h3 id="scan_a_book">Scan a book to unassign it</h3>
                                  <table id="BookTransactionTable" class="display" style="width:100%">
                                    <thead>
                                      <tr>
                                        <th>Transaction Id</th>
                                        <th>Book Name</th>
                                        <th>Issue Date</th>
                                        <th>Expected Return Date</th>
                                        <th>Unassign</th>
                                      </tr>
                                    </thead>
                                  </table>
                              </div>

                            </div>


                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        </div>


      </div>
    </div>

    <div class="modal fade bs-example-modal-lg in" tabindex="-1" role="dialog" aria-hidden="true" id="book_modal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Unassign Book</h4>
          </div>
          <div class="modal-body">
           <div class="container-fluid">
             <div class="row">
               <div class="col-md-5" style="text-align: center">
                 <img src="images/book1.png">
               </div>
               <div class="col-md-7" style="padding-left: 0px">
                 <h3>Book Name: <span id="modal_book_name">Java</span></h3>
                 <h4  style="margin-top: 60px">Book Author: <span id="modal_book_author">Schildt</span></h4>
                 <h4  style="margin-top: 10px">Taken on: <span id="modal_book_taken_on">10/12/2018</span></h4>
                 <h4  style="margin-top: 10px">Fine Amount: Rs <span id="modal_book_fine">0</span>.00</h4>
               </div>
             </div>
           </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success" id="unassign">Mark as Recieved</button>
          </div>

        </div>
      </div>
    </div>

    <?php require_once('components/scripts.php'); ?>
    <script type="text/javascript">

      var current_book;
      var current_fine_amount;
      student_id = false;
      books_id = [];
      function fetchStudentDetails() {
          $.ajax({
             url: host+'/app/api/employee/librarian/assign_book_data.php',
             method: 'POST',
             data: {
                 'empid': <?php echo $_SESSION['empid']; ?>,
                 'action': 'fetchStudent',
                 'student_id': $('#student_id').val()
                 },
                success: function(data) {
                    data = JSON.parse(data);
                    if (data['error'] == 1) {
                      $('#no_student').show();
                      $('#student_card').hide();
                      student_id = false;
                      book_id = false;
                    } else {
                      $('#no_student').hide();
                      $('#student_name').html(data['student']['name']);
                      $('#active_book_count').html(data['student']['active_book_no']);
                      $('#student_phone').html(data['student']['phone']);
                      $('#student_card').show();
                      student_id = $('#student_id').val();
                    }
                }

          });
      }

      function genTable(sid) {
        var studentId = sid;
        if(studentId!=''){
            table = $('#BookTransactionTable').DataTable( {
                 dom: "Bfrtip",
                 ajax: host+'/app/api/employee/librarian/view_book_transactions.php?action=view&studentID='+studentId,
                 responsive: true,
                 "columns": [
                   {"data" : "transactionid"},
                   {"data" : "bookname"},
                   {"data" : "issue_date"},
                   {"data" : "expected_return_date"},
                   {"data" : "link",
                      render : function(data,e,row){
                        books_id[data] = 1;
                        return "<a href='book_unassign.php?value="+data+"'><button type='button' class='btn'>Unassign Book</button></a>";
                      }
                   }
                 ],
                 buttons: []
              });
            console.log(books_id);
          }
      }

      function fetchBookData(fetched_book_id) {
        current_book = fetched_book_id;
        $.ajax({
           url: host+'/app/api/employee/librarian/unassign_book_data.php?bookid='+fetched_book_id,
           method: 'POST',
           data: {
               'empid': <?php echo $_SESSION['empid']; ?>,
               'action': 'fetchBook',
               'student_id': $('#student_id').val()
               },
              success: function(data) {
                  data = JSON.parse(data);
                  if (data['error'] == 1) {

                    // No such book
                    //   new PNotify({
                    //     title: 'Invalid BookId!',
                    //     type: 'error',
                    //     styling: 'bootstrap3'
                    // });
                  } else if(data['error'] == 2) {

                    // not taken
                    //   new PNotify({
                    //     title: 'Invalid BookId!',
                    //     type: 'error',
                    //     styling: 'bootstrap3'
                    // });
                  } else {
                    // prepare data
                    bookdata = data['book'];
                    $('#modal_book_name').html(bookdata['name']);
                    $('#modal_book_author').html(bookdata['author']);
                    $('#modal_book_taken_on').html(bookdata['issue_date']);
                    $('#modal_book_fine').html(bookdata['fine']);
                    current_fine_amount = bookdata['fine'];
                    // show modal
                    $('#book_modal').modal('show');
                  }
              }

        });
      }
        scanbook = false;
        scan = false;
        showerror = true;
        $(document).ready(function () {
            $('#scan').hide();
            $('#fetch').show();

            setInterval(function() {
              $.ajax({
                 url: host+'/app/api/employee/librarian/scanner_data.php?empid=<?php echo $_SESSION['empid']; ?>',
                 method: 'POST',
                 data: {
                     'pc_sync': true
                     },
                 success: function(data) {
                   data = JSON.parse(data);
                   if(data['connected']) {
                     $('#scanner_status').show();
                      $('#scan').show();
                      $('#fetch').hide();
                     if (data['data']) {
                      // $('#data').html("Last result: "+data['data']);
                      if (scan) {
                        $('#student_id').val(data['data']);
                        // verify Student data and fetch
                        fetchStudentDetails();
                        scanbook = false;
                        setTimeout(function() {
                          scanbook = true;
                        }, 10000);
                        genTable(data['data']);
                        $('#scan_a_book').show();
                        scan = false;
                        $('#input_elements').hide();
                        $('#reset').show();
                      } else if (scanbook) {
                        // Trigger ajax to load book info
                        if (books_id[data['data']]) {
                          // book exists in the student's list (stored in js)
                          fetchBookData(data['data']);
                        } else {
                          //  book does not exist in the student's list (stored in js)
                          if (showerror) {
                            showerror = false;
                            setTimeout(function() {
                              showerror = true;
                            }, 10000);
                              new PNotify({
                                title: 'Invalid BookId!',
                                type: 'error',
                                styling: 'bootstrap3'
                            });
                          }
                        }
                      } else {
                      $('#data').html('');
                     }
                   }
                 } else {

                  $('#scanner_status').hide();
                  
                  $('#scan').hide();
                  $('#fetch').show();
                 }
             }
              });
            }, 2000);

            $('#scan').click(function() {
              scan = true;
            });

            $('#student_id').keyup(function() {
              $('#scan').hide();
              $('#fetch').show();
            });

            $('#reset').click(function() {
              location.reload();
            });

            $('#unassign').click(function() {
                $.ajax({
                   url: host+'/app/api/employee/librarian/unassign_book_data.php?bookid='+current_book,
                   method: 'POST',
                   data: {
                       'fine': current_fine_amount,
                       'action': 'unassign',
                       'student_id': $('#student_id').val()
                       },
                      success: function(data) {
                          data = JSON.parse(data);
                          if (data['success'] == 1) {
                            $('#book_modal').modal('hide');
                            table.ajax.reload();
                          }
                      }

                });
            });
          });

    </script>
  </body>
</html>
