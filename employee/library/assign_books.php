<?php

# Project init file
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

if (!sessionSetFor('employee')) {
    session_destroy();
    header("Location: ".HOST."/employee/login.php");
}

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Librarian;

$db = new Handler();

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$librarian = new Librarian($_SESSION['empid']);
$librarian->setDb($db);
$librarian->fetchDetails();

if ($librarian->getAccessLevel() != 7) {
  header("Location: ".HOST."/employee/emp_route.php"); 
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Home </title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
    </script>
    <style type="text/css">
        #scanner_status {
          display: none;
          position: fixed;
          z-index: 9999999999999;
          bottom: 30px;
          right: 20px;
          width: 170px;
          padding-left: 17px;
          line-height: 40px;
          height: 40px;
          background-color: #00fb00a3;
          color: white;
          box-shadow: 5px 5px 10px rgba(0,0,0,0.2);
        }
        #book_card, #student_card {
          display: none;
        }
        #fetchbook, #fetch {
          display: none;
        }
        #no_student {
          display: none;
        }
        #book_error {
          color: red;
          display: none;
        }
    </style>
  <script src="<?php echo HOST."/assets/js/os.js";?>"></script>    
  </head>

  <body class="nav-md">
    <div id="scanner_status">
      <i class="fas fa-mobile-alt" style="font-size: 16px"></i> &nbsp;&nbsp;&nbsp; Connected
    </div>
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>

        <div class="right_col" role="main">
        <div class="">
            <div class="row">
              <div class="col-md-12">
                <div class="x_panel" >
                  <div class="x_title">
                    <h2><i class="fas fa-home"></i> &nbsp;&nbsp;Assign book</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div>
                        <div class="container-fluid">

                            <div class="row">
                              <div id="connected">Scanner Connected<div id="data"></div></div>
                              <div id="data"></div>

                              <div class="col-md-12 col-sm-12 col-xs-12">
                                              <div class="x_panel">
                                                <div class="x_title">
                                                  <h2>Student Information <small></small></h2>
                                                  
                                                  <div class="clearfix"></div>
                                                </div>
                                                <div class="x_content">
                                                  <br>
                                                  <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Student Id</label>

                                                        <div class="col-sm-9">
                                                          <div class="input-group">
                                                            <input type="text" class="form-control" id="student_id">
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-primary" id="scan">Scan</button>
                                                                <button type="button" class="btn btn-primary" id="fetch">Fetch Students</button>
                                                            </span>
                                                          </div>
                                                        </div>
                                                      </div>

                                                  </form>
                                                  <div class="container-fluid">
                                                    <div class="row">
                                                      <div class="col-md-12">
                                                        <div class="row" style="text-align: center">
                                                          <h5 id="no_student" style="color: red">Not a valid id</h5>
                                                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 profile_details" style="text-align: left" id="student_card">
                                                              <div class="well profile_view">
                                                                <div class="col-sm-12">
                                                                  <div class="left col-xs-8">
                                                                    <h2 id="student_name">Nicole Pearson</h2>
                                                                    <p><strong>Curremtly having: <span id="active_book_count"></span> books</strong> </p>
                                                                    <ul class="list-unstyled">
                                                                      <li><i class="fa fa-phone"></i> Phone #:<span id="student_phone"></span> </li>
                                                                    </ul>
                                                                  </div>
                                                                  <div class="right col-xs-4 text-center">
                                                                    <img src="images/avatar-male.png" alt="" class="img-circle img-responsive">
                                                                  </div>
                                                                </div>
                                                                <div class="col-xs-12 bottom text-center">
                                                                  
                                                                </div>
                                                              </div>
                                                            </div>
                                                             
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Book Id</label>

                                                        <div class="col-sm-9">
                                                          <div class="input-group">
                                                            <input type="text" class="form-control" id="bookid">
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-primary" id="scanbook">Scan</button>
                                                                <button type="button" class="btn btn-primary" id="fetchbook">Fetch Details</button>
                                                            </span>
                                                          </div>
                                                        </div>
                                                      </div>

                                                  </form>

                                                  <div class="container-fluid">
                                                    <div class="row">
                                                      <div class="col-md-12">
                                                        <div class="row" style="text-align: center">
                                                          <h4 id="book_error"></h4>
                                                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 profile_details" style="text-align: left" id="book_card">
                                                              <div class="well profile_view">
                                                                <div class="col-sm-12">
                                                                  
                                                                  <div class="left col-xs-8">
                                                                    <h2 id="book_name">Nicole Pearson</h2>
                                                                    <p>Author : <span id="book_author"></span></p>
                                                                    <ul class="list-unstyled">
                                                                      <li><i class="fa fa-building"></i> Category: <span id="book_category"></span> </li>
                                                                      <
                                                                    </ul>
                                                                  </div>
                                                                  <div class="right col-xs-4 text-center">
                                                                    <img src="images/avatar-male.png" alt="" class="img-circle img-responsive">
                                                                  </div>
                                                                </div>
                                                                <div class="col-xs-12 bottom text-center">
                                                                  
                                                                </div>
                                                              </div>
                                                            </div>
                                                             
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <div style="text-align: center">
                                                    <button type="button" class="btn btn-success btn-lg" id="assign_book">Assign Book</button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>



                            </div>


                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        </div>


      </div>
    </div>

    <?php require_once('components/scripts.php'); ?>
    <script type="text/javascript">

      student_id = false;
      book_id = false;
      function fetchStudentDetails() {
          $.ajax({
             url: host+'/app/api/employee/librarian/assign_book_data.php',
             method: 'POST',
             data: {
                 'empid': <?php echo $_SESSION['empid']; ?>,
                 'action': 'fetchStudent',
                 'student_id': $('#student_id').val()
                 },
                success: function(data) {
                    data = JSON.parse(data);
                    if (data['error'] == 1) {
                      $('#no_student').show();
                      $('#student_card').hide();
                      student_id = false;
                      book_id = false;
                    } else {
                      $('#no_student').hide();
                      $('#student_name').html(data['student']['name']);
                      $('#active_book_count').html(data['student']['active_book_no']);
                      $('#student_phone').html(data['student']['phone']);
                      $('#student_card').show();
                      student_id = $('#student_id').val();
                    }
                }

          });
      }
      function fetchBookDetails() {
         $.ajax({
             url: host+'/app/api/employee/librarian/assign_book_data.php',
             method: 'POST',
             data: {
                 'empid': <?php echo $_SESSION['empid']; ?>,
                 'action': 'fetchBook',
                 'bookid': $('#bookid').val()
                 },
                success: function(data) {
                    data = JSON.parse(data);
                    if (data['error'] == 1) {
                      $('#book_error').html('Invalid book id');
                      $('#book_error').show();
                      $('#book_card').hide();
                      book_id = false;
                    } else if (data['error'] == 2) {
                      $('#book_error').html('Book already taken');
                      $('#book_error').show();
                      $('#book_card').hide();
                      book_id = false;
                    } else {
                      $('#book_error').hide();
                      $('#book_name').html(data['book']['name']);
                      $('#book_author').html(data['book']['author']);
                      $('#book_category').html(data['book']['category']);
                      $('#book_card').show();
                      book_id = $('#bookid').val();
                    }
                }

          });
      }
      scan = false;
      scanbook = false;
        $(document).ready(function () {
          $('#connected').hide();
            setInterval(function() {
              $.ajax({
                 url: host+'/app/api/employee/librarian/scanner_data.php?empid=<?php echo $_SESSION['empid']; ?>',
                 method: 'POST',
                 data: {
                     'pc_sync': true
                     },
                 success: function(data) {
                   data = JSON.parse(data);
                   if(data['connected']) {
                     $('#scanner_status').show();
                      $('#scan').show();
                      $('#fetch').hide();
                      $('#scanbook').show();
                      $('#fetchbook').hide();
                     if (data['data']) {
                      // $('#data').html("Last result: "+data['data']);
                      if (scan) {
                        $('#student_id').val(data['data']);
                        // verify Student data and fetch
                        fetchStudentDetails();
                        scan = false;
                      } else if (scanbook) {
                        $('#bookid').val(data['data']);
                        // verify book data and fetch
                        fetchBookDetails();
                        scanbook = false;
                      }
                     } else {
                      $('#data').html('');
                     }
                   } else {
                    $('#scanner_status').hide();
                   }
                 }
             });
            }, 2000);

            $('#scan').click(function() {
              scan = true;
            });
            $('#scanbook').click(function() {
              scanbook = true;
            });
            
            $('#student_id').keyup(function() {
              $('#scan').hide();
              $('#fetch').show();
            });
            $('#bookid').keyup(function() {
              $('#scanbook').hide();
              $('#fetchbook').show();
            });

            $('#assign_book').click(function() {
              if (student_id && book_id) {
                  $.ajax({
                      url: host+'/app/api/employee/librarian/assign_book_data.php',
                      method: 'POST',
                      data: {
                          'empid': <?php echo $_SESSION['empid']; ?>,
                          'action': 'assignBook',
                          'bookid': book_id,
                          'student_id': student_id
                          },
                         success: function(data) {
                            data = JSON.parse(data);
                            if (data['success'] == 1) {
                                new PNotify({
                                    title: 'Assigned Successfully!',
                                    type: 'success',
                                    styling: 'bootstrap3'
                                });
                                setTimeout(function() {
                                  window.location.replace("assign_books.php");
                                }, 2000);
                            }
                         }

                    });
              } else {
                alert('Select a book and a student first')
              }
            });
          });
    </script>
  </body>
</html>