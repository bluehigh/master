<?php
# Project init file
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

if (!sessionSetFor('employee')) {
    session_destroy();
    header("Location: ".HOST."/employee/login.php");
}

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Hod;
use Scholarly\Teacher;
use Scholarly\Department;
use Scholarly\Course;
use Scholarly\Batch;

$db = new Handler();
Department::setDb($db);
Course::setDb($db);
Batch::setDb($db);

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$hod = new Hod($_SESSION['empid']);
$hod->setDb($db);
$hod->fetchDetails();

if ($hod->getAccessLevel() != 2) {
  header("Location: ".HOST."/employee/emp_route.php"); 
}
$current_batch_id = false;

$fetched_dept_ids = $institute->getDeptIds();

if (count($fetched_dept_ids)) {
  foreach ($fetched_dept_ids as $dept_id) {
    $dept = new Department($dept_id);
    $dept->fetchDetails();
    $fetched_course_ids = $dept->getCourseIds();

    if (count($fetched_course_ids)) {
      foreach ($fetched_course_ids as $course_id) {
        $course = new Course($course_id);
        $course->fetchDetails();

        $fetched_batch_ids = $course->getBatchIds();
        if (count($fetched_batch_ids)) {
          foreach ($fetched_batch_ids as $batch_id) {
            if ($batch_id == $_GET['batch_id']) {
            	$current_dept = $dept;
            	$current_course = $course;
             	$current_batch_id = $batch_id;
             	break 3;
            }
          }
        }
      }
    }
  }
}



if (!$current_batch_id) {
  header("Location: ".HOST."/employee/hod/courses.php");
}

$batch = new Batch($current_batch_id);
$batch->fetchDetails();

require ROOT."/app/helpers/institute_check.php";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Manage Students </title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
    </script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>
        <div class="right_col" role="main">
            <div class="">
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel" >
                    <div class="x_title">
                      <h2>Manage Students</h2>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      <div>
                          <div class="container-fluid">
                              <div class="row">
                                <div class="col-12" style="margin-bottom: 5%">
                                  <h4>Course : <?php echo $current_course->getName(); ?></h4>
                                  <h5>Department : <?php echo $current_dept->getName(); ?></h5>
                                  <h5>Batch : <?php echo $batch->starting_year." - ".$batch->ending_year; ?></h5>
                                </div>
                                  <div class="col-12">
                                      <table id="studentTable" class="display" style="width:100%">
                                        <thead>
                                          <tr>
                                            <th>Student ID</th>
                                            <th>Name</th>
                                            <th>U Roll</th>
                                            <th>U Reg</th>
                                            <th>Status</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Address</th>
                                          </tr>
                                        </thead>
                                      </table>
                                  </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>            
            </div>
        </div>

      </div>
    </div>

    <?php require_once('components/scripts.php'); ?>
    <script type="text/javascript">
        var editor;
        $(document).ready(function () {


          editor = new $.fn.dataTable.Editor({
            ajax: host+"/app/api/admin/student_data.php?batch_id=<?php echo $current_batch_id;?>",
            table: "#studentTable",
            fields: [{
                label: "Student Name:",
                name: "student.name"
              }, {
                label: "Univeristy Roll:",
                name: "student.u_roll"
              }, {
                label: "University Registration:",
                name: "student.u_reg"
              }, {
                label: "Status :",
                name: "student.status_id",
                type: 'select'
              }, {
                label: "Email Id:",
                name: "student.email"
              }, {
                label: "Phone:",
                name: "student.phone"
              }, {
                label: "Address:",
                name: "student.address"
              }
            ]
          });


          $('#studentTable').DataTable( {
            dom: "Bfrtip",
            ajax: host+"/app/api/admin/student_data.php?action=view&batch_id=<?php echo $current_batch_id;?>",
            responsive: true,
            "columns": [
              { "data": "student.id" },
              { "data": "student.name" },
              { "data": "student.u_roll" },
              { "data": "student.u_reg" },
              { "data": "status.name" },
              { "data": "student.email" },
              { "data": "student.phone" },
              { "data": "student.address" }
            ],
            select: 'single',
            buttons: [
               { extend: "create", editor: editor, text: "Register Student" },
               { extend: "edit", editor: editor, text: "Modify Details" }
            ]
          } );

          editor.on('onInitEdit', function() {
            editor.enable('student.status_id');
          });
          editor.on('onInitCreate', function() {
            editor.disable('student.status_id');
          });

          editor.on('onInitEdit', function() {
            editor.disable('student.u_reg');
          });
          editor.on('onInitCreate', function() {
            editor.enable('student.u_reg');
          });

          editor.on('onInitEdit', function() {
            editor.disable('student.u_roll');
          });
          editor.on('onInitCreate', function() {
            editor.enable('student.u_roll');
          });

          editor.on( 'preSubmit', function ( e, o, action ) {
              if ( action !== 'remove' ) {
                      return validateEditorFields(editor);
                  }
          } );


        });
    </script>
  </body>
</html>