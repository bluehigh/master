<?php

# Project init file
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

if (!sessionSetFor('institute')) {
    session_destroy();
    header("Location: ".HOST."/institute/login.php");
}

require ROOT."/app/helpers/institute_check.php";

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Manage Employees </title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
    </script>
    <style type="text/css">

    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>

        <div class="right_col" role="main">
	      <div class="">
	      		<div class="row">
	      		  <div class="col-md-12">
	      		    <div class="x_panel" >
	      		      <div class="x_title">
	      		        <h2>Manage Employees</h2>
	      		        <div class="clearfix"></div>
	      		      </div>
	      		      <div class="x_content">
	      		        <div>
	      		            <div class="container-fluid">
	      		                <div class="row">
	      		                    <div class="col-12">
	      		                       
	      		                    </div>
	      		                </div>
	      		            </div>
	      		        </div>
	      		      </div>
	      		    </div>
	      		  </div>
	      		</div>
	      </div>
      	</div>


      </div>
    </div>

    <?php require_once('components/scripts.php'); ?>
    <script type="text/javascript">


        $(document).ready(function () {


        });


    </script>
  </body>
</html>