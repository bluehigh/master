<?php

# Project init file
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

if (!sessionSetFor('institute')) {
    session_destroy();
    header("Location: ".HOST."/institute/dashboard/login.php");
}

require ROOT."/app/helpers/institute_check.php";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Manage Employees </title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
    </script>
    <style type="text/css">
        div.DTE_Field_Name_emp_type, div.DTE_Field_Name_dept, div.DTE_Field_Name_desig {
        padding-bottom: 20px!important;
        margin-bottom: 20px!important;
        border-bottom: 1px solid #ccc;
        }
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>

        <div class="right_col" role="main">
          <div class="">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel" >
                  <div class="x_title">
                    <h2>Manage Employees</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12">
                                    <table id="EmpTable" class="display" style="width:100%">
                                      <thead>
                                        <tr>
                                          <th>Type</th>
                                          <th>Empid</th>
                                          <th>Name</th>
                                          <th>Designation</th>
                                          <th>Email</th>
                                          <th>Phone</th>
                                          <th>Address</th>
                                        </tr>
                                      </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php require_once('components/scripts.php'); ?>
    <script type="text/javascript">
        var editor;
        $(document).ready(function () {
          editor = new $.fn.dataTable.Editor({
            ajax: host+"/app/api/admin/emp_data.php",
            table: "#EmpTable",
            fields: [{
                label: "Employee Name:",
                name: "emp.name"
              },
              {
                label: "Employee Email:",
                name: "emp.email"
              },
              {
                label: "Type:",
                name: "emp.emp_type",
                type: 'select'
              },
              {
                label: "Department:",
                name: "emp.dept_id",
                type: 'select'
              },
              {
                label: "Designation:",
                name: "emp.desig_id",
                type: 'select'
              },
              {
                label: "Employee Phone:",
                name: "emp.phone"
              },
              {
                label: "Employee Address:",
                name: "emp.address"
              }
            ]
          });
          $('#EmpTable').DataTable( {
            dom: "Bfrtip",
            ajax: host+"/app/api/admin/emp_data.php?action=view",
            responsive: true,
            "columns": [
              { "data": "type.name" },
              { "data": "emp.empid" },
              { "data": "emp.name" },
              { "data": "desig.name" },
              { "data": "emp.email" },
              { "data": "emp.phone"},
              { "data": "emp.address"}
            ],
             select: 'single',
             buttons: [
              { extend: "create", editor: editor },
              { extend: "edit", editor: editor },
              { extend: "remove", editor: editor }
            ]
          } );

          editor.on( 'preSubmit', function ( e, o, action ) {
              if ( action !== 'remove' ) {
                      //  Check if the designation is proper for the type
                      desig = editor.field('emp.desig_id');
                      type = editor.field('emp.emp_type');

                      if (type.val() == 'T' && desig.val() > 5 ) {
                        desig.error('Select proper designation for type teacher');
                        return false;
                      }

                      if (type.val() == 'N' && desig.val() < 6 ) {
                        desig.error('Select proper designation for a non-teaching staff');
                        return false;
                      }

                      return validateEditorFields(editor);
                  }
          } );

          editor.on('onInitEdit', function() {
            editor.disable('emp.dept_id');
          });
          editor.on('onInitCreate', function() {
            editor.enable('emp.dept_id');
          });

          editor.on('onInitEdit', function() {
            editor.disable('emp.emp_type');
          });
          editor.on('onInitCreate', function() {
            editor.enable('emp.emp_type');
          });

        });


    </script>
  </body>
</html>
