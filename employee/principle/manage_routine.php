<?php
# Project init file
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

if (!sessionSetFor('employee')) {
    session_destroy();
    header("Location: ".HOST."/employee/login.php");
}

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Hod;
use Scholarly\Teacher;
use Scholarly\Department;
use Scholarly\Course;
use Scholarly\Batch;

$db = new Handler();
Department::setDb($db);
Course::setDb($db);
Batch::setDb($db);

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$hod = new Hod($_SESSION['empid']);
$hod->setDb($db);
$hod->fetchDetails();

if ($hod->getAccessLevel() != 2) {
  header("Location: ".HOST."/employee/emp_route.php"); 
}

$current_batch_id = false;

$fetched_dept_ids = $institute->getDeptIds();

if (count($fetched_dept_ids)) {
  foreach ($fetched_dept_ids as $dept_id) {
    $dept = new Department($dept_id);
    $dept->fetchDetails();
    $fetched_course_ids = $dept->getCourseIds();

    if (count($fetched_course_ids)) {
      foreach ($fetched_course_ids as $course_id) {
        $course = new Course($course_id);
        $course->fetchDetails();

        $fetched_batch_ids = $course->getBatchIds();
        if (count($fetched_batch_ids)) {
          foreach ($fetched_batch_ids as $batch_id) {
            if ($batch_id == $_GET['batch_id']) {
              $current_dept = $dept;
              $current_course = $course;
              $current_batch_id = $batch_id;
              break 3;
            }
          }
        }
      }
    }
  }
}



if (!$current_batch_id) {
  header("Location: ".HOST."/employee/hod/courses.php");
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Manage Routine </title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <style type="text/css">
      .clearall {
        background: red!important;
        color: white!important;
        border: 1px solid red!important; 
      }
    </style>
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
    </script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>
        <div class="right_col" role="main">
            <div class="">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel" >
                      <div class="x_title">
                        <h2>Manage Routine</h2>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <div>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12">
                                    <div class="">
                                          <div class="x_panel">
                                            
                                            <div class="x_content">

                                              <div class="col-md-2 col-sm-12">
                                                <!-- required for floating -->
                                                <!-- Nav tabs -->
                                                <ul class="nav nav-tabs tabs-left">
                                                  <li class="active"><a href="#monday" data-toggle="tab">Monday</a>
                                                  </li>
                                                  <li><a href="#tuesday" data-toggle="tab">Tuesday</a>
                                                  </li>
                                                  <li><a href="#wednesday" data-toggle="tab">Wednesday</a>
                                                  </li>
                                                  <li><a href="#thursday" data-toggle="tab">Thursday</a>
                                                  </li>
                                                  <li><a href="#friday" data-toggle="tab">Friday</a>
                                                  </li>
                                                  <li><a href="#saturday" data-toggle="tab">Saturday</a>
                                                  </li>
                                                </ul>
                                              </div>

                                              <div class="col-md-10">
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                  <div class="tab-pane active" id="monday">
                                                    <p class="lead">Monday</p>
                                                    <div>
                                                      <table id="mondayTable" class="display" style="width:100%">
                                                        <thead>
                                                          <tr>
                                                            <th>Starts</th>
                                                            <th>Ends</th>
                                                            <th>Subject Code</th>
                                                            <th>Subject Name</th>
                                                            <th>Assigned Teacher</th>
                                                          </tr>
                                                        </thead>
                                                      </table>
                                                    </div>
                                                  </div>
                                                  <div class="tab-pane" id="tuesday">
                                                    <p class="lead">Tuesday</p>
                                                    <div>
                                                      <table id="tuesdayTable" class="display" style="width:100%">
                                                        <thead>
                                                          <tr>
                                                            <th>Starts</th>
                                                            <th>Ends</th>
                                                            <th>Subject Code</th>
                                                            <th>Subject Name</th>
                                                            <th>Assigned Teacher</th>
                                                          </tr>
                                                        </thead>
                                                      </table>
                                                    </div>
                                                  </div>
                                                  <div class="tab-pane" id="wednesday">
                                                    <p class="lead">Wednesday</p>
                                                    <div>
                                                      <table id="wednesdayTable" class="display" style="width:100%">
                                                        <thead>
                                                          <tr>
                                                            <th>Starts</th>
                                                            <th>Ends</th>
                                                            <th>Subject Code</th>
                                                            <th>Subject Name</th>
                                                            <th>Assigned Teacher</th>
                                                          </tr>
                                                        </thead>
                                                      </table>
                                                    </div>
                                                  </div>
                                                  <div class="tab-pane" id="thursday">
                                                    <p class="lead">Thursday</p>
                                                    <div>
                                                      <table id="thursdayTable" class="display" style="width:100%">
                                                        <thead>
                                                          <tr>
                                                            <th>Starts</th>
                                                            <th>Ends</th>
                                                            <th>Subject Code</th>
                                                            <th>Subject Name</th>
                                                            <th>Assigned Teacher</th>
                                                          </tr>
                                                        </thead>
                                                      </table>
                                                    </div>
                                                  </div>
                                                  <div class="tab-pane" id="friday">
                                                    <p class="lead">Friday</p>
                                                    <div>
                                                      <table id="fridayTable" class="display" style="width:100%">
                                                        <thead>
                                                          <tr>
                                                            <th>Starts</th>
                                                            <th>Ends</th>
                                                            <th>Subject Code</th>
                                                            <th>Subject Name</th>
                                                            <th>Assigned Teacher</th>
                                                          </tr>
                                                        </thead>
                                                      </table>
                                                    </div>
                                                  </div>
                                                  <div class="tab-pane" id="saturday">
                                                    <p class="lead">Saturday</p>
                                                    <div>
                                                      <table id="saturdayTable" class="display" style="width:100%">
                                                        <thead>
                                                          <tr>
                                                            <th>Starts</th>
                                                            <th>Ends</th>
                                                            <th>Subject Code</th>
                                                            <th>Subject Name</th>
                                                            <th>Assigned Teacher</th>
                                                          </tr>
                                                        </thead>
                                                      </table>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>

                                              <div class="clearfix"></div>

                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>            
            </div>
        </div>

      </div>
    </div>

    <?php require_once('components/scripts.php'); ?>
    <script type="text/javascript">

        
        var time_span = new Array();
        time_span[1] = new Array(86400).fill(0);
        time_span[2] = new Array(86400).fill(0);
        time_span[3] = new Array(86400).fill(0);
        time_span[4] = new Array(86400).fill(0);
        time_span[5] = new Array(86400).fill(0);
        time_span[6] = new Array(86400).fill(0);

        function initSubmitCreate(editor, day) {

          starts = editor.field('class.starts');
          ends = editor.field('class.ends');

          if (starts.val() == '' || (starts.val().split(':')).length !=2 ) {
            starts.error('Input proper time in HH:MM format');
            return false;
          }
          if (ends.val() == '' || (ends.val().split(':')).length !=2 ) {
            ends.error('Input proper time in HH:MM format');
            return false;
          }

          starts_in_seconds = convertToSeconds(starts.val());
          starts.set(String(starts_in_seconds));
          ends_in_seconds = convertToSeconds(ends.val());
          ends.set(String(ends_in_seconds));
          editor.submit();

          if (time_span[day][starts.val()]) {
            starts.error('Another period is already assigned at this time');
            starts_in_seconds = convertToTime(starts.val());
            starts.set(String(starts_in_seconds));
            ends_in_seconds = convertToTime(ends.val());
            ends.set(String(ends_in_seconds));
            return false;
          }

          if (time_span[day][ends.val()]) {
            ends.error('Another period is already assigned at this time');
            starts_in_seconds = convertToTime(starts.val());
            starts.set(String(starts_in_seconds));
            ends_in_seconds = convertToTime(ends.val());
            ends.set(String(ends_in_seconds));
            return false;
          }

          for (i = starts.val(); i <= ends.val(); i++) {
            if (time_span[day][i]) {
              editor.error('Other period exists within this time span');
              starts_in_seconds = convertToTime(starts.val());
              starts.set(String(starts_in_seconds));
              ends_in_seconds = convertToTime(ends.val());
              ends.set(String(ends_in_seconds));

              return false;
            }

            return true;
          }

          if (starts.val()>=ends.val()) {
            editor.error("Invalid time span");
            starts_in_seconds = convertToTime(starts.val());
            starts.set(String(starts_in_seconds));
            ends_in_seconds = convertToTime(ends.val());
            ends.set(String(ends_in_seconds));
            return false;
          }
        }

        function initSubmitEdit(editor) {
          starts = editor.field('class.starts');
          ends = editor.field('class.ends');

          starts_in_seconds = convertToSeconds(starts.val());
          starts.set(String(starts_in_seconds));
          ends_in_seconds = convertToSeconds(ends.val());
          ends.set(String(ends_in_seconds));
          editor.submit();
        }

        function postSubmit(editor) {
            starts = editor.field('class.starts');
            ends = editor.field('class.ends');

            starts_in_seconds = convertToTime(starts.val());
            starts.set(String(starts_in_seconds));
            ends_in_seconds = convertToTime(ends.val());
            ends.set(String(ends_in_seconds));
        }

        function preOpen(editor) {
            starts = editor.field('class.starts');
            ends = editor.field('class.ends');

            starts_in_seconds = convertToTime(starts.val());
            starts.set(String(starts_in_seconds));
            ends_in_seconds = convertToTime(ends.val());
            ends.set(String(ends_in_seconds));
        }

        function preSubmit(editor) {
            starts = editor.field('class.starts');
            ends = editor.field('class.ends');

            return validateEditorFields(editor);
        }
        $(document).ready(function () {

          editor1 = new $.fn.dataTable.Editor({
            ajax: host+"/app/api/employee/hod/routine_data.php?batch_id=<?php echo $current_batch_id;?>&day=1",
            table: "#mondayTable",
            fields: [{
                label: "Starts:",
                name: "class.starts",
                type:  'datetime',
                format: 'HH:mm',
                fieldInfo: '24 hour clock format',
                default: ''
              }, {
                label: "Ends:",
                name: "class.ends",
                type: 'select',
                type:  'datetime',
                format: 'HH:mm',
                fieldInfo: '24 hour clock format',
                default: ''
              }, {
                label: "Subject Code:",
                name: "class.sub_code",
                type: "select"
              }, {
                label: "Assigned Teacher:",
                name: "class.empid",
                type: "select"
              }
            ]
          });


          $('#mondayTable').DataTable( {
            dom: "Bfrtip",
            ajax: host+"/app/api/employee/hod/routine_data.php?action=view&batch_id=<?php echo $current_batch_id;?>&day=1",
            responsive: true,
            "rowCallback": function( row, data, index ) { 
                for (i = data['class']['starts']; i <= data['class']['ends']; i++) {
                    time_span[1][i] = 1;
                }
              },
            "columns": [
              { "data": "class.starts", 
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.ends",
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.sub_code" },
              { "data": "class.sub_name" },
              { "data": "emp.name" }
            ],
            select: 'single',
            buttons: [
               { extend: "create", editor: editor1, text: "Add a period" },
               { extend: "edit", editor: editor1, text: "Modify period" },
               { extend: "remove", editor: editor1, text: "Remove period" },
               { editor: editor1, text: 'Clear all', className: 'clearall',action: function() {

                   $.ajax({
                      url: host+"/app/api/employee/hod/routine_data.php?batch_id=<?php echo $current_batch_id;?>&day=1",
                      method: 'POST',
                      data: {
                            'action': 'clearall'
                          },
                      success: function() {
                         location.reload();
                      }
                  });
               }}
            ]
          } );

          editor1.on('onInitEdit', function() {
            editor1.disable('class.starts');
          });
          editor1.on('onInitCreate', function() {
            editor1.enable('class.starts');
          });

          editor1.on('onInitEdit', function() {
            editor1.disable('class.ends');
          });
          editor1.on('onInitCreate', function() {
            editor1.enable('class.ends');
          });

          editor1.on( 'preOpen', function ( e, o, action ) {
              if ( action == 'edit' ) {
                    preOpen(editor1);
                  }
          } );

          editor1.on( 'preSubmit', function ( e, o, action ) {
              if ( action !== 'remove' ) {
                      preSubmit(editor1);
                  }
          } );

          editor1.on( 'postSubmit', function ( e, o, action ) {
              if ( action !== 'remove' ) {
                      postSubmit(editor1);
                  }
          } );

          editor1.on( 'initSubmit', function ( e,  action ) {
              if ( action == 'create' ) {
                    if (!initSubmitCreate(editor1, 1)) {
                      return false;
                    }
                  } else if ( action == 'edit' ) {
                    initSubmitEdit(editor1);
                  }
          } );
          /*        Editor-1 Starts      */

          /*        Editor-1 Ends      */


          /*        Editor-2 Starts      */
          editor2 = new $.fn.dataTable.Editor({
            ajax: host+"/app/api/employee/hod/routine_data.php?batch_id=<?php echo $current_batch_id;?>&day=2",
            table: "#tuesdayTable",
            fields: [{
                label: "Starts:",
                name: "class.starts",
                type:  'datetime',
                format: 'HH:mm',
                fieldInfo: '24 hour clock format',
                default: ''
              }, {
                label: "Ends:",
                name: "class.ends",
                type: 'select',
                type:  'datetime',
                format: 'HH:mm',
                fieldInfo: '24 hour clock format',
                default: ''
              }, {
                label: "Subject Code:",
                name: "class.sub_code",
                type: "select"
              }, {
                label: "Assigned Teacher:",
                name: "class.empid",
                type: "select"
              }
            ]
          });


          $('#tuesdayTable').DataTable( {
            dom: "Bfrtip",
            ajax: host+"/app/api/employee/hod/routine_data.php?action=view&batch_id=<?php echo $current_batch_id;?>&day=2",
            responsive: true,
            "rowCallback": function( row, data, index ) { 
                for (i = data['class']['starts']; i <= data['class']['ends']; i++) {
                    time_span[2][i] = 1;
                }
              },
            "columns": [
              { "data": "class.starts", 
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.ends",
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.sub_code" },
              { "data": "class.sub_name" },
              { "data": "emp.name" }
            ],
            select: 'single',
            buttons: [
               { extend: "create", editor: editor2, text: "Add a period" },
               { extend: "edit", editor: editor2, text: "Modify period" },
               { extend: "remove", editor: editor2, text: "Remove period" },
               { editor: editor2, text: 'Clear all', className: 'clearall',action: function() {

                   $.ajax({
                      url: host+"/app/api/employee/hod/routine_data.php?batch_id=<?php echo $current_batch_id;?>&day=2",
                      method: 'POST',
                      data: {
                            'action': 'clearall'
                          },
                      success: function() {
                         location.reload();
                      }
                  });
               }}
            ]
          } );

          editor2.on('onInitEdit', function() {
            editor2.disable('class.starts');
          });
          editor2.on('onInitCreate', function() {
            editor2.enable('class.starts');
          });

          editor2.on('onInitEdit', function() {
            editor2.disable('class.ends');
          });
          editor2.on('onInitCreate', function() {
            editor2.enable('class.ends');
          });

          editor2.on( 'preOpen', function ( e, o, action ) {
              if ( action == 'edit' ) {
                    preOpen(editor2);
                  }
          } );

          editor2.on( 'preSubmit', function ( e, o, action ) {
              if ( action !== 'remove' ) {
                      preSubmit(editor2);
                  }
          } );

          editor2.on( 'postSubmit', function ( e, o, action ) {
              if ( action !== 'remove' ) {
                      postSubmit(editor2);
                  }
          } );

          editor2.on( 'initSubmit', function ( e,  action ) {
              if ( action == 'create' ) {
                    if (!initSubmitCreate(editor2, 2)) {
                      return false;
                    }
                  } else if ( action == 'edit' ) {
                    initSubmitEdit(editor2);
                  }
          } );
          /*        Editor-2 Ends      */


          /*        Editor-3 Starts      */
          editor3 = new $.fn.dataTable.Editor({
            ajax: host+"/app/api/employee/hod/routine_data.php?batch_id=<?php echo $current_batch_id;?>&day=3",
            table: "#wednesdayTable",
            fields: [{
                label: "Starts:",
                name: "class.starts",
                type:  'datetime',
                format: 'HH:mm',
                fieldInfo: '24 hour clock format',
                default: ''
              }, {
                label: "Ends:",
                name: "class.ends",
                type: 'select',
                type:  'datetime',
                format: 'HH:mm',
                fieldInfo: '24 hour clock format',
                default: ''
              }, {
                label: "Subject Code:",
                name: "class.sub_code",
                type: "select"
              }, {
                label: "Assigned Teacher:",
                name: "class.empid",
                type: "select"
              }
            ]
          });


          $('#wednesdayTable').DataTable( {
            dom: "Bfrtip",
            ajax: host+"/app/api/employee/hod/routine_data.php?action=view&batch_id=<?php echo $current_batch_id;?>&day=3",
            responsive: true,
            "rowCallback": function( row, data, index ) { 
                for (i = data['class']['starts']; i <= data['class']['ends']; i++) {
                    time_span[3][i] = 1;
                }
              },
            "columns": [
              { "data": "class.starts", 
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.ends",
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.sub_code" },
              { "data": "class.sub_name" },
              { "data": "emp.name" }
            ],
            select: 'single',
            buttons: [
               { extend: "create", editor: editor3, text: "Add a period" },
               { extend: "edit", editor: editor3, text: "Modify period" },
               { extend: "remove", editor: editor3, text: "Remove period" },
               { editor: editor3, text: 'Clear all', className: 'clearall',action: function() {

                   $.ajax({
                      url: host+"/app/api/employee/hod/routine_data.php?batch_id=<?php echo $current_batch_id;?>&day=3",
                      method: 'POST',
                      data: {
                            'action': 'clearall'
                          },
                      success: function() {
                         location.reload();
                      }
                  });
               }}
            ]
          } );

          editor3.on('onInitEdit', function() {
            editor3.disable('class.starts');
          });
          editor3.on('onInitCreate', function() {
            editor3.enable('class.starts');
          });

          editor3.on('onInitEdit', function() {
            editor3.disable('class.ends');
          });
          editor3.on('onInitCreate', function() {
            editor3.enable('class.ends');
          });

          editor3.on( 'preOpen', function ( e, o, action ) {
              if ( action == 'edit' ) {
                    preOpen(editor3);
                  }
          } );

          editor3.on( 'preSubmit', function ( e, o, action ) {
              if ( action !== 'remove' ) {
                      preSubmit(editor3);
                  }
          } );

          editor3.on( 'postSubmit', function ( e, o, action ) {
              if ( action !== 'remove' ) {
                      postSubmit(editor3);
                  }
          } );

          editor3.on( 'initSubmit', function ( e,  action ) {
              if ( action == 'create' ) {
                    if (!initSubmitCreate(editor3, 3)) {
                      return false;
                    }
                  } else if ( action == 'edit' ) {
                    initSubmitEdit(editor3);
                  }
          } );
          /*        Editor-3 Ends      */


          /*        Editor-4 Starts      */
          editor4 = new $.fn.dataTable.Editor({
            ajax: host+"/app/api/employee/hod/routine_data.php?batch_id=<?php echo $current_batch_id;?>&day=4",
            table: "#thursdayTable",
            fields: [{
                label: "Starts:",
                name: "class.starts",
                type:  'datetime',
                format: 'HH:mm',
                fieldInfo: '24 hour clock format',
                default: ''
              }, {
                label: "Ends:",
                name: "class.ends",
                type: 'select',
                type:  'datetime',
                format: 'HH:mm',
                fieldInfo: '24 hour clock format',
                default: ''
              }, {
                label: "Subject Code:",
                name: "class.sub_code",
                type: "select"
              }, {
                label: "Assigned Teacher:",
                name: "class.empid",
                type: "select"
              }
            ]
          });


          $('#thursdayTable').DataTable( {
            dom: "Bfrtip",
            ajax: host+"/app/api/employee/hod/routine_data.php?action=view&batch_id=<?php echo $current_batch_id;?>&day=4",
            responsive: true,
            "rowCallback": function( row, data, index ) { 
                for (i = data['class']['starts']; i <= data['class']['ends']; i++) {
                    time_span[4][i] = 1;
                }
              },
            "columns": [
              { "data": "class.starts", 
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.ends",
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.sub_code" },
              { "data": "class.sub_name" },
              { "data": "emp.name" }
            ],
            select: 'single',
            buttons: [
               { extend: "create", editor: editor4, text: "Add a period" },
               { extend: "edit", editor: editor4, text: "Modify period" },
               { extend: "remove", editor: editor4, text: "Remove period" },
               { editor: editor4, text: 'Clear all', className: 'clearall',action: function() {

                   $.ajax({
                      url: host+"/app/api/employee/hod/routine_data.php?batch_id=<?php echo $current_batch_id;?>&day=4",
                      method: 'POST',
                      data: {
                            'action': 'clearall'
                          },
                      success: function() {
                         location.reload();
                      }
                  });
               }}
            ]
          } );

          editor4.on('onInitEdit', function() {
            editor4.disable('class.starts');
          });
          editor4.on('onInitCreate', function() {
            editor4.enable('class.starts');
          });

          editor4.on('onInitEdit', function() {
            editor4.disable('class.ends');
          });
          editor4.on('onInitCreate', function() {
            editor4.enable('class.ends');
          });

          editor4.on( 'preOpen', function ( e, o, action ) {
              if ( action == 'edit' ) {
                    preOpen(editor4);
                  }
          } );

          editor4.on( 'preSubmit', function ( e, o, action ) {
              if ( action !== 'remove' ) {
                      preSubmit(editor4);
                  }
          } );

          editor4.on( 'postSubmit', function ( e, o, action ) {
              if ( action !== 'remove' ) {
                      postSubmit(editor4);
                  }
          } );

          editor4.on( 'initSubmit', function ( e,  action ) {
              if ( action == 'create' ) {
                    if (!initSubmitCreate(editor4, 4)) {
                      return false;
                    }
                  } else if ( action == 'edit' ) {
                    initSubmitEdit(editor4);
                  }
          } );
          /*        Editor-4 Ends      */


          /*        Editor-5 Starts      */
          editor5 = new $.fn.dataTable.Editor({
            ajax: host+"/app/api/employee/hod/routine_data.php?batch_id=<?php echo $current_batch_id;?>&day=5",
            table: "#fridayTable",
            fields: [{
                label: "Starts:",
                name: "class.starts",
                type:  'datetime',
                format: 'HH:mm',
                fieldInfo: '24 hour clock format',
                default: ''
              }, {
                label: "Ends:",
                name: "class.ends",
                type: 'select',
                type:  'datetime',
                format: 'HH:mm',
                fieldInfo: '24 hour clock format',
                default: ''
              }, {
                label: "Subject Code:",
                name: "class.sub_code",
                type: "select"
              }, {
                label: "Assigned Teacher:",
                name: "class.empid",
                type: "select"
              }
            ]
          });


          $('#fridayTable').DataTable( {
            dom: "Bfrtip",
            ajax: host+"/app/api/employee/hod/routine_data.php?action=view&batch_id=<?php echo $current_batch_id;?>&day=5",
            responsive: true,
            "rowCallback": function( row, data, index ) { 
                for (i = data['class']['starts']; i <= data['class']['ends']; i++) {
                    time_span[5][i] = 1;
                }
              },
            "columns": [
              { "data": "class.starts", 
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.ends",
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.sub_code" },
              { "data": "class.sub_name" },
              { "data": "emp.name" }
            ],
            select: 'single',
            buttons: [
               { extend: "create", editor: editor5, text: "Add a period" },
               { extend: "edit", editor: editor5, text: "Modify period" },
               { extend: "remove", editor: editor5, text: "Remove period" },
               { editor: editor5, text: 'Clear all', className: 'clearall',action: function() {

                   $.ajax({
                      url: host+"/app/api/employee/hod/routine_data.php?batch_id=<?php echo $current_batch_id;?>&day=5",
                      method: 'POST',
                      data: {
                            'action': 'clearall'
                          },
                      success: function() {
                         location.reload();
                      }
                  });
               }}
            ]
          } );

          editor5.on('onInitEdit', function() {
            editor5.disable('class.starts');
          });
          editor5.on('onInitCreate', function() {
            editor5.enable('class.starts');
          });

          editor5.on('onInitEdit', function() {
            editor5.disable('class.ends');
          });
          editor5.on('onInitCreate', function() {
            editor5.enable('class.ends');
          });

          editor5.on( 'preOpen', function ( e, o, action ) {
              if ( action == 'edit' ) {
                    preOpen(editor5);
                  }
          } );

          editor5.on( 'preSubmit', function ( e, o, action ) {
              if ( action !== 'remove' ) {
                      preSubmit(editor5);
                  }
          } );

          editor5.on( 'postSubmit', function ( e, o, action ) {
              if ( action !== 'remove' ) {
                      postSubmit(editor5);
                  }
          } );

          editor5.on( 'initSubmit', function ( e,  action ) {
              if ( action == 'create' ) {
                    if (!initSubmitCreate(editor5, 5)) {
                      return false;
                    }
                  } else if ( action == 'edit' ) {
                    initSubmitEdit(editor5);
                  }
          } );
          /*        Editor-5 Ends      */

          /*        Editor-6 Starts      */
          editor6 = new $.fn.dataTable.Editor({
            ajax: host+"/app/api/employee/hod/routine_data.php?batch_id=<?php echo $current_batch_id;?>&day=6",
            table: "#saturdayTable",
            fields: [{
                label: "Starts:",
                name: "class.starts",
                type:  'datetime',
                format: 'HH:mm',
                fieldInfo: '24 hour clock format',
                default: ''
              }, {
                label: "Ends:",
                name: "class.ends",
                type: 'select',
                type:  'datetime',
                format: 'HH:mm',
                fieldInfo: '24 hour clock format',
                default: ''
              }, {
                label: "Subject Code:",
                name: "class.sub_code",
                type: "select"
              }, {
                label: "Assigned Teacher:",
                name: "class.empid",
                type: "select"
              }
            ]
          });


          $('#saturdayTable').DataTable( {
            dom: "Bfrtip",
            ajax: host+"/app/api/employee/hod/routine_data.php?action=view&batch_id=<?php echo $current_batch_id;?>&day=6",
            responsive: true,
            "rowCallback": function( row, data, index ) { 
                for (i = data['class']['starts']; i <= data['class']['ends']; i++) {
                    time_span[6][i] = 1;
                }
              },
            "columns": [
              { "data": "class.starts", 
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.ends",
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.sub_code" },
              { "data": "class.sub_name" },
              { "data": "emp.name" }
            ],
            select: 'single',
            buttons: [
               { extend: "create", editor: editor6, text: "Add a period" },
               { extend: "edit", editor: editor6, text: "Modify period" },
               { extend: "remove", editor: editor6, text: "Remove period" },
               { editor: editor6, text: 'Clear all', className: 'clearall',action: function() {

                   $.ajax({
                      url: host+"/app/api/employee/hod/routine_data.php?batch_id=<?php echo $current_batch_id;?>&day=6",
                      method: 'POST',
                      data: {
                            'action': 'clearall'
                          },
                      success: function() {
                         location.reload();
                      }
                  });
               }}
            ]
          } );

          editor6.on('onInitEdit', function() {
            editor6.disable('class.starts');
          });
          editor6.on('onInitCreate', function() {
            editor6.enable('class.starts');
          });

          editor6.on('onInitEdit', function() {
            editor6.disable('class.ends');
          });
          editor6.on('onInitCreate', function() {
            editor6.enable('class.ends');
          });

          editor6.on( 'preOpen', function ( e, o, action ) {
              if ( action == 'edit' ) {
                    preOpen(editor6);
                  }
          } );

          editor6.on( 'preSubmit', function ( e, o, action ) {
              if ( action !== 'remove' ) {
                      preSubmit(editor6);
                  }
          } );

          editor6.on( 'postSubmit', function ( e, o, action ) {
              if ( action !== 'remove' ) {
                      postSubmit(editor6);
                  }
          } );

          editor6.on( 'initSubmit', function ( e,  action ) {
              if ( action == 'create' ) {
                    if (!initSubmitCreate(editor6, 6)) {
                      return false;
                    }
                  } else if ( action == 'edit' ) {
                    initSubmitEdit(editor6);
                  }
          } );
          /*        Editor-6 Ends      */

        });
    </script>
  </body>
</html>