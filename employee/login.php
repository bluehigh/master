<?php

# Project init file
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;

session_start();

$name = '';
$id = '';

if (isset($_GET['logout'])) {
    session_destroy();
    header("Location: ".HOST."/employee/login.php");
}

# If institute id is not passed
if (isset($_GET['unique_id'])) {
    $db = new Handler();
    
    $institute = new Institute($_GET['unique_id']);
    $institute->setDb($db);
    $institute->fetchDetails();

    $name = $institute->get('institute_name')['institute_name'];
    $id = $institute->getId();
}


if(sessionSetFor('employee')){
  header("Location: ".HOST."/employee/emp_route.php");
}


?>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <!--        Bootstrap css        -->
        <link href="<?php echo HOST; ?>/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="<?php echo HOST; ?>/assets/css/bootstrap-grid.css" rel="stylesheet" type="text/css">

        <!--            aos            -->
        <link href="<?php echo HOST; ?>/assets/css/aos.css" rel="stylesheet">

        <!--        `Custom css        -->
        <link href="<?php echo HOST; ?>/assets/css/global.css" rel="stylesheet" type="text/css">
        <link href="<?php echo HOST; ?>/assets/css/employee_login.css" rel="stylesheet" type="text/css">
    </head>

    <body>
        <div id="preloader_parent">
              <div id="preloader-content">
                  <img src="<?php echo HOST; ?>/assets/images/preloader.gif">
              </div>
        </div>
        <div class="container-fluid" id="landing">
           <div class="row">
                <div class="col-md-8" id="landing_logo_parent">
                    <div class="row justify-content-center">
                        <div class="col-5" id="main_logo" data-aos="slide-up" data-aos-duration="1000">
                            <img src="<?php echo HOST; ?>/assets/images/employee_login_main_logo.png" class="img-fluid">
                        </div>
                    </div>
                </div>
                <div class="col-md-4" id="form_section" data-aos="fade-left" data-aos-delay="1400">
                    <div class="row justify-content-center">
                        <div class="col-3">
                            <img src="<?php echo HOST; ?>/assets/images/college_logo.png" class="img-fluid">
                        </div>
                    </div>
                     <div class="row justify-content-center">
                        <div class="col-12 center">
                            <h6 class="college_name blue"><?php echo $name; ?></h6>
                        </div>
                    </div>
                    <div class="row justify-content-center" id="form_parent">
                        <!--                login form              -->
                        <div class="col-10 center" id="login_form_parent">
                            <input type="text" name="employee_id_login" id="employee_id_login" class="input" placeholder="Employee ID" value="<?php echo $id; ?>">
                            <input type="password" name="password_login" id="password_login" class="input" placeholder="Password">
                            <button type="button" class="btn" id="login_btn">Login</button>
                            <div class="container-fluid form_links_parent">
                                <div class="row center">
                                    <div class="col-6 center">
                                        <a href="#" class="red links form_links otp_received">OTP Recieved?</a>
                                    </div>
                                    <div class="col-6 center">
                                        <a href="#" class="red links form_links reset_link">Reset Password</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--                /login form              -->
                        <!--                      reset form                          -->
                        <div class="col-10 center" id="reset_form_parent">
                            <input type="text" name="employee_id_reset" id="employee_id_reset" class="input" placeholder="Employee ID">
                            <input type="text" name="email_reset" id="email_reset" class="input" placeholder="Registered Email ID">
                            <button type="button" class="btn login_btn" id="reset_btn">Send OTP</button>
                            <div class="container-fluid form_links_parent">
                                <div class="row center">
                                    <div class="col-6 center">
                                        <a href="#" class="red links form_links otp_received">OTP Recieved?</a>
                                    </div>
                                    <div class="col-6 center">
                                        <a href="#" class="red links form_links normal_login">Normal Login</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--                      /reset form                          -->
                        <!--                      otp form                          -->
                        <div class="col-10 center" id="otp_form_parent">
                            <input type="text" name="employee_id_otp" id="employee_id_otp" class="input" placeholder="Employee ID">
                            <input type="password" name="otp" id="otp" class="input" placeholder="Enter OTP Here">
                            <button type="button" class="btn" id="procced_btn">Procced</button>
                            <div class="container-fluid form_links_parent">
                                <div class="row center">
                                    <div class="col-6 center">
                                        <a href="#" class="red links form_links normal_login">Normal Login</a>
                                    </div>
                                    <div class="col-6 center">
                                        <a href="#" class="red links form_links reset_link">Reset Password</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--                      /otp form                          -->
                        <!--                      change passwprd form                          -->
                        <div class="col-10 center" id="change_password_form_parent">
                            <input type="password" name="new_password" id="new_password" class="input" placeholder="New Password">
                            <input type="password" name="retype_password" id="retype_password" class="input" placeholder="Retype Password">
                            <button type="button" class="btn save_btn">Save</button>
                            <div class="container-fluid form_links_parent">
                                <div class="row center">
                                    <div class="col-12 center">
                                        <a href="#" class="red links form_links normal_login">Normal Login</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--                      /change password form                          -->
                        <div class="container">
                                <div class="row justify-content-center">
                                    <div class="center" id="error_display">

                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
           </div>
        </div>

        <!--        Scripts        -->
        <script src="<?php echo HOST; ?>/assets/js/jquery-3.3.1.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/bootstrap.min.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/aos.js"></script>

        <!--        Custom Scripts       -->
        <script src="<?php echo HOST; ?>/assets/js/main.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/employee_login.js"></script>
    <!--        <script src="assets/js/main.js"></script>-->
    </body>
</html>
