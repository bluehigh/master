<?php

# Project init file
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';


use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Department;
use Scholarly\Employee;


session_start();

if (!sessionSetFor('institute')) {
    session_destroy();
    header("Location: ".HOST."/institute/dashboard/login.php");
}

require ROOT."/app/helpers/institute_check.php";

# Check if dept id is provided or not
if (!isset($_GET['dept_id'])) {
  // If dept_id is not provided
  header("Location: ".HOST."/institute/dashboard/manage_teachers_select_dept.php");
}

# Check if the course_id is valid for that institute
$db = new Handler();
Department::setDb($db);
$current_dept = false;

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$fetched_dept_ids = $institute->getDeptIds();

foreach ($fetched_dept_ids as $dept_id) {
  if ($dept_id == $_GET['dept_id']) {
    $current_dept = $_GET['dept_id'];
  }
}

if (!$current_dept) {
  // If dept_id is not provided
  header("Location: ".HOST."/institute/dashboard/manage_teachers_select_dept.php");
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title><?php ?> </title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
    </script>
    <style type="text/css">
        div.DTE_Field_Name_emp_type, div.DTE_Field_Name_dept, div.DTE_Field_Name_desig {
        padding-bottom: 20px!important;
        margin-bottom: 20px!important;
        border-bottom: 1px solid #ccc;
        }
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>
        <div class="right_col" role="main">
            <div class="">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel" >
                      <div class="x_title">
                        <h2>Manage Employees</h2>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <div>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12">
                                        <table id="EmpTable" class="display" style="width:100%">
                                          <thead>
                                            <tr>
                                              <th>Empid</th>
                                              <th>Name</th>
                                              <th>Designation</th>
                                            </tr>
                                          </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>            
            </div>
        </div>

      </div>
    </div>

    <?php require_once('components/scripts.php'); ?>
    <script type="text/javascript">
        $(document).ready(function () {
          $('#EmpTable').DataTable( {
            dom: "Bfrtip",
            ajax: host+"/app/api/admin/view_dept_emp.php?action=view&dept_id=<?php echo $current_dept; ?>",
            responsive: true,
            "columns": [
              { "data": "emp.empid" },
              { "data": "emp.name" },
              { "data": "desig.name" }
            ],
             buttons: [
            ]
          } );

        });


    </script>
  </body>
</html>
