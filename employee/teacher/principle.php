<?php

# Project init file
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

if (!sessionSetFor('institute')) {
    session_destroy();
    header("Location: ".HOST."/institute/login.php");
}

require ROOT."/app/helpers/institute_check.php";

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Principle;
use Scholarly\Teacher;
use Scholarly\Department;

$db = new Handler();
Department::setDb($db);

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$principle_id = $institute->getPrincipleId();

$error = false;

if (isset($_POST['teachers'], $_POST['password_check'])) {
	# Check if error is there
	$new_emp_id = $_POST['teachers'];
	$password = $_POST['password_check'];

	if ($institute->checkPass($password)) {
		if ($institute->principleExists()) {
			if ($institute->updatePrinciple($new_emp_id)) {
				$success = true;
			}
		} else {
			if ($institute->addPrinciple($new_emp_id)) {
				$success = true;
			}
		}
		
	} else {
		$error = "Password does not match";
	}
}

$principle_id = $institute->getPrincipleId();

$fetched_dept_ids = $institute->getDeptIds();
$dept_details = [];
foreach ($fetched_dept_ids as $dept_id) {
  $dept = new Department($dept_id);
  $dept->fetchDetails();
  $dept_details[$dept->getId()] = $dept->getName();
}

$all_teacher_ids = $institute->getAllTeacherIds();

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Manage Role </title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
    </script>
    <style type="text/css">

    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>

        <div class="right_col" role="main">
	      <div class="">
	      		<div class="row">
	      		  <div>
	      		    <div class="x_panel" >
	      		      <div class="x_title">
	      		        <h2> <i class="fas fa-shield-alt"></i> Manage Role : Principle</h2>
	      		        <div class="clearfix"></div>
	      		      </div>
	      		      <div class="x_content">
	      		        <div>
	      		            <div class="container-fluid">
	      		                <div class="row">
	      		                    <div class="col-12">
	      		                    	<?php
	      		                    		# If principle exists
	      		                    		if ($principle_id) {

	      		                    			$fetched_desigs = $institute->getAllDesignations();

	      		                    			$principle = new Principle($principle_id);
	      		                    			$principle->setDb($db);
	      		                    			$principle_data = $principle->fetchEmpDataDetails();
	      		                    			?>
	      		                    			<!-- Current Profile view -->
	      		                    			<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3 profile_details">
	            			                        <div class="well profile_view">
	            			                          <div class="col-sm-12">
	            			                            <h4 class="brief"><i><?php echo $fetched_desigs[$principle_data['desig_id']] ; ?></i></h4>
	            			                            <div class="left col-xs-7">
	            			                              <h2><?php echo $principle_data['name'] ; ?></h2>
	            			                              <p><strong>Department: </strong><?php echo $dept_details[$principle_data['dept_id']] ; ?></p>
	            			                              <ul class="list-unstyled" style="margin-top: 2em">
	            			                              	<li><i class="fa fa-envelope"></i> Email : <?php echo $principle_data['email'] ; ?> </li>
	            			                                <li><i class="fa fa-building"></i> Address: <?php echo $principle_data['address'] ; ?></li>
	            			                                <li><i class="fa fa-phone"></i> Phone #: <?php echo $principle_data['phone'] ; ?> </li>
	            			                              </ul>
	            			                            </div>
	            			                            <div class="right col-xs-5 text-center" style="padding-left: 2em">
	            			                              <img src="images/avatar-male.png" alt="" class="img-circle img-responsive" width="100px" height="80px">
	            			                            </div>
	            			                          </div>
	            			                          <div class="col-xs-12 bottom text-center">
	            			                            <div class="col-xs-12 col-sm-6 emphasis">
	            			                              
	            			                            </div>
	            			                            <div class="col-xs-12 col-sm-6 emphasis">
	            			                              <button type="button" class="btn btn-danger btn-xs">
	            			                                <i class="fa fa-times"> </i> Remove
	            			                              </button>
	            			                            </div>
	            			                          </div>
	            			                        </div>
	            			                    </div>
	            			                      <!-- /Current Profile view -->

  	            			                    <!-- Change -->
  	            			                    <div class="col-md-12 col-sm-12 col-xs-12">
      			                                    <div class="x_panel">
      			                                      <div class="x_title">
      			                                        <h2>Change </h2>
      			                                        <ul class="nav navbar-right panel_toolbox">
      			                                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
      			                                          </li>
      			                                          </li>
      			                                        </ul>
      			                                        <div class="clearfix"></div>
      			                                      </div>
      			                                      <div class="x_content">

      			                                        <form class="form-horizontal form-label-left" action="<?php echo $_SERVER["PHP_SELF"];?>" method="post">

      			                                          <div class="form-group">
      			                                            <label for="heard">Select an employee from the list:</label>
  																<select name="teachers" class="form-control" required="">
  			                                                           <option value="">Choose..</option>
      			                                            <?php
      			                                            if (count($all_teacher_ids)) {
      			                                            	foreach ($all_teacher_ids as $teacher_id) {
      			                                            		$teacher = new Teacher($teacher_id);
      			                                            		$teacher->setDb($db);
      			                                            		$teacher_data = $teacher->fetchEmpDataDetails();
      			                                            		?>
  			                                                            <option value="<?php echo $teacher_data['empid']?>"><?php echo $teacher_data['name']; ?></option>
      			                                            		<?php
      			                                            	}
      			                                            }
      			                                            ?>
      			                                            </select>
      			                                            

      			                                          </div>
      			                                          <div class="divider-dashed"></div>

      			                                          <div class="form-group">
      			                                            <label class="col-sm-3 control-label">Enter Admin Password</label>

      			                                            <div class="col-sm-9">

      			                                              <div class="input-group">
      			                                                <input type="password" class="form-control" name="password_check" required="">
      			                                                <span class="input-group-btn">
      			                                                  <input class="btn btn-primary" type="submit" value="Change!" name="change_principle">
      			                                                </span>
      			                                              </div>
      			                                            </div>
      			                                          </div>
      			                                        </form>
      			                                      </div>
      			                                    </div>
      			                                </div>
  	            			                    <!-- /Change -->	      		                    			<?php
	      		                    		} else {

	      		                    			?>

	            			                    <!-- Change -->
	            			                    <div class="col-md-12 col-sm-12 col-xs-12">
    			                                    <div class="x_panel">
    			                                      <div class="x_title">
    			                                        <h2>Assign</h2>
    			                                        <ul class="nav navbar-right panel_toolbox">
    			                                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
    			                                          </li>
    			                                          </li>
    			                                        </ul>
    			                                        <div class="clearfix"></div>
    			                                      </div>
    			                                      <div class="x_content">

    			                                        <form class="form-horizontal form-label-left" action="<?php echo $_SERVER["PHP_SELF"];?>" method="post">

    			                                          <div class="form-group">
    			                                            <label for="heard">Select an employee from the list:</label>
																<select name="teachers" class="form-control" required="">
			                                                           <option value="">Choose..</option>
    			                                            <?php
    			                                            if (count($all_teacher_ids)) {
    			                                            	foreach ($all_teacher_ids as $teacher_id) {
    			                                            		$teacher = new Teacher($teacher_id);
    			                                            		$teacher->setDb($db);
    			                                            		$teacher_data = $teacher->fetchEmpDataDetails();
    			                                            		?>
			                                                            <option value="<?php echo $teacher_data['empid']?>"><?php echo $teacher_data['name']; ?></option>
    			                                            		<?php
    			                                            	}
    			                                            }
    			                                            ?>
    			                                            </select>
    			                                            

    			                                          </div>
    			                                          <div class="divider-dashed"></div>

    			                                          <div class="form-group">
    			                                            <label class="col-sm-3 control-label">Enter Admin Password</label>

    			                                            <div class="col-sm-9">

    			                                              <div class="input-group">
    			                                                <input type="password" class="form-control" name="password_check" required="">
    			                                                <span class="input-group-btn">
    			                                                  <input class="btn btn-primary" type="submit" value="Change!" name="change_principle">
    			                                                </span>
    			                                              </div>
    			                                            </div>
    			                                          </div>
    			                                        </form>
    			                                      </div>
    			                                    </div>
    			                                </div>
	            			                    <!-- /Change -->
	      		                    			<?php
	      		                    		}
	      		                    	?>
	      		                    </div>
	      		                </div>
	      		            </div>
	      		        </div>
	      		      </div>
	      		    </div>
	      		  </div>
	      		</div>
	      </div>
      	</div>


      </div>
    </div>

    <?php require_once('components/scripts.php'); ?>
    <script type="text/javascript">


        $(document).ready(function () {
        	<?php
        		if ($error) {
        			?>
    					new PNotify({
    				      title: 'Operation not successful!',
    				      text: 'Password does not match.',
    				      type: 'error',
    				      styling: 'bootstrap3'
    				  });
        			<?php
        		}
        		if (isset($success)) {
        	?>
				new PNotify({
			      title: 'Role updated!',
			      text: 'New Principle Assigned.',
			      type: 'success',
			      styling: 'bootstrap3'
			  });
			<?php
		}
        ?>
        });


    </script>
  </body>
</html>