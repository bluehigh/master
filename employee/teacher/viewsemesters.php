<?php
# Project init file
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

if (!sessionSetFor('employee')) {
    session_destroy();
    header("Location: ".HOST."/employee/login.php");
}

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Hod;
use Scholarly\Teacher;
use Scholarly\Department;
use Scholarly\Course;
use Scholarly\Batch;

$db = new Handler();
Department::setDb($db);
Course::setDb($db);
Batch::setDb($db);

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$hod = new Hod($_SESSION['empid']);
$hod->setDb($db);
$hod->fetchDetails();

if ($hod->getAccessLevel() != 2) {
  header("Location: ".HOST."/employee/emp_route.php"); 
}
# Check if course id is provided or not
if (!isset($_GET['course_id'])) {
  // If course_id is not provided, rediret to course management page
  header("Location: ".HOST."/institute/dashboard/courses.php");
}

$fetched_dept_ids = $institute->getDeptIds();

foreach ($fetched_dept_ids as $dept_id) {
  $dept = new Department($dept_id);
  $dept->fetchDetails();
  $fetched_course_ids = $dept->getCourseIds();

  foreach ($fetched_course_ids as $course_id) {
    if ($_GET['course_id'] == $course_id) {
      $current_course = $_GET['course_id']; 
      break 2;
    }
  }
}

if (!$current_course) {
  header("Location: ".HOST."/institute/dashboard/courses.php");
}

$course = new Course($current_course);
$course->fetchDetails();

require ROOT."/app/helpers/institute_check.php";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Semesters </title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
    </script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>
        <div class="right_col" role="main">
            <div class="">
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel" >
                    <div class="x_title">
                      <h2>Semesters for course : <?php echo $course->getName(); ?></h2>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      <div>
                          <div class="container-fluid">
                              <div class="row">
                                  <div class="col-12">
                                      <table id="semTable" class="display" style="width:100%">
                                        <thead>
                                          <tr>
                                            <th>Semester No</th>
                                            <th>Manage</th>
                                          </tr>
                                        </thead>
                                      </table>
                                  </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>            
            </div>
        </div>

      </div>
    </div>

    <?php require_once('components/scripts.php'); ?>
    <script type="text/javascript">
        var editor;
        $(document).ready(function () {
          $('#semTable').DataTable( {
            dom: "Bfrtip",
            ajax: host+"/app/api/admin/sem_data.php?action=view&course_id=<?php echo $current_course;?>",
            responsive: true,
            "columns": [
              { "data": "sem_no",
                "render" : function(data, type, row) {
                    return "Semester - "+data;
                }
              },
              { "data": "sem_id",
                "render" : function(data, type, row) {
                    return "<a href='"+host+"/employee/hod/viewsubjects.php?sem_id="+data+"'><button type='button' class='btn'>View Subjects</button></a>";
                }
              }
            ],
              "buttons" : []
          } );
        });
    </script>
  </body>
</html>