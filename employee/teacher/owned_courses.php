<?php

# Project init file
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

if (!sessionSetFor('employee')) {
    session_destroy();
    header("Location: ".HOST."/employee/login.php");
}

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Hod;
use Scholarly\Teacher;
use Scholarly\Department;
use Scholarly\Course;
use Scholarly\Batch;

$db = new Handler();
Department::setDb($db);
Course::setDb($db);
Batch::setDb($db);

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$hod = new Hod($_SESSION['empid']);
$hod->setDb($db);
$hod->fetchDetails();

if ($hod->getAccessLevel() != 2) {
  header("Location: ".HOST."/employee/emp_route.php"); 
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Owned Courses </title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
    </script>
    <style type="text/css">
        /* FontAwesome for working BootSnippet :> */

        @import url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
        #team {
            background: #eee !important;
        }

        .btn-primary:hover,
        .btn-primary:focus {
            background-color: #108d6f;
            border-color: #108d6f;
            box-shadow: none;
            outline: none;
        }

        .btn-primary {
            color: #fff;
            background-color: #2A3E53;
            border-color: #2A3E53;
        }

        section {
            padding: 60px 0;
        }

        section .section-title {
            text-align: center;
            color: #2A3E53;
            margin-bottom: 50px;
            text-transform: uppercase;
        }

        #team .card {
            border: none;
            background: #ffffff;
        }

        .image-flip:hover .backside,
        .image-flip.hover .backside {
            -webkit-transform: rotateY(0deg);
            -moz-transform: rotateY(0deg);
            -o-transform: rotateY(0deg);
            -ms-transform: rotateY(0deg);
            transform: rotateY(0deg);
            border-radius: .25rem;
        }

        .image-flip:hover .frontside,
        .image-flip.hover .frontside {
            -webkit-transform: rotateY(180deg);
            -moz-transform: rotateY(180deg);
            -o-transform: rotateY(180deg);
            transform: rotateY(180deg);
        }

        .mainflip {
            -webkit-transition: 1s;
            -webkit-transform-style: preserve-3d;
            -ms-transition: 1s;
            -moz-transition: 1s;
            -moz-transform: perspective(1000px);
            -moz-transform-style: preserve-3d;
            -ms-transform-style: preserve-3d;
            transition: 1s;
            transform-style: preserve-3d;
            position: relative;
        }

        .frontside {
            position: relative;
            -webkit-transform: rotateY(0deg);
            -ms-transform: rotateY(0deg);
            z-index: 2;
            margin-bottom: 30px;
        }

        .backside {
            position: absolute;
            top: 0;
            left: 0;
            background: white;
            -webkit-transform: rotateY(-180deg);
            -moz-transform: rotateY(-180deg);
            -o-transform: rotateY(-180deg);
            -ms-transform: rotateY(-180deg);
            transform: rotateY(-180deg);
            -webkit-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
            -moz-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
            box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
        }

        .frontside,
        .backside {
            -webkit-backface-visibility: hidden;
            -moz-backface-visibility: hidden;
            -ms-backface-visibility: hidden;
            backface-visibility: hidden;
            -webkit-transition: 1s;
            -webkit-transform-style: preserve-3d;
            -moz-transition: 1s;
            -moz-transform-style: preserve-3d;
            -o-transition: 1s;
            -o-transform-style: preserve-3d;
            -ms-transition: 1s;
            -ms-transform-style: preserve-3d;
            transition: 1s;
            transform-style: preserve-3d;
        }

        .frontside .card,
        .backside .card {
            min-height: 312px;
        }

        .backside .card a {
            font-size: 18px;
            color: #2A3E53 !important;
        }

        .frontside .card .card-title,
        .backside .card .card-title {
            color: #2A3E53 !important;
        }

        .frontside .card .card-body img {
            width: 120px;
            height: 120px;
            border-radius: 50%;
        }
        .course_card {
          box-shadow: 2px 2px 5px rgba(0,0,0,0.29);
        }
        .circle {
            top: 4px;
            color: white;
            background-color: #2A3E53;
            width: 108px;
            height: 108px;
            border-radius: 50%;
            line-height: 108px;
            font-size: 25px;
            text-align: center;
            cursor: pointer;
            z-index: 999;
            margin: auto;
            margin-top: 20px;
            margin-bottom: 30px;
        }
        .hovermanage {
          margin-top: 1.8em;
          border: none;
          width: 80%;
          background-color: #2A3E53;
          color: white;
        }
        .managebtn {
          margin: auto;
          margin-top: 1.8em;
          border: none;
          width: 80%;
          background-color: #2A3E53;
          color: white;
          transition: 0.4s;
        }
        .managebtn:hover {
          margin: auto;
          margin-top: 1.8em;
          border: none;
          width: 100%;
          background-color: #2A3E53;
          color: white;
        }
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>

        <div class="right_col" role="main">
	      <div class="">
	      		<div class="row">
	      		  <div class="col-md-12">
	      		    <div class="x_panel" >
	      		      <div class="x_title">
	      		        <h2>Courses Managed by You</h2>
	      		        <div class="clearfix"></div>
	      		      </div>
	      		      <div class="x_content">
	      		        <div>
	      		            <div class="container-fluid">
	      		                <div class="row">
	      		                    <div class="col-12">
                                  <?php
                                      $own_course_ids = $hod->getAssignedCourseIds();
                                      foreach ($own_course_ids as $course_id) {
                                        $course = new Course($course_id);
                                        $course->fetchDetails();
                                        ?>
                                   <!-- Card -->
                                   <div class="col-xs-12 col-sm-6 col-md-4">
                                       <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                                           <div class="mainflip">
                                               <div class="frontside">
                                                   <div class="card">
                                                       <div class="card-body text-center">
                                                           <div class="circle"><?php echo strtoupper($course->getCode());?></div>
                                                           <h3 class="card-title"><?php echo ucwords($course->getName());?></h3>
                                                           <button type="button" class="btn hovermanage">Hover To Manage</button>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="backside">
                                                   <div class="card">
                                                       <div class="card-body text-center mt-4">
                                                           <h4 class="card-title" style="padding: 0px 20px"><?php echo ucwords($course->getName());?></h4>
                                                           <div class="col-xs-12">
                                                             <div class="col-6"><h4 class="card-title"><span style="font-size: 180%"><?php echo $course->countActiveStudents();?></span><br>Active Students</h4></div>
                                                           </div>
                                                            <div class="col-xs-12">
                                                              <a href="batches.php?course_id=<?php echo $course->getId(); ?>"><button type="button" class="btn managebtn">Manage Students</button></a>
                                                            </div>
                                                            <div class="col-xs-12">
                                                              <a href="manage_teachers.php?course_id=<?php echo $course->getId(); ?>"><button type="button" class="btn managebtn">Manage Teachers</button></a>
                                                            </div>
                                                            <div class="col-xs-12">
                                                              <a href="manage_routine_select_batch.php?course_id=<?php echo $course->getId(); ?>"><button type="button" class="btn managebtn">Manage Schedules</button></a>
                                                            </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                   <!-- /Card -->
                                        <?php
                                      }
                                  ?>
	      		                    </div>
	      		                </div>
	      		            </div>
	      		        </div>
	      		      </div>
	      		    </div>
	      		  </div>
	      		</div>
	      </div>
      	</div>


      </div>
    </div>

    <?php require_once('components/scripts.php'); ?>
    <script type="text/javascript">


        $(document).ready(function () {


        });


    </script>
  </body>
</html>