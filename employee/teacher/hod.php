<?php

# Project init file
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

if (!sessionSetFor('institute')) {
    session_destroy();
    header("Location: ".HOST."/institute/login.php");
}

require ROOT."/app/helpers/institute_check.php";

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Hod;
use Scholarly\Teacher;
use Scholarly\Department;
use Scholarly\Course;

$db = new Handler();
Department::setDb($db);
Course::setDb($db);

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();


if (isset($_POST['add_hod'], $_POST['password_check'])) {
  if ($institute->checkPass($_POST['password_check'])) {
    $new_hod_id = $_POST['new_hod_id'];
    $course_id = $_POST['course_id'];

    $course = new Course($course_id);
    $course->fetchDetails();

    $course->addHod($new_hod_id);
    $success = $course->getName();
  } else {
    $error = true;
  }
}
if (isset($_POST['update_hod'], $_POST['password_check'])) {
  if ($institute->checkPass($_POST['password_check'])) {
    $new_hod_id = $_POST['new_hod_id'];
    $course_id = $_POST['course_id'];

    $course = new Course($course_id);
    $course->fetchDetails();

    $course->addHod($new_hod_id);
    $success = $course->getName();
  } else {
    $error = true;
  }
}

$fetched_desigs = $institute->getAllDesignations();

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Manage Hods </title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
    </script>
    <style type="text/css">

    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>

        <div class="right_col" role="main">
        <div class="">
            <div class="row">
              <div class="col-md-12">
                <div class="x_panel" >
                  <div class="x_title">
                    <h2>Manage Head of the Departments</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12">
                                   <?php
                                      $fetched_dept_ids = $institute->getDeptIds();

                                      if (count($fetched_dept_ids)) {
                                        foreach ($fetched_dept_ids as $dept_id) {

                                          $dept = new Department($dept_id);
                                          $dept->fetchDetails();
                                          $dept_details[$dept->getId()] = $dept->getName();
                                          $fetched_course_ids = $dept->getCourseIds();


                                          if (count($fetched_course_ids)) {
                                            foreach ($fetched_course_ids as $course_id) {
                                              
                                              $course = new Course($course_id);
                                              $course->fetchDetails();

                                              if (isset($teachers_of_course)) {
                                                unset($teachers_of_course);
                                              }
                                              if($course->hasHod()) {
                                                  
                                                  $hod_id = $course->getHodId();
                                                  $hod = new Hod($hod_id);
                                                  $hod->setDb($db);
                                                  $hod_details = $hod->fetchEmpDataDetails();
                                                  
                                                  $all_teachers_of_this_course = $course->getAllEmpIds();

                                                  foreach ($all_teachers_of_this_course as $teacher_id) {
                                                    if ($hod_id != $teacher_id) {
                                                      $teacher = new Teacher($teacher_id);
                                                      $teacher->setDb($db);
                                                      $data = $teacher->fetchEmpDataDetails();
                                                      if ($data['dept_id'] == $dept->getId()) {
                                                        $teachers_of_course[$data['empid']] = $data['name'];  
                                                      }
                                                       
                                                    }
                                                  }
                                                  /*  { Process cards  */
                                                  ?>
                                                  <div class="col-md-6 col-sm-6 col-xs-12 profile_details">
                                                    <div class="well profile_view">
                                                      <div class="col-sm-12">
                                                        <h2 class="brief"><?php echo $course->getName() ; ?></h2>
                                                        <div class="left col-xs-7">
                                                          <h2><?php echo $hod_details['name'] ; ?></h2>
                                                          <p><strong>Department: </strong><?php echo $dept->getName(); ?></p>
                                                          <ul class="list-unstyled" style="margin-top: 2em">
                                                            <li><i class="fa fa-envelope"></i> Email : <?php echo $hod_details['email'] ; ?> </li>
                                                            <li><i class="fa fa-building"></i> Address: <?php echo $hod_details['address'] ; ?></li>
                                                            <li><i class="fa fa-phone"></i> Phone #: <?php echo $hod_details['phone'] ; ?> </li>
                                                          </ul>
                                                        </div>
                                                        <div class="right col-xs-5 text-center" style="padding-left: 2em">
                                                          <img src="images/avatar-male.png" alt="" class="img-circle img-responsive" width="100px" height="80px">
                                                        </div>
                                                      </div>
                                                      <div class="col-xs-12 bottom text-center">
                                                        <div class="col-xs-12 col-sm-12 emphasis">
                                                          <form id="demo-form" action="<?php echo $_SERVER["PHP_SELF"];?>" method="post">
                                                            <div class="form-group">
                                                              <select name="new_hod_id" class="form-control" required="">
                                                                <option value="">Change...</option>
                                                                <?php
                                                                  if (count($teachers_of_course)) {
                                                                    foreach ($teachers_of_course as $emp_id => $emp_name) {
                                                                      ?>
                                                                        <option value="<?php echo $emp_id; ?>"><?php echo $emp_name; ?></option>
                                                                      <?php
                                                                    }
                                                                  }
                                                                ?>
                                                              </select>
                                                            </div>
                                                            <input type="hidden" name="course_id" value="<?php echo $course->getId(); ?>">
                                                            <div class="form-group">

                                                              <div class="col-sm-12">

                                                                <div class="input-group">
                                                                  <input type="password" class="form-control" name="password_check" required="" placeholder="Enter Admin Password">
                                                                  <span class="input-group-btn">
                                                                    <input class="btn btn-primary" type="submit" value="Update" name="update_hod">
                                                                  </span>
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </form>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                                  <?php
                                                 /*  Process cards }  */
                                              } else {
                                                $all_teachers_of_this_course = $course->getAllEmpIds();

                                                foreach ($all_teachers_of_this_course as $teacher_id) {
                                                    $teacher = new Teacher($teacher_id);
                                                    $teacher->setDb($db);
                                                    $data = $teacher->fetchEmpDataDetails();
                                                    if ($data['dept_id'] == $dept->getId()) {
                                                      $teachers_of_course[$data['empid']] = $data['name'];  
                                                    }
                                                  }
                                                /*  { Assign Hod Box  */
                                                ?>
                                                  <div class="col-md-6 col-sm-6 col-xs-12 profile_details">
                                                      <div class="bs-example" data-example-id="simple-jumbotron">
                                                      <div class="jumbotron" style="text-align: center">
                                                        <h3><?php echo $course->getName() ; ?></h3>
                                                        <h4 style="margin-top: 30px">Not Assigned</h4>
                                                        <div class="container-fluid">
                                                          <div class="row" style="padding-top: 2em ">
                                                                <div class="col-xs-12 col-sm-12">
                                                                  <form id="demo-form" action="<?php echo $_SERVER["PHP_SELF"];?>" method="post">
                                                                    <div class="form-group">
                                                                      <select name="new_hod_id" class="form-control" required="">
                                                                        <option value="">Select...</option>
                                                                        <?php
                                                                          if (count($teachers_of_course)) {
                                                                            foreach ($teachers_of_course as $emp_id => $emp_name) {
                                                                              ?>
                                                                                <option value="<?php echo $emp_id; ?>"><?php echo $emp_name; ?></option>
                                                                              <?php
                                                                            }
                                                                          }
                                                                        ?>
                                                                      </select>
                                                                    </div>
                                                                    <input type="hidden" name="course_id" value="<?php echo $course->getId(); ?>">
                                                                    <div class="form-group">

                                                                      <div class="col-sm-12">

                                                                        <div class="input-group">
                                                                          <input type="password" class="form-control" name="password_check" required="" placeholder="Enter Admin Password">
                                                                          <span class="input-group-btn">
                                                                            <input class="btn btn-primary" type="submit" value="Assign" name="add_hod">
                                                                          </span>
                                                                        </div>
                                                                      </div>
                                                                    </div>
                                                                  </form>
                                                                </div>
                                                          </div>
                                                        </div>
                                                        
                                                      </div>
                                                    </div>
                                                  </div>
                                                  
                                                <?php
                                                /*  Assign Hod Box }  */
                                              }

                                            }
                                          }
                                        }
                                      }
                                      
                                   ?>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        </div>


      </div>
    </div>

    <?php require_once('components/scripts.php'); ?>
    <script type="text/javascript">


        $(document).ready(function () {
              <?php
                if (isset($error)) {
                  ?>
                  new PNotify({
                      title: 'Operation not successful!',
                      text: 'Password does not match.',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
                  <?php
                }
                if (isset($success)) {
              ?>
            new PNotify({
                title: 'Role updated!',
                text: 'Role updated for <?php echo $success; ?>.',
                type: 'success',
                styling: 'bootstrap3'
            });
          <?php
        }
            ?>
        });


    </script>
  </body>
</html>