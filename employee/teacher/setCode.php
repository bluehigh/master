<?php
    
# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

if (!sessionSetFor('institute')) {
    session_destroy();
    header("Location: ".HOST."/institute/dashboard/login.php");
}

// Check if institute has already code

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;

date_default_timezone_set("Asia/Kolkata");

$db = new Handler();

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

if ($institute->hasCode()) {
    header('Location: '.HOST.'/institute/dashboard/index.php');
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Student Dashboard! | </title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <style type="text/css">
        #availablity {
            display: none;
            line-height: 4em;
            font-size: 1.2em;
            color: green;
        }
    </style>
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
    </script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>
        <div class="right_col" role="main">
            <div class="">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel" >
                      <div class="x_title">
                        <h2>Set an unique code for your institute</h2>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <br>
                        <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" onsubmit="return setCode()">

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Code : <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="text" id="new_code" required="required" class="form-control col-md-7 col-xs-12"><span id="availablity"><i class="fas fa-check"></i>Available</span>
                            </div>
                          </div>
                          <div class="ln_solid"></div>
                          <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                              <button class="btn btn-success" id="save_button" disabled>Save</button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>

      </div>
    </div>

    <?php require_once('components/scripts.php'); ?>
	<script type="text/javascript">
        $("form").submit(function(e){
            e.preventDefault();
        });
           $('#new_code').keyup(function() {
                if ($('#new_code').val().length > 2) {
                    $.ajax({
                        url: "<?php echo HOST; ?>/app/api/admin/set_institute_code.php",
                        method: "POST",
                        data: {
                            "code": $(this).val(),
                            "action": "check",
                            "id": unique_id
                        },
                        success: function(data) {
                            console.log(data);
                            data = JSON.parse(data);
                            if (data['available']) {
                                console.log('aa');
                                $('#save_button').removeAttr('disabled');
                                $('#availablity').show();
                            } else {
                                $('#save_button').attr('disabled','');
                                $('#availablity').hide();
                            }
                        }
                    });
                } else {
                    $('#save_button').attr('disabled','');
                    $('#availablity').hide();
                }
           });
           function setCode() {
                $.ajax({
                    url: "<?php echo HOST; ?>/app/api/admin/set_institute_code.php",
                    method: "POST",
                    data: {
                        "code": $('#new_code').val(),
                        "action": "set",
                        "id": unique_id
                    },
                    success: function(data) {
                        console.log(data);
                        data = JSON.parse(data);
                        if (data['saved']) {
                            window.location = host+'/institute/dashboard/index.php';
                        }
                    }
                });
           }
    </script>
  </body>
</html>