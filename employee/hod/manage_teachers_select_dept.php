<?php
    
# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

if (!sessionSetFor('institute')) {
    session_destroy();
    header("Location: ".HOST."/institute/dashboard/login.php");
}

require ROOT."/app/helpers/institute_check.php";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Manage Teachers</title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
    </script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>
        <div class="right_col" role="main">
            <div class="">
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel" >
                    <div class="x_title">
                      <h2>Select a depratment </h2>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      <div>
                          <div class="container-fluid">
                              <div class="row">
                                  <div class="col-12">
                                      <table id="deptTable" class="display" style="width:100%">
                                        <thead>
                                          <tr>
                                            <th>Dept Name</th>
                                            <th>Dept Code</th>
                                            <th>No of Courses</th>
                                            <th>Manage</th>
                                          </tr>
                                        </thead>
                                      </table>
                                  </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>            
            </div>
        </div>

      </div>
    </div>

    <?php require_once('components/scripts.php'); ?>
    <script type="text/javascript">
        var editor;
        $(document).ready(function () {

          $('#deptTable').DataTable( {
            dom: "Bfrtip",
            ajax: host+"/app/api/admin/dept_data.php?action=view",
            responsive: true,
            "columns": [
              { "data": "dept_name" },
              { "data": "dept_code" },
              { "data": "course_count" },
              { "data": "manage",
                  "render" : function(data, type, row) {
                      console.log(row);
                      return "<a href='"+host+"/institute/dashboard/view_teachers.php?dept_id="+data+"'><button type='button' class='btn'>View Teachers</button></a>";
                  }
              }
            ],
             buttons: []
          } );
        });
    </script>
  </body>
</html>