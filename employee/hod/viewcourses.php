<?php
# Project init file
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

if (!sessionSetFor('employee')) {
    session_destroy();
    header("Location: ".HOST."/employee/login.php");
}

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Hod;
use Scholarly\Teacher;
use Scholarly\Department;
use Scholarly\Course;
use Scholarly\Batch;

$db = new Handler();
Department::setDb($db);
Course::setDb($db);
Batch::setDb($db);

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$hod = new Hod($_SESSION['empid']);
$hod->setDb($db);
$hod->fetchDetails();

if ($hod->getAccessLevel() != 2) {
  header("Location: ".HOST."/employee/emp_route.php"); 
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Manage Courses </title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
    </script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>
        <div class="right_col" role="main">
          <div class="">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel" >
                  <div class="x_title">
                    <h2>All Courses</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12">
                                    <table id="courseTable" class="display" style="width:100%">
                                      <thead>
                                        <tr>
                                          <th>Department</th>
                                          <th>Course Name</th>
                                          <th>Course Code</th>
                                          <th>Span</th>
                                          <th>Status</th>
                                          <th>Manage</th>
                                        </tr>
                                      </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php require_once('components/scripts.php'); ?>
    <script type="text/javascript">
        var editor;
        $(document).ready(function () {

          $('#courseTable').DataTable( {
            dom: "Bfrtip",
            ajax: host+"/app/api/employee/hod/course_data.php?action=view",
            responsive: true,
            "columns": [
              { "data": "dept.name" },
              { "data": "course.course_name" },
              { "data": "course.course_code" },
              { "data": "course.course_span" },
              { "data": "status.name" },
              { "data": "course.manage",
                "render" : function(data, type, row) {
                    return "<a href='"+host+"/employee/hod/viewsemesters.php?course_id="+data+"'><button type='button' class='btn'>Details</button></a><a href='"+host+"/employee/hod/viewbatchs.php?course_id="+data+"'><button type='button' class='btn'>Students</button></a>";
                }
              }
            ],
             buttons: [
            ]
          } );
          
        });
    </script>
  </body>
</html>