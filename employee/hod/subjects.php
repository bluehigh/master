<?php
    
# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Department;
use Scholarly\Course;
use Scholarly\Semester;

if (!sessionSetFor('institute')) {
    session_destroy();
    header("Location: ".HOST."/institute/dashboard/login.php");
}

# Check if semester id is provided or not
if (!isset($_GET['sem_id'])) {
  // If sem_id is not provided, rediret to course management page
  header("Location: ".HOST."/institute/dashboard/courses.php");
}

# Check if the sem_id is valid for that institute
$db = new Handler();
Department::setDb($db);
Course::setDb($db);
Semester::setDb($db);
$current_sem = false;

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$fetched_dept_ids = $institute->getDeptIds();

if (count($fetched_dept_ids)) {
  foreach ($fetched_dept_ids as $dept_id) {
    $dept = new Department($dept_id);
    $dept->fetchDetails();
    $fetched_course_ids = $dept->getCourseIds();

    if (count($fetched_course_ids)) {
      foreach ($fetched_course_ids as $course_id) {
        $course = new Course($course_id);
        $course->fetchDetails();

        $fetched_sem_ids = $course->getSemIds();
        if (count($fetched_sem_ids)) {
          foreach ($fetched_sem_ids as $sem_id) {
            if ($sem_id == $_GET['sem_id']) {
              $current_sem = $sem_id;
              break 3;
            }
          }
        }
      }
    }
  }
}



if (!$current_sem) {
  header("Location: ".HOST."/institute/dashboard/courses.php");
}

$sem = new Semester($current_sem);
$sem->fetchDetails();

require ROOT."/app/helpers/institute_check.php";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Manage Subjects </title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
    </script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>
        <div class="right_col" role="main">
            <div class="">
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel" >
                    <div class="x_title">
                      <h2>Manage Subjects of course : <?php echo $course->getName()." (Sem - ".$sem->getNo()." )"; ?></h2>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      <div>
                          <div class="container-fluid">
                              <div class="row">
                                  <div class="col-12">
                                      <table id="subjectTable" class="display" style="width:100%">
                                        <thead>
                                          <tr>
                                            <th>Type</th>
                                            <th>Subject Name</th>
                                            <th>Subject Code</th>
                                            <th>Subject Points</th>
                                          </tr>
                                        </thead>
                                      </table>
                                  </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>            
            </div>
        </div>

      </div>
    </div>

    <?php require_once('components/scripts.php'); ?>
    <script type="text/javascript">
        var editor;
        $(document).ready(function () {


          editor = new $.fn.dataTable.Editor({
            ajax: host+"/app/api/admin/subject_data.php?sem_id=<?php echo $current_sem;?>",
            table: "#subjectTable",
            fields: [{
                label: "Subject Name:",
                name: "sub.sub_name"
              }, {
                label: "Subject Type:",
                name: "sub.sub_type",
                type: 'select'
              }, {
                label: "Points:",
                name: "sub.points"
              }
            ]
          });


          $('#subjectTable').DataTable( {
            dom: "Bfrtip",
            ajax: host+"/app/api/admin/subject_data.php?action=view&sem_id=<?php echo $current_sem;?>",
            responsive: true,
            "columns": [
              { "data": "type.name" },
              { "data": "sub.sub_name" },
              { "data": "sub.sub_code" },
              { "data": "sub.points" }
            ],
            select: 'single',
            buttons: [
               { extend: "create", editor: editor },
               { extend: "edit", editor: editor },
               { extend: "remove", editor: editor }
            ]
          } );

          editor.on('onInitEdit', function() {
            editor.disable('sub.sub_type');
          });
          editor.on('onInitCreate', function() {
            editor.enable('sub.sub_type');
          });


        });
    </script>
  </body>
</html>