<?php
  $first_name = explode(' ', $hod->name)['0'];
?>
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="departments.php" class="site_title">&nbsp;&nbsp;&nbsp;<span><?php echo strtoupper($institute->get('code')['code']); ?>&nbsp;&nbsp;&nbsp;<span style="font-size: 40%;font-style: italic;">Powered by SCHOLARLY</span></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="images/avatar-male.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $first_name; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-book"></i>Courses <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="owned_courses.php">Manage Owned Courses</a></li>
                      <li><a href="viewcourses.php">View All Courses</a></li>
                    </ul>
                  </li>
                  <!-- <li><a href="departments.php"><i class="fa fa-book"></i> Manage Departments <span class="fa fa-chevron-down"></span></a> -->
                    <!-- <ul class="nav child_menu">
                      <li><a href="courses.php">Manage Courses</a></li>
                    </ul>
                  </li> -->
                  <li><a href="viewroutine.php"><i class="fa fa-calendar-minus-o"></i>My Classes</a>
                  </li>
                
                </ul>
              </div>
              <div class="menu_section">

              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/avatar-male.png" alt=""><?php echo $first_name; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                   <!--  <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li> -->
                    <li><a href="<?php echo HOST; ?>/employee/login.php?logout=true"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->