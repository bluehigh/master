<?php

# Project init file
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

if (!sessionSetFor('employee')) {
    session_destroy();
    header("Location: ".HOST."/employee/login.php");
}

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Hod;
use Scholarly\Teacher;
use Scholarly\Department;
use Scholarly\Course;
use Scholarly\Batch;
use Scholarly\Routine;
use Scholarly\Semester;
use Scholarly\Student;

$db = new Handler();
Department::setDb($db);
Semester::setDb($db);
Routine::setDb($db);
Course::setDb($db);
Batch::setDb($db);


date_default_timezone_set("Asia/Kolkata");

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$hod = new Hod($_SESSION['empid']);
$hod->setDb($db);
$hod->fetchDetails();

if ($hod->getAccessLevel() != 2) {
  header("Location: ".HOST."/employee/emp_route.php"); 
}

if (!isset($_GET['period_id'])) {
   header("Location: ".HOST."/employee/hod/viewroutine.php"); 
}

# Get sem_id from period_id
$routine = new Routine();
$period_data = $routine->fetchPeriod($_GET['period_id']);
$current_sem_id = $period_data['sem_id'];

# Get course_id from sem_id
$sem = new Semester($current_sem_id);
$sem->fetchDetails();
$current_course_id = $sem->getCourseId();

# Get batch_id
$course = new Course($current_course_id);
$course->fetchDetails();
$current_batch_id = $course->getBatchIdFor($sem);

$batch = new Batch($current_batch_id);
$batch->fetchDetails();
$all_student_id = $batch->getAllStudentIds();

$student_name_array = [];

if (count($all_student_id)) {
  foreach ($all_student_id as $student_id) {
    $student = new Student($student_id);
    $student->setDb($db);
    $student->fetchDetails();
    $student_name_array[$student_id] = $student->getName();
    $js_status_array[$student_id] = 0;
  }
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Attendance</title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
        var students = <?php print_r(json_encode($js_status_array, true)); ?>;
    </script>
    <style type="text/css">
      .student_parent_row {
        margin: auto;
        box-shadow: 0px 0px 5px rgba(0,0,0,0.12);
        margin-bottom: 8px;
        width: 100%;
      }
      .student_parent_row .col-xs-8 {
        padding-top: 12px;
      }
      .button_parent {
        background-color: red;
        height: inherit;
      }
      .attn_btn {
        width: 100%;
        background-color: red;
        color: white;
      }
      .toggle_modal {
        margin-top: 3em;
        width: 40%;
      }
      .reset_all, .reset_all:hover, .reset_all:focus {
        margin-top: 3em;
        width: 40%;
        background-color: red;
        border: 1px solid red;
      }
      @media only screen and (min-width: 900px) {
         .student_parent_row {
            width: 70%;
        }
        .toggle_modal {
          margin-top: 3em;
          width: 10%;
        }
        .reset_all, .reset_all:hover, .reset_all:focus {
          margin-top: 3em;
          width: 10%;
          background-color: red;
          border: 1px solid red;
        }
      }

      
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>

        <div class="right_col" role="main" id="main_content">
	      <div class="">
	      		<div class="row">
	      		  <div class="col-md-12">
	      		    <div class="x_panel" >
	      		      <div class="x_title">
	      		        <h2>Attendance </h2>
	      		        <div class="clearfix"></div>
	      		      </div>
	      		      <div class="x_content">
	      		        <div>
	      		            <div class="container-fluid">
	      		                <div class="row">
	      		                    <div class="col-12">
                                    <div class="row">

                                      <?php
                                        foreach ($student_name_array as $key => $value) {
                                          ?>
                                              <div class="col-sm-12 student_parent_row">
                                                <div class="row">
                                                  <div class="col-xs-8">
                                                    <div class="col-md-5 col-sm-12"><?php echo $key; ?></div>
                                                    <div class="col-md-7 col-sm-12"><?php echo $value; ?></div>
                                                  </div>
                                                  <div class="col-xs-4 button_parent">
                                                    <button type="button" class="btn attn_btn" id="sid_<?php echo $key; ?>" status="0">Absent</button>
                                                  </div>
                                                </div>
                                              </div>
                                          <?php
                                        }
                                      ?>

                                    </div>
                                    <div class="row" style="text-align: left">
                                      <button type="button" class="btn btn-primary toggle_modal" data-toggle="modal" data-target=".bs-example-modal-lg">Save</button>
                                      <button type="button" class="btn btn-primary reset_all">Reset</button>
                                    </div>
                                   
	      		                    </div>
	      		                </div>
	      		            </div>
	      		        </div>
	      		      </div>
	      		    </div>
	      		  </div>
	      		</div>
	      </div>
      	</div>


      </div>
    </div>

    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Confirm Attendance Submission</h4>
          </div>
          <div class="modal-body">
           <div class="container">
             <div class="row">
               <div class="col-md-8">
                 <label for="fullname">Enter your password * :</label>
                 <input type="password" id="emp_pass" class="form-control" name="fullname" required="">
                 <div class="col-md-9 col-sm-9 col-xs-12">
                   <select class="form-control" id="weightage">
                     <option value='0'>Select Weightage</option>
                     <option value='1'>1</option>
                     <option value='2'>2</option>
                     <option value='3'>3</option>
                     <option value='4'>4</option>
                   </select>
                 </div>
               </div>
             </div>
           </div>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="submit_with_pass">Submit</button>
          </div>

        </div>
      </div>
    </div>

    <?php require_once('components/scripts.php'); ?>
    <script type="text/javascript">


        $(document).ready(function () {
            <?php
            if ($hod->alreadyRecordedFor($_GET['period_id'])) {
            ?>
              $('#main_content').hide();
                new PNotify({
                  title: 'Records exist!',
                  text: 'Attendance for this period is already recorded'+'<br>You will be redirect to routine page',
                  type: 'error',
                  styling: 'bootstrap3'
              });
                setTimeout(function() {
                  window.location.replace(host+"/employee/hod/viewroutine.php");
                }, 5000);
            <?php
            }
            ?>

            <?php
            if ($period_data['day_of_week'] != Date('N')) {
            ?>
              $('#main_content').hide();
                new PNotify({
                  title: 'Access Denied!',
                  text: 'Attendance for this period is not valid'+'<br>You will be redirect to routine page',
                  type: 'error',
                  styling: 'bootstrap3'
              });
                setTimeout(function() {
                  window.location.replace(host+"/employee/hod/viewroutine.php");
                }, 5000);
            <?php
            }
            ?>

            var all_status = students;

            console.log(all_status);
            $('.attn_btn').click(function() {
              console.log($(this).attr('status'));
                if ($(this).attr('status')==1) {
                  // if 1
                  $(this).attr('status', 0);
                  all_status[$(this).attr('id').substring(4)] = 0;
                  $(this).css('background-color','red');
                  $(this).css('color','white');
                  $(this).parent('.button_parent').css('background-color', 'red');
                  $(this).html('Absent');  
                } else {
                  // if 0
                  $(this).attr('status', 1);
                  all_status[$(this).attr('id').substring(4)] = 1;
                  $(this).css('background-color','#00ff00');
                  $(this).css('color','white');
                  $(this).parent('.button_parent').css('background-color', '#00ff00');
                  $(this).html('Present');  
                }
                console.log($(this).attr('id'));
                
                console.log(all_status);
            });


            $('.toggle_modal').click(function() {

            });
            $('.reset_all').click(function() {
                $('.attn_btn')
                $('.attn_btn').attr('status', 0);
                $('.attn_btn').css('background-color','red');
                $('.attn_btn').css('color','white');
                $('.attn_btn').parent('.button_parent').css('background-color', 'red');
                $('.attn_btn').html('Absent');
                $.each(all_status, function(index, value) {
                  all_status[index] = 0;
                })
                console.log(all_status);
            });

            $('#submit_with_pass').click(function() {
                if ($('#emp_pass').val() == '') {
                  new PNotify({
                      title: 'Input your Password!',
                      text: '',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
                } else {
                  if ($('#weightage').val() == 0) {
                    new PNotify({
                      title: 'Select a weightage!',
                      text: '',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
                  } else {
                    console.log($('#weightage').val());
                       $.ajax({
                          url: host+"/app/api/employee/teacher/attendance_data.php?batch_id=<?php echo $current_batch_id;?>",
                          method: 'POST',
                          data: {
                              'record': all_status,
                              'pass': $('#emp_pass').val(),
                              'weightage': $('#weightage').val(),
                              'period_id': <?php echo $_GET['period_id']; ?>
                              },
                          success: function(data) {
                             data = JSON.parse(data);
                             if (data['success']) {
                                  new PNotify({
                                    title: 'Recorded Successfully!',
                                    text: 'You will be redirected to routine page',
                                    type: 'success',
                                    styling: 'bootstrap3'
                                });
                                setTimeout(function() {
                                  window.location.replace(host+"/employee/hod/viewroutine.php");
                                }, 2000);
                             } else {
                                if (data['error_type'] == "1") {
                                      new PNotify({
                                        title: 'Wrong Password!',
                                        text: data['error'],
                                        type: 'error',
                                        styling: 'bootstrap3'
                                    });
                                } else {
                                      new PNotify({
                                        title: 'Records exist!',
                                        text: data['error']+'<br>You will be redirect to routine page',
                                        type: 'error',
                                        styling: 'bootstrap3'
                                    });
                                      setTimeout(function() {
                                        window.location.replace(host+"/employee/hod/viewroutine.php");
                                      }, 8000);
                                }
                             }
                          }
                      });
                  }
                }
            });
        });


    </script>
  </body>
</html>