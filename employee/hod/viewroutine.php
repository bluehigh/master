<?php
# Project init file
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

if (!sessionSetFor('employee')) {
    session_destroy();
    header("Location: ".HOST."/employee/login.php");
}

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Hod;
use Scholarly\Department;
use Scholarly\Course;
use Scholarly\Batch;

$db = new Handler();
Department::setDb($db);
Course::setDb($db);
Batch::setDb($db);

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$hod = new Hod($_SESSION['empid']);
$hod->setDb($db);
$hod->fetchDetails();

if ($hod->getAccessLevel() != 2) {
  header("Location: ".HOST."/employee/emp_route.php"); 
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Routine </title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <style type="text/css">
      .clearall {
        background: red!important;
        color: white!important;
        border: 1px solid red!important; 
      }
    </style>
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
    </script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>
        <div class="right_col" role="main">
            <div class="">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel" >
                      <div class="x_title">
                        <h2>My Classes</h2>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <div>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12">
                                    <div class="">
                                          <div class="x_panel">
                                            
                                            <div class="x_content">

                                              <div class="col-md-2 col-sm-12">
                                                <!-- required for floating -->
                                                <!-- Nav tabs -->
                                                <ul class="nav nav-tabs tabs-left">
                                                  <li class="active"><a href="#monday" data-toggle="tab">Monday</a>
                                                  </li>
                                                  <li><a href="#tuesday" data-toggle="tab">Tuesday</a>
                                                  </li>
                                                  <li><a href="#wednesday" data-toggle="tab">Wednesday</a>
                                                  </li>
                                                  <li><a href="#thursday" data-toggle="tab">Thursday</a>
                                                  </li>
                                                  <li><a href="#friday" data-toggle="tab">Friday</a>
                                                  </li>
                                                  <li><a href="#saturday" data-toggle="tab">Saturday</a>
                                                  </li>
                                                </ul>
                                              </div>

                                              <div class="col-md-10">
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                  <div class="tab-pane active" id="monday">
                                                    <p class="lead">Monday</p>
                                                    <div>
                                                      <table id="mondayTable" class="display" style="width:100%">
                                                        <thead>
                                                          <tr>
                                                            <th>Starts</th>
                                                            <th>Ends</th>
                                                            <th>Subject Code</th>
                                                            <th>Subject Name</th>
                                                            <th>Attendance</th>
                                                          </tr>
                                                        </thead>
                                                      </table>
                                                    </div>
                                                  </div>
                                                  <div class="tab-pane" id="tuesday">
                                                    <p class="lead">Tuesday</p>
                                                    <div>
                                                      <table id="tuesdayTable" class="display" style="width:100%">
                                                        <thead>
                                                          <tr>
                                                            <th>Starts</th>
                                                            <th>Ends</th>
                                                            <th>Subject Code</th>
                                                            <th>Subject Name</th>
                                                            <th>Attendance</th>
                                                          </tr>
                                                        </thead>
                                                      </table>
                                                    </div>
                                                  </div>
                                                  <div class="tab-pane" id="wednesday">
                                                    <p class="lead">Wednesday</p>
                                                    <div>
                                                      <table id="wednesdayTable" class="display" style="width:100%">
                                                        <thead>
                                                          <tr>
                                                            <th>Starts</th>
                                                            <th>Ends</th>
                                                            <th>Subject Code</th>
                                                            <th>Subject Name</th>
                                                            <th>Attendance</th>
                                                          </tr>
                                                        </thead>
                                                      </table>
                                                    </div>
                                                  </div>
                                                  <div class="tab-pane" id="thursday">
                                                    <p class="lead">Thursday</p>
                                                    <div>
                                                      <table id="thursdayTable" class="display" style="width:100%">
                                                        <thead>
                                                          <tr>
                                                            <th>Starts</th>
                                                            <th>Ends</th>
                                                            <th>Subject Code</th>
                                                            <th>Subject Name</th>
                                                            <th>Attendance</th>
                                                          </tr>
                                                        </thead>
                                                      </table>
                                                    </div>
                                                  </div>
                                                  <div class="tab-pane" id="friday">
                                                    <p class="lead">Friday</p>
                                                    <div>
                                                      <table id="fridayTable" class="display" style="width:100%">
                                                        <thead>
                                                          <tr>
                                                            <th>Starts</th>
                                                            <th>Ends</th>
                                                            <th>Subject Code</th>
                                                            <th>Subject Name</th>
                                                            <th>Attendance</th>
                                                          </tr>
                                                        </thead>
                                                      </table>
                                                    </div>
                                                  </div>
                                                  <div class="tab-pane" id="saturday">
                                                    <p class="lead">Saturday</p>
                                                    <div>
                                                      <table id="saturdayTable" class="display" style="width:100%">
                                                        <thead>
                                                          <tr>
                                                            <th>Starts</th>
                                                            <th>Ends</th>
                                                            <th>Subject Code</th>
                                                            <th>Subject Name</th>
                                                            <th>Attendance</th>
                                                          </tr>
                                                        </thead>
                                                      </table>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>

                                              <div class="clearfix"></div>

                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>            
            </div>
        </div>

      </div>
    </div>

    <?php require_once('components/scripts.php'); ?>
    <script type="text/javascript">

        $(document).ready(function () {



          /*        Editor-1 Starts      */

          $('#mondayTable').DataTable( {
            dom: "Bfrtip",
            ajax: host+"/app/api/employee/teacher/view_routine_data.php?action=view&day=1",
            responsive: true,
            "columns": [
              { "data": "class.starts", 
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.ends",
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.sub_code" },
              { "data": "class.sub_name" },
              { "data": "class.period_id",
                "render" : function(data, type, row) {
                    return "<a href='attendance.php?period_id="+data+"'><button type='button' class='btn'>Record Attendance</button></a>"
                }
              }
            ],
            buttons: [
            ]
          } );


          /*        Editor-1 Ends      */


          /*        Editor-2 Starts      */
          $('#tuesdayTable').DataTable( {
            dom: "Bfrtip",
            ajax: host+"/app/api/employee/teacher/view_routine_data.php?action=view&day=2",
            responsive: true,
            "columns": [
              { "data": "class.starts", 
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.ends",
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.sub_code" },
              { "data": "class.sub_name" },
              { "data": "class.period_id",
                "render" : function(data, type, row) {
                    return "<a href='attendance.php?period_id="+data+"'><button type='button' class='btn'>Record Attendance</button></a>"
                }
              }
            ],
            buttons: [
              
            ]
          } );

          /*        Editor-2 Ends      */


          /*        Editor-3 Starts      */

          $('#wednesdayTable').DataTable( {
            dom: "Bfrtip",
            ajax: host+"/app/api/employee/teacher/view_routine_data.php?action=view&day=3",
            responsive: true,
            "columns": [
              { "data": "class.starts", 
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.ends",
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.sub_code" },
              { "data": "class.sub_name" },
              { "data": "class.period_id",
                "render" : function(data, type, row) {
                    return "<a href='attendance.php?period_id="+data+"'><button type='button' class='btn'>Record Attendance</button></a>"
                }
              }
            ],
            buttons: [
            ]
          } );


          /*        Editor-3 Ends      */


          /*        Editor-4 Starts      */


          $('#thursdayTable').DataTable( {
            dom: "Bfrtip",
            ajax: host+"/app/api/employee/teacher/view_routine_data.php?action=view&day=4",
            responsive: true,
            "columns": [
              { "data": "class.starts", 
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.ends",
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.sub_code" },
              { "data": "class.sub_name" },
              { "data": "class.period_id",
                "render" : function(data, type, row) {
                    return "<a href='attendance.php?period_id="+data+"'><button type='button' class='btn'>Record Attendance</button></a>"
                }
              }
            ],
            buttons: [
            ]
          } );
          /*        Editor-4 Ends      */


          /*        Editor-5 Starts      */

          $('#fridayTable').DataTable( {
            dom: "Bfrtip",
            ajax: host+"/app/api/employee/teacher/view_routine_data.php?action=view&day=5",
            responsive: true,
            "columns": [
              { "data": "class.starts", 
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.ends",
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.sub_code" },
              { "data": "class.sub_name" },
              { "data": "class.period_id",
                "render" : function(data, type, row) {
                    return "<a href='attendance.php?period_id="+data+"'><button type='button' class='btn'>Record Attendance</button></a>"
                }
              }
            ],
            buttons: [
            ]
          } );

          
          /*        Editor-5 Ends      */

          /*        Editor-6 Starts      */



          $('#saturdayTable').DataTable( {
            dom: "Bfrtip",
            ajax: host+"/app/api/employee/teacher/view_routine_data.php?action=view&day=6",
            responsive: true,
            "columns": [
              { "data": "class.starts", 
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.ends",
                "render": function(data, type, row) {
                  return convertToTime(data);
                }
              },
              { "data": "class.sub_code" },
              { "data": "class.sub_name" },
              { "data": "class.period_id",
                "render" : function(data, type, row) {
                    return "<a href='attendance.php?period_id="+data+"'><button type='button' class='btn'>Record Attendance</button></a>"
                }
              }
            ],
            buttons: [
              
            ]
          } );
          /*        Editor-6 Ends      */

        });
    </script>
  </body>
</html>