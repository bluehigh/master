<?php

# Project init file
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Employee;

session_start();
$db = new Handler();

if(!sessionSetFor('employee')){
	session_destroy();
 	header("Location: ".HOST."/employee/login.php");
 	exit();
} else {
	$employee = new Employee($_SESSION['empid']);
	$employee->setDb($db);
	$employee->fetchDetails();

	switch ($employee->getAccessLevel()) {
		case '1':
			# Principle
			header("Location: ".HOST."/employee/principle/index.php");
			exit();
			break;

		case '2':
			# HOD
			header("Location: ".HOST."/employee/hod/viewroutine.php");
			exit();
			break;

		case '3':
			# Assistant Professor
			header("Location: ".HOST."/employee/teacher/viewroutine.php");
			exit();

		case '4':
			# Senior Technical Assistant
			header("Location: ".HOST."/employee/teacher/viewroutine.php");
			exit();
			
		case '5':
			# Junior Technical Assistant
			header("Location: ".HOST."/employee/teacher/viewroutine.php");
			exit();
			break;
			
		case '6':
			# Clerk
			header("Location: ".HOST."/employee/clerk/index.php");
			exit();
			break;

		case '7':
			# Clerk
			header("Location: ".HOST."/employee/library/index.php");
			exit();
			break;
	}
}


