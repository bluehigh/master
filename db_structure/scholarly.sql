-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 19, 2018 at 06:02 AM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scholarly`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_table`
--

CREATE TABLE `admin_table` (
  `pid` int(11) NOT NULL,
  `admin_id` text,
  `password` text,
  `otp` text,
  `name` text,
  `type` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attendance_record`
--

CREATE TABLE `attendance_record` (
  `pid` int(11) NOT NULL,
  `date` text,
  `period_id` text,
  `student_id` text,
  `weightage` text,
  `day` text,
  `empid` text,
  `status` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendance_record`
--

INSERT INTO `attendance_record` (`pid`, `date`, `period_id`, `student_id`, `weightage`, `day`, `empid`, `status`) VALUES
(13, '14052018', '7', '10181', '2', '1', '1011801', '1'),
(14, '14052018', '7', '10182', '2', '1', '1011801', '1'),
(15, '14052018', '7', '10183', '2', '1', '1011801', '1');

-- --------------------------------------------------------

--
-- Table structure for table `batch_map`
--

CREATE TABLE `batch_map` (
  `batch_id` int(11) NOT NULL,
  `course_id` text,
  `starting_year` int(11) DEFAULT NULL,
  `ending_year` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batch_map`
--

INSERT INTO `batch_map` (`batch_id`, `course_id`, `starting_year`, `ending_year`) VALUES
(1, '11', 2018, 2022),
(2, '11', 2014, 2018),
(3, '11', 2016, 2020),
(4, '11', 2017, 2021),
(5, '12', 2012, 2014),
(6, '16', 2019, 2023),
(7, '17', 2019, 2022),
(8, '16', 2015, 2019),
(9, '16', 2016, 2020),
(10, '16', 2017, 2021);

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `bid` int(11) NOT NULL,
  `bookid` text,
  `unique_id` text,
  `name` text,
  `author` text,
  `category` text,
  `tags` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`bid`, `bookid`, `unique_id`, `name`, `author`, `category`, `tags`) VALUES
(1, '1526410305', '101', 'Book1', 'bvc', 'fxcg', 'xv'),
(2, '1526410604', '101', 'Book1', 'bvc', 'fxcg', 'xv'),
(4, '1526410611', '101', 'Book2', 'sf', 'dgf', 'gf');

-- --------------------------------------------------------

--
-- Table structure for table `class_map`
--

CREATE TABLE `class_map` (
  `pid` int(11) NOT NULL,
  `emp_id` text,
  `subject_id` text,
  `class_id` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `course_map`
--

CREATE TABLE `course_map` (
  `course_id` int(11) NOT NULL,
  `course_code` text,
  `course_name` text,
  `course_span` text,
  `dept_id` text,
  `status` int(255) NOT NULL DEFAULT '1',
  `hod` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_map`
--

INSERT INTO `course_map` (`course_id`, `course_code`, `course_name`, `course_span`, `dept_id`, `status`, `hod`) VALUES
(11, 'cse', 'Computer Science and Engineering', '8', '8', 1, NULL),
(12, 'dip', 'Diploma', '3', '8', 1, NULL),
(13, 'cse', 'Computer Science and Engineering', '8', '9', 0, NULL),
(14, NULL, NULL, NULL, NULL, 1, NULL),
(15, 'cse', 'Computer Science and Engineering', '8', '10', 1, NULL),
(16, 'cse', 'Computer Science and Engineering', '8', '12', 1, '1011801'),
(17, 'BCS', 'Bachelor in Computer Science', '6', '13', 1, '1011804'),
(18, 'IT', 'Information Tech', '8', '12', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dept_map`
--

CREATE TABLE `dept_map` (
  `dept_id` int(11) NOT NULL,
  `unique_id` text,
  `dept_code` text,
  `dept_name` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dept_map`
--

INSERT INTO `dept_map` (`dept_id`, `unique_id`, `dept_code`, `dept_name`) VALUES
(8, '10000018', 'engp', 'Engineering'),
(9, '10010018', 'engggggg', 'Engineering'),
(10, '10020018', 'engggggg', 'Engineering'),
(11, '14', 'ENGG', 'Engineering'),
(12, '101', 'ENGG', 'Engineering'),
(13, '101', 'SC', 'Science');

-- --------------------------------------------------------

--
-- Table structure for table `emp_data`
--

CREATE TABLE `emp_data` (
  `pid` int(11) NOT NULL,
  `empid` text,
  `unique_id` text,
  `name` text,
  `email` text,
  `emp_type` text,
  `dept_id` text,
  `desig_id` text,
  `phone` text,
  `address` text,
  `password` text,
  `status` varchar(255) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_data`
--

INSERT INTO `emp_data` (`pid`, `empid`, `unique_id`, `name`, `email`, `emp_type`, `dept_id`, `desig_id`, `phone`, `address`, `password`, `status`) VALUES
(10, '1011801', '101', 'Bulbul Mukherjee', 'bmukherjee@gmail.com', 'T', '12', '3', '09083417379', 'Kolkata', '1234567890', '1'),
(11, '1011802', '101', 'Souvik Sengupta', 'souvik.sengupta@bitcollege.in', 'T', '12', '3', '09083417379', 'Kolkataa', 'f0d6b84a83da3c83e5af14aa24afd7f9', '1'),
(12, '1011803', '101', 'Debasis Mondal', 'dmondal@gmail.com', 'N', '12', '6', '980414632', 'Kolkata', 'c4f688eaf3b86437eac61740338d5a8a', '1'),
(13, '1011804', '101', 'Debalina Barik', 'dbarik@gmail.com', 'T', '13', '3', '951357159', 'Burduan', '3f5ca8318852c97b537b20a03911f01f', '1'),
(14, '1011805', '101', 'Tushar Debnath', 'tdebnath@gmail.com', 'T', '12', '3', '9235478114', 'Kolkata', '1234567890', '1'),
(15, '1011806', '101', 'Pratik', 'bulldog@gmail.com', 'N', '12', '7', '55555555555', 'Kolkata', '1234567890', '1'),
(16, '1011807', '101', 'Bisu Mondal', 'bmondal@gmail.com', 'N', '12', '6', '943416542', 'Kolkata', '1234567890', '1'),
(17, '1011808', '101', 'Shibaji Panda', 'spanda@gmail.com', 'T', '12', '1', '123', 'Kolkata', '1234567890', '1');

-- --------------------------------------------------------

--
-- Table structure for table `emp_legend`
--

CREATE TABLE `emp_legend` (
  `pid` int(11) NOT NULL,
  `emp_type` text,
  `desig` text,
  `desig_id` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_legend`
--

INSERT INTO `emp_legend` (`pid`, `emp_type`, `desig`, `desig_id`) VALUES
(1, 'T', 'Principle, Assistant Professor', '1'),
(2, 'T', 'HOD, Assistant Professor', '2'),
(3, 'T', 'Assistant Professor', '3'),
(4, 'T', 'Senior technical assistant\r\n', '4'),
(5, 'T', 'Junior technical assistant', '5'),
(6, 'N', 'Clerk', '6'),
(7, 'N', 'Librarian', '7');

-- --------------------------------------------------------

--
-- Table structure for table `emp_panels`
--

CREATE TABLE `emp_panels` (
  `emp_type` text,
  `panel` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fees_legend`
--

CREATE TABLE `fees_legend` (
  `pid` int(11) NOT NULL,
  `fee_struct_id` text,
  `course_id` text,
  `year` text,
  `amount_to_pay` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fees_record`
--

CREATE TABLE `fees_record` (
  `pid` int(11) NOT NULL,
  `student_id` text,
  `fee_struct_id` text,
  `date_of_payment` text,
  `amount_paid` text,
  `fine_amount` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `institution_general`
--

CREATE TABLE `institution_general` (
  `pid` int(11) NOT NULL,
  `unique_id` text,
  `code` text NOT NULL,
  `institute_name` text,
  `university_name` text,
  `registration_id` text,
  `email` text,
  `type` text,
  `plan` text,
  `otp` text,
  `password` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `institution_general`
--

INSERT INTO `institution_general` (`pid`, `unique_id`, `code`, `institute_name`, `university_name`, `registration_id`, `email`, `type`, `plan`, `otp`, `password`) VALUES
(23, '101', 'BIT', 'Bengal Institute of Technology', 'WBUT', '123456', 'dsahapersonal@gmail.com', 'Engineering', '1', NULL, '1234567890'),
(24, '102', 'HIT', 'Heritage Institute of Technology', 'WBUT', '1231', 'dsahapersoaaaanal@gmail.com', 'Engineering', '1', NULL, '1234567890'),
(25, '103', '', 'MSIT', 'WBUT', '12222', 'dsahapersonalsds@gmail.com', 'Engineering', '1', NULL, '1234567890'),
(26, '104', '', 'BIT', 'WBUT', '12345622', 'dsahapersosasnal@gmail.com', 'Engineering', '1', 'xtzR6gP', NULL),
(27, '105', '', 'Heritage Institute of Technology', 'WBUT', '123456111', 'dsahapersosasanal@gmail.com', 'Engineering', '1', 'cEVynD1', NULL),
(28, '106', 'SIT', 'SIT', 'WBUT', '54554', 'sa@aa.aa', 'Engineering', '1', NULL, '1234567890');

-- --------------------------------------------------------

--
-- Table structure for table `library_details`
--

CREATE TABLE `library_details` (
  `lib_id` int(11) NOT NULL,
  `unique_id` text,
  `fine_per_day` text,
  `return_period` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `library_details`
--

INSERT INTO `library_details` (`lib_id`, `unique_id`, `fine_per_day`, `return_period`) VALUES
(1, '101', '2', '180'),
(2, '105', '2', '180'),
(3, '106', '2', '180');

-- --------------------------------------------------------

--
-- Table structure for table `manage_books`
--

CREATE TABLE `manage_books` (
  `transactionid` int(11) NOT NULL,
  `student_id` text,
  `bookid` text,
  `unique_id` text,
  `issue_date` text,
  `return_date` text,
  `expected_return_date` text,
  `fine` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_books`
--

INSERT INTO `manage_books` (`transactionid`, `student_id`, `bookid`, `unique_id`, `issue_date`, `return_date`, `expected_return_date`, `fine`) VALUES
(4, '10184', '1526410305', '101', '1526462507', '1526668914', '1542100907', '0'),
(5, '10184', '1526410305', '101', '1526669014', '1526669054', '1542307414', '0'),
(6, '10184', '1526410305', '101', '1526669129', '1526669158', '1542307529', '0'),
(7, '10184', '1526410305', '101', '1526669201', '1526669231', '1542307601', '0'),
(8, '10184', '1526410604', '101', '1526669316', '1526669379', '1542307716', '0'),
(9, '10184', '1526410611', '101', '1526669341', '1526669367', '1542307741', '0'),
(10, '10184', '1526410305', '101', '1526702126', '1526702151', '1542340526', '0');

-- --------------------------------------------------------

--
-- Table structure for table `marks_record_sgpa`
--

CREATE TABLE `marks_record_sgpa` (
  `pid` int(11) NOT NULL,
  `student_id` text,
  `sem` text,
  `sgpa` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `marks_record_subjectwise`
--

CREATE TABLE `marks_record_subjectwise` (
  `pid` int(11) NOT NULL,
  `student_id` text,
  `subject_id` text,
  `sem` text,
  `marks_obtained` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `routine_map`
--

CREATE TABLE `routine_map` (
  `period_id` int(11) NOT NULL,
  `sem_id` text,
  `day_of_week` text,
  `sub_id` text,
  `empid` text,
  `s_time` text,
  `e_time` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `routine_map`
--

INSERT INTO `routine_map` (`period_id`, `sem_id`, `day_of_week`, `sub_id`, `empid`, `s_time`, `e_time`) VALUES
(1, '41', '6', '17', '1011802', '36000', '39300'),
(5, '41', '2', '17', '1011801', '36000', '39300'),
(7, '41', '1', '18', '1011801', '36000', '39300'),
(8, '41', '1', '17', '1011801', '43200', '50400');

-- --------------------------------------------------------

--
-- Table structure for table `salary_map`
--

CREATE TABLE `salary_map` (
  `pid` int(11) NOT NULL,
  `unique_id` text,
  `desig_id` text,
  `salary` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scanner_status`
--

CREATE TABLE `scanner_status` (
  `pid` int(11) NOT NULL,
  `librarian_id` text,
  `timestamp` text,
  `result` text,
  `data_time` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scanner_status`
--

INSERT INTO `scanner_status` (`pid`, `librarian_id`, `timestamp`, `result`, `data_time`) VALUES
(1, '1011806', '1526702270', '10184', '1526702158');

-- --------------------------------------------------------

--
-- Table structure for table `sem_map`
--

CREATE TABLE `sem_map` (
  `sem_id` int(11) NOT NULL,
  `course_id` text,
  `sem_no` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sem_map`
--

INSERT INTO `sem_map` (`sem_id`, `course_id`, `sem_no`) VALUES
(9, '11', '1'),
(10, '11', '2'),
(11, '11', '3'),
(12, '11', '4'),
(13, '11', '5'),
(14, '11', '6'),
(15, '11', '7'),
(16, '11', '8'),
(17, '12', '1'),
(18, '12', '2'),
(19, '12', '3'),
(20, '13', '1'),
(21, '13', '2'),
(22, '13', '3'),
(23, '13', '4'),
(24, '13', '5'),
(25, '13', '6'),
(26, '13', '7'),
(27, '13', '8'),
(28, '15', '1'),
(29, '15', '2'),
(30, '15', '3'),
(31, '15', '4'),
(32, '15', '5'),
(33, '15', '6'),
(34, '15', '7'),
(35, '15', '8'),
(36, '16', '1'),
(37, '16', '2'),
(38, '16', '3'),
(39, '16', '4'),
(40, '16', '5'),
(41, '16', '6'),
(42, '16', '7'),
(43, '16', '8'),
(44, '17', '1'),
(45, '17', '2'),
(46, '17', '3'),
(47, '17', '4'),
(48, '17', '5'),
(49, '17', '6'),
(50, '18', '1'),
(51, '18', '2'),
(52, '18', '3'),
(53, '18', '4'),
(54, '18', '5'),
(55, '18', '6'),
(56, '18', '7'),
(57, '18', '8');

-- --------------------------------------------------------

--
-- Table structure for table `stream_set`
--

CREATE TABLE `stream_set` (
  `id` int(11) NOT NULL,
  `stream` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `pid` int(11) NOT NULL,
  `student_id` text,
  `course_id` text,
  `batch_id` text,
  `name` text,
  `status` text,
  `u_roll` text,
  `u_reg` text,
  `starting_year` text,
  `email` text,
  `password` text,
  `address` text,
  `otp` text,
  `phone` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`pid`, `student_id`, `course_id`, `batch_id`, `name`, `status`, `u_roll`, `u_reg`, `starting_year`, `email`, `password`, `address`, `otp`, `phone`) VALUES
(4, '10171', '17', '7', 'Debojyoti Saha', '0', '12100114018', '12100114018', '2019', 'dsahapersonal@gmail.com', NULL, 'Gitanjali Apartment, Natunpally,  Purba Putiary', NULL, '+919933050905'),
(5, '10181', '16', '8', 'D Saha', '1', '121', '121', '2015', 'd@d.d', NULL, 'asd', NULL, '123'),
(6, '10182', '16', '8', 'S Ghosh', '1', '141', '111', '2015', 's@s.s', '1234567890', 'ssss', NULL, '123'),
(7, '10183', '16', '8', 'M Gupta', '1', '145', '145', '2015', 'aa@aa.aa', '1234567890', 'asasas', NULL, '45545'),
(8, '10184', '16', '8', 'S Chakrabarty', '1', '5478', '525', '2015', 's@s.s', '1234567890', 'sssssccccc', NULL, '1234'),
(9, '10185', '16', '8', 'P Ray', '1', '12100114011', '12100114011', '2015', 'ds', '6354182', 'sds', NULL, '45');

-- --------------------------------------------------------

--
-- Table structure for table `subject_map`
--

CREATE TABLE `subject_map` (
  `sub_id` int(11) NOT NULL,
  `sem_id` text,
  `sub_code` text,
  `sub_name` text,
  `sub_type` text,
  `status` text,
  `points` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject_map`
--

INSERT INTO `subject_map` (`sub_id`, `sem_id`, `sub_code`, `sub_name`, `sub_type`, `status`, `points`) VALUES
(9, '29', 'CSE2T1', 'Datastructure', 'T', '1', 10),
(10, '29', 'CSE2T2', 'Mechanics', 'T', '1', 10),
(11, '29', 'CSE2P3', 'Architecure', 'P', '1', 10),
(12, '28', 'CSE1T1', 'Chemistryy', 'T', '1', 100),
(13, '35', 'CSE8T1', 'Ecommerce', 'T', '1', 10),
(14, '9', 'CSE1P1', 'Automata', 'P', '1', 1),
(15, '9', 'CSE1T2', 'Datastructure', 'T', '1', 10),
(16, '44', 'BCS1T1', 'Datastructure', 'T', '1', 10),
(17, '41', 'CSE6P1', 'Architecure', 'P', '1', 3),
(18, '41', 'CSE6T2', 'Automata', 'T', '1', 3),
(19, '41', 'CSE6P3', 'Data Structure', 'P', '1', 3),
(20, '41', 'CSE6T4', 'Data Structure', 'T', '1', 3),
(21, '39', 'CSE4T1', 'Oper Sys', 'T', '1', 3);

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `pid` int(11) NOT NULL,
  `emp_id` text,
  `name` text,
  `email` text,
  `password` text,
  `type` text,
  `dept` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teacher_course_map`
--

CREATE TABLE `teacher_course_map` (
  `pid` int(11) NOT NULL,
  `empid` text,
  `course_id` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_course_map`
--

INSERT INTO `teacher_course_map` (`pid`, `empid`, `course_id`) VALUES
(3, '1011801', '16'),
(7, '1011802', '16'),
(8, '1011804', '16'),
(9, '1011804', '17'),
(10, '1011802', '17');

-- --------------------------------------------------------

--
-- Table structure for table `time_table`
--

CREATE TABLE `time_table` (
  `pid` int(11) NOT NULL,
  `year` text,
  `class_id` text,
  `day_of_week` text,
  `start_time` text,
  `end_time` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_table`
--
ALTER TABLE `admin_table`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `attendance_record`
--
ALTER TABLE `attendance_record`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `batch_map`
--
ALTER TABLE `batch_map`
  ADD PRIMARY KEY (`batch_id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`bid`);

--
-- Indexes for table `class_map`
--
ALTER TABLE `class_map`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `course_map`
--
ALTER TABLE `course_map`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `dept_map`
--
ALTER TABLE `dept_map`
  ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `emp_data`
--
ALTER TABLE `emp_data`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `emp_legend`
--
ALTER TABLE `emp_legend`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `fees_legend`
--
ALTER TABLE `fees_legend`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `fees_record`
--
ALTER TABLE `fees_record`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `institution_general`
--
ALTER TABLE `institution_general`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `library_details`
--
ALTER TABLE `library_details`
  ADD PRIMARY KEY (`lib_id`);

--
-- Indexes for table `manage_books`
--
ALTER TABLE `manage_books`
  ADD PRIMARY KEY (`transactionid`);

--
-- Indexes for table `marks_record_sgpa`
--
ALTER TABLE `marks_record_sgpa`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `marks_record_subjectwise`
--
ALTER TABLE `marks_record_subjectwise`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `routine_map`
--
ALTER TABLE `routine_map`
  ADD PRIMARY KEY (`period_id`);

--
-- Indexes for table `salary_map`
--
ALTER TABLE `salary_map`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `scanner_status`
--
ALTER TABLE `scanner_status`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `sem_map`
--
ALTER TABLE `sem_map`
  ADD PRIMARY KEY (`sem_id`);

--
-- Indexes for table `stream_set`
--
ALTER TABLE `stream_set`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `subject_map`
--
ALTER TABLE `subject_map`
  ADD PRIMARY KEY (`sub_id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `teacher_course_map`
--
ALTER TABLE `teacher_course_map`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `time_table`
--
ALTER TABLE `time_table`
  ADD PRIMARY KEY (`pid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_table`
--
ALTER TABLE `admin_table`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attendance_record`
--
ALTER TABLE `attendance_record`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `batch_map`
--
ALTER TABLE `batch_map`
  MODIFY `batch_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `bid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `class_map`
--
ALTER TABLE `class_map`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `course_map`
--
ALTER TABLE `course_map`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `dept_map`
--
ALTER TABLE `dept_map`
  MODIFY `dept_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `emp_data`
--
ALTER TABLE `emp_data`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `emp_legend`
--
ALTER TABLE `emp_legend`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `fees_legend`
--
ALTER TABLE `fees_legend`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fees_record`
--
ALTER TABLE `fees_record`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `institution_general`
--
ALTER TABLE `institution_general`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `library_details`
--
ALTER TABLE `library_details`
  MODIFY `lib_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `manage_books`
--
ALTER TABLE `manage_books`
  MODIFY `transactionid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `marks_record_sgpa`
--
ALTER TABLE `marks_record_sgpa`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `marks_record_subjectwise`
--
ALTER TABLE `marks_record_subjectwise`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `routine_map`
--
ALTER TABLE `routine_map`
  MODIFY `period_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `salary_map`
--
ALTER TABLE `salary_map`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `scanner_status`
--
ALTER TABLE `scanner_status`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sem_map`
--
ALTER TABLE `sem_map`
  MODIFY `sem_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `stream_set`
--
ALTER TABLE `stream_set`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `subject_map`
--
ALTER TABLE `subject_map`
  MODIFY `sub_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `teacher_course_map`
--
ALTER TABLE `teacher_course_map`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `time_table`
--
ALTER TABLE `time_table`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
