-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2018 at 05:44 PM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scholarly`
--

-- --------------------------------------------------------

--
-- Table structure for table `emp_legend`
--

CREATE TABLE `emp_legend` (
  `pid` int(11) NOT NULL,
  `emp_type` text,
  `desig` text,
  `desig_id` text,
  `salary` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_legend`
--

INSERT INTO `emp_legend` (`pid`, `emp_type`, `desig`, `desig_id`, `salary`) VALUES
(1, 'T', 'Principle, Assistant Professor', '1', NULL),
(2, 'T', 'HOD, Assistant Professor', '2', NULL),
(3, 'T', 'Assistant Professor', '3', NULL),
(4, 'T', 'Senior technical assistant\r\n', '4', NULL),
(5, 'T', 'Junior technical assistant', '5', NULL),
(6, 'N', 'Clerk', '6', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `emp_legend`
--
ALTER TABLE `emp_legend`
  ADD PRIMARY KEY (`pid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `emp_legend`
--
ALTER TABLE `emp_legend`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
