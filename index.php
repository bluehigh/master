<?php
# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

# Package classes
use Debojyoti\PhpRoute;
use Debojyoti\PdoConnect\Handler;

# Project classes
use Scholarly\Controller;
use Scholarly\Institute;

$route = new PhpRoute();

# Get uri parameters
$uri_parts = $route->getParts();

if(count($uri_parts)) {
    $controller = new Controller($uri_parts);

    # Check if request is proper
    if ($controller->isRequestProper()) {
        # Redirect accordingly
        $db = new Handler();
        $controller->setDb($db);
        $institue_id = $uri_parts[0];

        $institute = new Institute();
        $institute->setDb($db);
        $institute->set($institue_id);

        if ($institute->exists()) {
            header($controller->preparePathFor($institute));
        } else {
            require(ROOT."/views/product.php");
        }
        # Redirect to Product Display page

    } else {
        # Redirect to error page
         require(ROOT."/views/error.php");
    }    
} else {
    require(ROOT."/views/product.php");
}




