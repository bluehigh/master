<?php

?>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        
        <!--        Bootstrap css        -->
        <link href="<?php echo HOST; ?>/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="<?php echo HOST; ?>/assets/css/bootstrap-grid.css" rel="stylesheet" type="text/css">
        
        <!--            aos            -->
        <link href="<?php echo HOST; ?>/assets/css/aos.css" rel="stylesheet">
        
        <!--        `Custom css        -->
        <link href="<?php echo HOST; ?>/assets/css/global.css" rel="stylesheet" type="text/css">
        <link href="<?php echo HOST; ?>/assets/css/product.css" rel="stylesheet" type="text/css">
    </head>
    
    <body>
        <div id="preloader_parent">
              <div id="preloader-content">
                  <img src="<?php echo HOST; ?>/assets/images/preloader.gif">
              </div>
        </div>
        <div class="container-fluid" id="landing">
            <div class="row" id="custom_navbar">
                <div class="col-md-6 offset-md-6 center">
                    <div class="conatiner-fluid">
                        <div class="row">
                                <a href="#pricing_parent">
                                    <div class="col-3 white">
                                        Pricing
                                    </div>
                                </a>
                                <div class="col-3 white">
                                    About
                                </div>
                                <div class="col-3 white">
                                    The Team
                                </div>
                                <div class="col-3 white">
                                    Contact
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row justify-content-center" id="landing_logo" data-aos="zoom-in" data-aos-delay="120">
                <div class="col-12 center">
                    <img src="<?php echo HOST; ?>/assets/images/product_main_logo.png" class="img-fluid">
                </div>
                <div class="container-fluid">
                    <div class="row justify-content-center">
                    <div class="col-md-3 center">
                        <a href="student_login.php">
                            <button class="btn explore_btn" type="button">
                            Student Portal
                        </button>
                        </a>
                    </div>
                    <div class="col-md-3 center">
                        <a href="employee.php">
                            <button class="btn explore_btn" type="button">
                            Employee Portal
                        </button>
                        </a>
                    </div>
                </div>
                </div>
            </div>
            
            <div class="row justify-content-center" data-aos="fade-up" data-aos-delay="600">
                <div class="col-md-6 center">
                    <div class="landing_features">
                        <img src="<?php echo HOST; ?>/assets/images/out_of_the_box.png" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-6 center">
                    <div class="landing_features">
                        <img src="<?php echo HOST; ?>/assets/images/cross_platfrom.png" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid" id="pricing_parent">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-4 col-sm-6">
                        <div class="pricingTable">
                            <span class="icon"><i class="fa fa-globe"></i></span>
                            <div class="pricingTable-header">
                                <h3 class="title">Plan - 1</h3>
                                <span class="price-value">₹ 3500/month</span>
                            </div>
                            <ul class="pricing-content">
                                <li>Upto 3000 active students</li>
                                <li>Upto 150 active employee</li>
                                <li>Upto 10 streams</li>
                                <li>Upto 3 admin email</li>
                                <li>Unlimited subjects data</li>
                            </ul>
                            <a href="<?php echo HOST; ?>/institute/register.php?plan=1" class="pricingTable-signup">Sign Up</a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="pricingTable">
                            <span class="icon"><i class="fa fa-briefcase"></i></span>
                            <div class="pricingTable-header">
                                <h3 class="title">Plan - 2</h3>
                                <span class="price-value">₹ 5000/month</span>
                            </div>
                            <ul class="pricing-content">
                                <li>Upto 20000 active students</li>
                                <li>Upto 600 active employee</li>
                                <li>Upto 40 streams</li>
                                <li>Upto 20 admin email</li>
                                <li>Unlimited subjects data</li>
                            </ul>
                            <a href="<?php echo HOST; ?>/institute/register.php?plan=2" class="pricingTable-signup">Sign Up</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--        Scripts        -->
        <script src="<?php echo HOST; ?>/assets/js/jquery-3.3.1.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/bootstrap.min.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/aos.js"></script>
        
        <!--        Custom Scripts       -->
        <script src="<?php echo HOST; ?>/assets/js/main.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/product.js"></script>
    <!--        <script src="js/main.js"></script>-->
    </body>
</html>