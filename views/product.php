<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	
	<!--        Bootstrap css        -->
	<link href="<?php echo HOST; ?>/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo HOST; ?>/assets/css/bootstrap-grid.css" rel="stylesheet" type="text/css">
	
	<!--            aos            -->
	<link href="<?php echo HOST; ?>/assets/css/aos.css" rel="stylesheet">
	
	<!--        `Custom css        -->
	<link href="<?php echo HOST; ?>/assets/css/global.css" rel="stylesheet" type="text/css">
	<link href="<?php echo HOST; ?>/assets/css/product.css?v=1" rel="stylesheet" type="text/css">
</head>
<body>
	<div id="preloader_parent">
	      <div id="preloader-content">
	          <img src="<?php echo HOST; ?>/assets/images/preloader.gif">
	      </div>
	</div>
	<div class="container-fluid" id="landing">
		<div class="row justify-content-end pt-4">
			<div class="col-md-2 col-6 center">
				<a href="#pricing_parent"><div id="nav_login">Purchase</div></a>
			</div>
			<div class="col-md-2 col-6 center nav_link">
				<a href="#" data-toggle="modal" data-target="#exampleModalCenter">Login</a>
				<!-- href="echo HOST; /institute/login.php" --> 
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-md-5 center" id="product_logo">
				<img src="<?php echo HOST; ?>/assets/images/product_logo.png" class="img-fluid">
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-md-7 center" id="search_institute_parent">
				<input type="text" id="search_institute" class="center" placeholder="Search Institutes . . .">
			</div>
			<div class="col-md-6 col-10" id="search_institute_result">
				<!-- <div class="result"><a href="">Bengal Institute of Technolog</a></div>
				<div class="result"><a href="">Bengal Institute of Technolog</a></div> -->
			</div>
		</div>
	</div>
	<div class="container-fluid" id="pricing_parent">
	    <div class="container">
	        <div class="row justify-content-center">
	            <div class="col-md-4 col-sm-6">
	                <div class="pricingTable">
	                    <span class="icon"><i class="fa fa-globe"></i></span>
	                    <div class="pricingTable-header">
	                        <h3 class="title">Standard</h3>
	                        <span class="price-value">₹ 3500/month</span>
	                    </div>
	                    <ul class="pricing-content">
	                        <li>Upto 3000 active students</li>
	                        <li>Upto 150 active employee</li>
	                        <li>Upto 10 streams</li>
	                        <li>Upto 3 admin email</li>
	                        <li>Unlimited subjects data</li>
	                    </ul>
	                    <a  href="<?php echo HOST; ?>/institute/register.php?plan=1" class="pricingTable-signup">Sign Up</a>
	                </div>
	            </div>
	            <div class="col-md-4 col-sm-6">
	                <div class="pricingTable">
	                    <span class="icon"><i class="fa fa-briefcase"></i></span>
	                    <div class="pricingTable-header">
	                        <h3 class="title">Ultimate</h3>
	                        <span class="price-value">₹ 5000/month</span>
	                    </div>
	                    <ul class="pricing-content">
	                        <li>Upto 20000 active students</li>
	                        <li>Upto 600 active employee</li>
	                        <li>Upto 40 streams</li>
	                        <li>Upto 20 admin email</li>
	                        <li>Unlimited subjects data</li>
	                    </ul>
	                    <a  href="<?php echo HOST; ?>/institute/register.php?plan=2" class="pricingTable-signup">Sign Up</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="height: 80vh">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	       
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body" style="text-align: center; padding: 7em 0em;">
	      		<a href="<?php echo HOST; ?>/employee/login.php"><button type="button" class="btn login_btn">Employee Login</button></a>
	      		<a href="<?php echo HOST; ?>/student/login.php"><button type="button" class="btn login_btn">Student Login</button></a>
	      		<a href="<?php echo HOST; ?>/institute/login.php"><button type="button" class="btn login_btn">Institute Login</button></a>
	      </div>
	      <div class="modal-footer">
	      </div>
	    </div>
	  </div>
	</div>

    <!--        Scripts        -->
    <script src="<?php echo HOST; ?>/assets/js/jquery-3.3.1.js"></script>
    <script src="<?php echo HOST; ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo HOST; ?>/assets/js/aos.js"></script>
    
    <!--        Custom Scripts       -->
    <script src="<?php echo HOST; ?>/assets/js/init.js"></script>
    <script src="<?php echo HOST; ?>/assets/js/main.js"></script>
    <script src="<?php echo HOST; ?>/assets/js/product.js"></script>
	<!--        <script src="js/main.js"></script>-->
</body>
</html>