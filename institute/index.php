<?php    
# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;

# If institute id is not passed 
if (!isset($_GET['unique_id'])) {
    # Send to select institute page
    require(ROOT."/views/product.php");
    exit;
} 

# If unique id is passed, check it
$institute = new Institute($_GET['unique_id']);
$db = new Handler();
$institute->setDb($db);

if (!$institute->exists()) {
    # Send to select institute page 
     require(ROOT."/views/product.php");
     exit;
}

# Get institute name 
$name = $institute->get("institute_name")["institute_name"];
?>

<html>
<head>
    <title><?php echo $name; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
    <!--        Bootstrap css        -->
    <link href="<?php echo HOST; ?>/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<?php echo HOST; ?>/assets/css/bootstrap-grid.css" rel="stylesheet" type="text/css">
    
    <!--            aos            -->
    <link href="<?php echo HOST; ?>/assets/css/aos.css" rel="stylesheet">
    
    <!--        `Custom css        -->
    <link href="<?php echo HOST; ?>/assets/css/global.css" rel="stylesheet" type="text/css">
    <link href="<?php echo HOST; ?>/assets/css/institute.css" rel="stylesheet" type="text/css">
</head>
<body>
<!--     <h3><?php echo $name; ?> home page</h3>
    <a href="<?php echo HOST; ?>/institute/login.php?unique_id=<?php echo $_GET['unique_id'];?>">Institute Login</a>
    <a href="<?php echo HOST; ?>/employee/login.php?unique_id=<?php echo $_GET['unique_id'];?>">Employee Login</a>
    <a href="<?php echo HOST; ?>/student/login.php?unique_id=<?php echo $_GET['unique_id'];?>">Student Login</a> -->
    <div id="preloader_parent">
          <div id="preloader-content">
              <img src="<?php echo HOST; ?>/assets/images/preloader.gif">
          </div>
    </div>
    <div class="container-fluid">
    	<div class="row">
    		<div class="col-md-10 left" style="padding-left: 8em">
    			<a href="<?php echo HOST; ?>"><img src="<?php echo HOST; ?>/assets/images/institute_index_logo.png">
    		</div>
    		<div class="col-md-2 center" style="padding-top: 18px"><a href="<?php echo HOST; ?>/institute/login.php?unique_id=<?php echo $_GET['unique_id'];?>">Institute Login</a></div>
    	</div>
    </div>
    <div class="container-fluid">
    	<div class="row">
    		<div class="col-12" id="landing">
    			<div class="container">
    				<div class="row pt-5">
    					<div class="col-12 left pt-3">
    						<h3 style="color: white"><?php echo $name; ?></h3>
    						<h6 style="color: white;margin-top: 1.5em">Code : <?php echo $_GET['unique_id'];?></h6>
    					</div>
    				</div>
    			</div>
    		</div>
    		<div class="col-12" style="margin-top: -12em!important">
    			<div class="row justify-content-center">
    				<div class="col-md-4">
    					<div class="card">
    						<div class="container-fluid">
    							<div class="row justify-content-center">
    								<div class="col-5">
    									<img src="<?php echo HOST; ?>/assets/images/student_avatar.png" class="img-fluid rounded-circle">
    								</div>
    								<div class="col-11 center">
    									<a href="<?php echo HOST; ?>/student/login.php?unique_id=<?php echo $_GET['unique_id'];?>"><button type="button" class="btn login_btn">Student Login</button></a>
    								</div>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="col-md-4">
    					<div class="card">
    						<div class="container-fluid">
    							<div class="row justify-content-center">
    								<div class="col-5">
    									<img src="<?php echo HOST; ?>/assets/images/employee_avatar.png" class="img-fluid rounded-circle">
    								</div>
    								<div class="col-11 center">
    									<a href="<?php echo HOST; ?>/employee/login.php?unique_id=<?php echo $_GET['unique_id'];?>"><button type="button" class="btn login_btn">Employee Login</button></a>
    								</div>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <!--        Scripts        -->
    <script src="<?php echo HOST; ?>/assets/js/jquery-3.3.1.js"></script>
    <script src="<?php echo HOST; ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo HOST; ?>/assets/js/aos.js"></script>
    
    <!--        Custom Scripts       -->
    <script src="<?php echo HOST; ?>/assets/js/init.js"></script>
    <script src="<?php echo HOST; ?>/assets/js/main.js"></script>
</body>
</html>