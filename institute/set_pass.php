<?php
# Load without controller

# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
#   Check for Institution ID
if (!isset($_GET['unique_id'], $_GET['otp'])) {
    header("Location: ".HOST);
    exit;
}
$db = new Handler();
$institute = new Institute($_GET['unique_id']);
$institute->setDb($db);
# Check if otp requested
if (!$institute->exists() || !$institute->otpRequested()) {
    header("Location: ".HOST);
    exit;
}
# Verify otp
if (!$institute->verifyOtp($_GET['otp'])) {
    header("Location: ".HOST);
    exit;
}
# If all conditions are true
$name = $institute->get('institute_name');
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--        Bootstrap css        -->
        <link href="<?php echo HOST; ?>/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="<?php echo HOST; ?>/assets/css/bootstrap-grid.css" rel="stylesheet" type="text/css">
        
        <!--            aos            -->
        <link href="<?php echo HOST; ?>/assets/css/aos.css" rel="stylesheet">
        

        <!--        `Custom css        -->
        <link href="<?php echo HOST; ?>/assets/css/global.css" rel="stylesheet" type="text/css">
        <link href="<?php echo HOST; ?>/assets/css/register.css" rel="stylesheet" type="text/css">
    </head>
    
    <body>
        <div id="preloader_parent">
              <div id="preloader-content">
                  <img src="<?php echo HOST; ?>/assets/images/preloader.gif">
              </div>
        </div>

        <div class="container">
            <div class="py-5 text-center">
              <img class="d-block mx-auto mb-4" src="<?php echo HOST; ?>/assets/images/password.png" alt="" width="92" height="92">
              <h2>Set password for <?php echo $name['institute_name'];?></h2>
              <p class="lead"></p>
            </div>

            <div class="row justify-content-center">
              <div class="col-md-8 order-md-1">
                <form class="needs-validation" novalidate action="<?php echo HOST; ?>/app/api/admin/institute_register.php" method="POST">
                  <div class="row">
                    <div class="col-md-12 mb-3">
                      <label for="firstName">Set Password (Minimum length : 10)</label>
                      <input type="password" class="form-control" id="password" name="password" placeholder="" value="" required>
                      <div class="invalid-feedback">
                        Valid password is required.
                      </div>
                    </div>
                    <div class="col-md-12 mb-3">
                      <label for="lastName">Confirm Password</label>
                      <input type="password" class="form-control" id="retype_password" placeholder="" value="" name="retype_password" required>
                      <div class="invalid-feedback">
                        Retype same password.
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="action" value="set_password">
                  <input type="hidden" name="unique_id" value="<?php echo $_GET['unique_id'];?>">
                  <input type="hidden" name="otp" value="<?php echo $_GET['otp'];?>">
                  <hr class="mb-4">
                  <button name="register" class="btn btn-secondary btn-lg btn-block" type="submit" id="submit_password" disabled>Set Password</button>
                </form>
              </div>
            </div>



        <!--        Scripts        -->
        <script src="<?php echo HOST; ?>/assets/js/jquery-3.3.1.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/bootstrap.min.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/aos.js"></script>
        <!--        Custom Scripts       -->
        <script src="<?php echo HOST; ?>/assets/js/main.js"></script>
        <script type="text/javascript">
            // Bootstrap form validation code
            (function() {
               'use strict';

               window.addEventListener('load', function() {
                 // Fetch all the forms we want to apply custom Bootstrap validation styles to
                 var forms = document.getElementsByClassName('needs-validation');

                 // Loop over them and prevent submission
                 var validation = Array.prototype.filter.call(forms, function(form) {
                   form.addEventListener('submit', function(event) {
                     if (form.checkValidity() === false) {
                       event.preventDefault();
                       event.stopPropagation();
                       console.log("error");
                     }
                     form.classList.add('was-validated');
                   }, false);
                 });
               }, false);
             })();
            flag = 1;
            function checkPasswords() {
                if (($('#password').val().length >= 10) && ($('#retype_password').val() == $('#password').val())) {
                    if(flag == 1) {
                        $('#submit_password').removeAttr('disabled');
                        $('#submit_password').removeClass('btn-secondary');
                        $('#submit_password').addClass('btn-primary');
                        flag = 0;
                    }
                } else {
                    if(flag == 0) {
                        $('#submit_password').prop('disabled', true);
                        $('#submit_password').addClass('btn-secondary');
                        $('#submit_password').removeClass('btn-primary');
                        flag = 1;
                    }
                }
            }
            $('#retype_password').keyup(function() {
                checkPasswords();
            });
            $('#password').keyup(function() {
                checkPasswords();
            });
        </script>
    <!--        <script src="js/main.js"></script>-->
    </body>
</html>