<?php
# Load without controller

# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';
//  Redirect if no plan is selected
if(!isset($_GET['plan'])) {
    header("Location: ".HOST);
    exit(0);
}

?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--        Bootstrap css        -->
        <link href="<?php echo HOST; ?>/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="<?php echo HOST; ?>/assets/css/bootstrap-grid.css" rel="stylesheet" type="text/css">
        
        <!--            aos            -->
        <link href="<?php echo HOST; ?>/assets/css/aos.css" rel="stylesheet">
        

        <!--        `Custom css        -->
        <link href="<?php echo HOST; ?>/assets/css/global.css" rel="stylesheet" type="text/css">
        <link href="<?php echo HOST; ?>/assets/css/register.css" rel="stylesheet" type="text/css">

        <script type="text/javascript">
            //  Getting selected plan from GET request
            var plan = <?php echo $_GET['plan']; ?>;
        </script>
    </head>
    
    <body>
        <div id="preloader_parent">
              <div id="preloader-content">
                  <img src="<?php echo HOST; ?>/assets/images/preloader.gif">
              </div>
        </div>

        <div class="container">
            <div class="py-5 text-center">
              <img class="d-block mx-auto mb-4" src="<?php echo HOST; ?>/assets/images/register.jpg" alt="" width="92" height="92">
              <h2>Institute Registration</h2>
              <p class="lead"></p>
            </div>

            <div class="row justify-content-center">
              <div class="col-md-8 order-md-1">
                <h4 class="mb-3">Plan selected : <?php echo $_GET['plan'];?></h4>
                <form class="needs-validation" novalidate action="<?php echo HOST; ?>/app/api/admin/institute_register.php" method="POST">
                  <div class="row">
                    <div class="col-md-12 mb-3">
                      <label for="firstName">Institute name</label>
                      <input type="text" class="form-control" id="institute_name" name="institute_name" placeholder="" value="" required>
                      <div class="invalid-feedback">
                        Valid institute name is required.
                      </div>
                    </div>
                    <div class="col-md-12 mb-3">
                      <label for="lastName">University Name</label>
                      <input type="text" class="form-control" id="university_name" placeholder="" value="" name="university_name" required>
                      <div class="invalid-feedback">
                        Valid university name is required.
                      </div>
                    </div>
                    <div class="col-md-12 mb-3">
                      <label for="registration_id">Registration ID</label>
                      <input type="text" class="form-control" id="registration_id" placeholder="" value="" name="registration_id" required>
                      <div class="invalid-feedback">
                        A valid registration id is required.
                      </div>
                    </div>
                  </div>


                  <div class="mb-3">
                    <label for="email">Primary Email <span class="text-muted">(Will be used for verification)</span></label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="you@example.com" >
                    <div class="invalid-feedback">
                      Please enter a valid email address.
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6 mb-3">
                      <input type="hidden" name="type" value="Engineering">
                    </div>

                  </div>
                  <input type="hidden" name="action" value="add_institute">
                  <input type="hidden" name="plan" value="<?php echo $_GET['plan'];?>">
                  <hr class="mb-4">
                  <button name="register" class="btn btn-primary btn-lg btn-block" type="submit">Verify Details</button>
                </form>
              </div>
            </div>

        <!--        Scripts        -->
        <script src="<?php echo HOST; ?>/assets/js/jquery-3.3.1.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/bootstrap.min.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/aos.js"></script>
        <!--        Custom Scripts       -->
        <script src="<?php echo HOST; ?>/assets/js/main.js"></script>
        <script>
          // Bootstrap form validation code
          (function() {
             'use strict';

             window.addEventListener('load', function() {
               // Fetch all the forms we want to apply custom Bootstrap validation styles to
               var forms = document.getElementsByClassName('needs-validation');

               // Loop over them and prevent submission
               var validation = Array.prototype.filter.call(forms, function(form) {
                 form.addEventListener('submit', function(event) {
                   if (form.checkValidity() === false) {
                     event.preventDefault();
                     event.stopPropagation();
                     console.log("error");
                   }
                   form.classList.add('was-validated');
                 }, false);
               });
             }, false);
           })();
        </script>
    </body>
</html>