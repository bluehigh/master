<?php
    
# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

properRedirectTo("institute");

?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        
        <!--        Bootstrap css        -->
        <link href="<?php echo HOST; ?>/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="<?php echo HOST; ?>/assets/css/bootstrap-grid.css" rel="stylesheet" type="text/css">
        
        <!--            aos            -->
        <link href="<?php echo HOST; ?>/assets/css/aos.css" rel="stylesheet">
        
        <!--        `Custom css        -->
        <link href="<?php echo HOST; ?>/assets/css/global.css" rel="stylesheet" type="text/css">
        <link href="<?php echo HOST; ?>/assets/css/institute_login.css" rel="stylesheet" type="text/css">
    </head>
    
    <body>
        <div id="preloader_parent">
              <div id="preloader-content">
                  <img src="<?php echo HOST; ?>/assets/images/preloader.gif">
              </div>
        </div>
        <div class="container-fluid" id="landing">
           <div class="row">
                <div class="col-md-8" id="landing_logo_parent">
                    <div class="row justify-content-center">
                        <div class="col-5" id="main_logo" data-aos="slide-up" data-aos-duration="1000">
                            <img src="<?php echo HOST; ?>/assets/images/institute_login_main_logo.png" class="img-fluid">
                        </div>
                    </div>
                </div> 
                <div class="col-md-4" id="form_section" data-aos="fade-left" data-aos-delay="1400">
                    <div class="row justify-content-center">
                      
                    </div>
                     <div class="row justify-content-center">
                        <div class="col-12 center">
                            <!-- <h6 class="college_name blue">Bengal Institute of Technology</h6> -->
                        </div>
                    </div>
                    <div class="row justify-content-center" id="form_parent">
                        <!--                login form              -->
                        <div class="col-10 center" id="login_form_parent">
                            <input type="text" name="institute_id_login" id="institute_id_login" class="input" placeholder="Institute ID / Admin Email" readonly  onfocus="this.removeAttribute('readonly');" <?php if (isset($_GET['unique_id'])) { echo "value=".$_GET['unique_id']; }?>>
                            <input type="password" name="password_login" id="password_login" class="input" placeholder="Password"   readonly  onfocus="this.removeAttribute('readonly');">
                            <button type="button" class="btn" id="login_btn">Login</button>
                            <div class="container-fluid form_links_parent">
                                <div class="row center">
                                    <div class="col-6 center">
                                        <a href="#" class="black links form_links otp_received">OTP Recieved?</a>
                                    </div>
                                    <div class="col-6 center">
                                        <a href="#" class="black links form_links reset_link">Reset Password</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--                /login form              -->
                        <!--                      reset form                          -->
                        <div class="col-10 center" id="reset_form_parent">
                            <input type="text" name="employee_id_reset" id="employee_id_reset" class="input" placeholder="Employee ID">
                            <input type="text" name="email_reset" id="email_reset" class="input" placeholder="Registered Email ID">
                            <button type="button" class="btn login_btn" id="reset_btn">Send OTP</button>
                            <div class="container-fluid form_links_parent">
                                <div class="row center">
                                    <div class="col-6 center">
                                        <a href="#" class="black links form_links otp_received">OTP Recieved?</a>
                                    </div>
                                    <div class="col-6 center">
                                        <a href="#" class="black links form_links normal_login">Normal Login</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--                      /reset form                          -->
                        <!--                      otp form                          -->
                        <div class="col-10 center" id="otp_form_parent">
                            <input type="text" name="employee_id_otp" id="employee_id_otp" class="input" placeholder="Employee ID">
                            <input type="password" name="otp" id="otp" class="input" placeholder="Enter OTP Here">
                            <button type="button" class="btn" id="procced_btn">Procced</button>
                            <div class="container-fluid form_links_parent">
                                <div class="row center">
                                    <div class="col-6 center">
                                        <a href="#" class="black links form_links normal_login">Normal Login</a>
                                    </div>
                                    <div class="col-6 center">
                                        <a href="#" class="black links form_links reset_link">Reset Password</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--                      /otp form                          -->
                        <!--                      change passwprd form                          -->
                        <div class="col-10 center" id="change_password_form_parent">
                            <input type="password" name="new_password" id="new_password" class="input" placeholder="New Password">
                            <input type="password" name="retype_password" id="retype_password" class="input" placeholder="Retype Password">
                            <button type="button" class="btn save_btn">Save</button>
                            <div class="container-fluid form_links_parent">
                                <div class="row center">
                                    <div class="col-12 center">
                                        <a href="#" class="black links form_links normal_login">Normal Login</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--                      /change password form                          -->
                        <div class="container">
                                <div class="row justify-content-center">
                                    <div class="center" id="error_display">
                                       
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
           </div>
        </div>
                
        <!--        Scripts        -->
        <script src="<?php echo HOST; ?>/assets/js/jquery-3.3.1.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/bootstrap.min.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/aos.js"></script>
        
        <!--        Custom Scripts       -->
        <script src="<?php echo HOST; ?>/assets/js/init.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/main.js"></script>
        <script type="text/javascript">
            
            //  Hide all other forms at first load
            $('#otp_form_parent').hide();
            $('#reset_form_parent').hide();
            $('#change_password_form_parent').hide();

            /*  General function to handle form toggle  */
            function formToggle(show_form) {
                $('#otp_form_parent').hide();
                $('#reset_form_parent').hide();
                $('#change_password_form_parent').hide();
                 $('#login_form_parent').hide();
                $('#'+show_form).show();
            }

            //  If otp_received link is clicked
            $('.otp_received').click(function() {
                errorToggle(0);
                formToggle('otp_form_parent');
            });

            //  If normal_login link is clicked
            $('.normal_login').click(function() {
                errorToggle(0);
                formToggle('login_form_parent');
            });

            //  If normal_login link is clicked
            $('.reset_link').click(function() {
                errorToggle(0);
                formToggle('reset_form_parent');
            });

            //  Function to display error (toggle=0/1) 1=display
            function errorToggle(toggle,msg=null) {
                if(toggle) {
                    $("#error_display").show();
                    $("#error_display").html(msg);
                } else {
                     $("#error_display").hide();
                }
            }


            /*  Handle Ajax requests */
            $("#login_btn").click(function() {
              var institute_id_login = $('#institute_id_login').val();
              var password_login = $('#password_login').val();

              if(password_login == '' || institute_id_login == '')
              {
                errorToggle(1,"Fields cannot be empty");
              }
               else if (!($.isNumeric(institute_id_login))) {
                 errorToggle(1,"Login ID || NUMERIC ONLY");
               }
               else if (password_login.length<8) {
                 errorToggle(1,"Minimum password length is 8");
                }
               else {

                 //var params = {'login':'login','employee_id':institute_id_login,'password':password_login};
                 var params = {'login':'login',
                                'unique_id':institute_id_login,
                                'password':password_login
                 };
                 $.ajax({
                   url: host+'/app/api/admin/institute_login.php',
                   type:  'post',
                   data: params,
                   dataType:  'json',
                   error: function(data){
                     errorToggle(1,data.responseText);
                     console.log(data.responseText);
                   },
                   success: function(data){
                     // console.log(data);
                     if(data.auth!=true){
                       errorToggle(1,"Incorrect Username or password");
                     } else {
                        errorToggle(0);
                        window.location = host+"/institute/dashboard/index.php";
                     }
                   }
                 });
               }
               //errorToggle(1,"Fill all the fields");
            });


            /* Handle reset password Ajax starts */
            $("#reset_btn").click(function () {
              var college_id_reset = $("#college_id_reset").val();
              var email_to_reset = $("#email_reset").val();

              var email_pattern_check = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i

              if (!($.isNumeric(college_id_reset))) {
                errorToggle(1,"Login ID || NUMERIC ONLY");
              }
              else if (!email_pattern_check.test(email_to_reset)) {
                errorToggle(1,"Enter a valid Email");
              }
              else {
                errorToggle(0);
                var params = {'reset':'reset',
                              'college_id':college_id_reset,
                              'email':email_to_reset,
                              'collegeUniqueID':'121'
                };

                $.ajax({
                  url:  'app/api/student_login_api.php',
                  method: 'post',
                  dataType: 'json',
                  data: params,
                  error: function(data){
                    console.log(data.responseText);
                    errorToggle(1,data.responseText);
                  },
                  success: function (data) {
                    console.log(data);

                  }
                });
              }

            })
            /* Handle reset password Ajax ends */
        </script>
    <!--        <script src="assets/js/main.js"></script>-->
    </body>
</html>