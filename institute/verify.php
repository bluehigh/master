<?php
# Load without controller

# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

# Check if unique_id is present in GET request
if(!isset($_GET['unique_id'])) {
    header("Location: ".HOST);
    exit(0);
}
$arrContextOptions=array(
    "ssl"=>array(
        "verify_peer"=>false,
        "verify_peer_name"=>false,
    ),
); 
# Check if verification required
$response = file_get_contents(HOST."/app/api/admin/institute_register.php?action=otp_verify&unique_id=".$_GET['unique_id'] , false, stream_context_create($arrContextOptions));
// print_r(json_decode($response,true));
# If response is not an array, request is invalid
if(!is_array(json_decode($response,true))) {
    // header("Location: ".HOST);
    exit(0);
}
$institute_name = json_decode($response, true)['institute_name'];
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--        Bootstrap css        -->
        <link href="<?php echo HOST; ?>/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="<?php echo HOST; ?>/assets/css/bootstrap-grid.css" rel="stylesheet" type="text/css">
        
        <!--            aos            -->
        <link href="<?php echo HOST; ?>/assets/css/aos.css" rel="stylesheet">

        <!--        `Custom css        -->
        <link href="<?php echo HOST; ?>/assets/css/global.css" rel="stylesheet" type="text/css">
        <link href="<?php echo HOST; ?>/assets/css/register.css" rel="stylesheet" type="text/css">
    </head>
    
    <body>
        <div id="preloader_parent">
              <div id="preloader-content">
                  <img src="<?php echo HOST; ?>/assets/images/preloader.gif">
              </div>
        </div>

        <div class="container">
            <div class="py-5 text-center">
               <img class="d-block mx-auto mb-4" src="<?php echo HOST; ?>/assets/images/otp.png" alt="" width="92" height="92">
              <h2>Verification for <?php echo $institute_name; ?></h2>
              <p class="lead"></p>
            </div>

            <div class="row justify-content-center">
              <div class="col-md-8 order-md-1">
                <form class="needs-validation" novalidate action="<?php echo HOST; ?>/app/api/admin/institute_register.php" method="POST">
                  <div class="row">
                    <div class="col-md-12 mb-3">
                      <label for="firstName">Enter OTP</label>
                      <input type="text" class="form-control" id="institute_name" name="otp" placeholder="" value="" required>
                      <div class="invalid-feedback">
                        Valid otp is required.
                      </div>
                    </div>
                  <input type="hidden" name="unique_id" value="<?php echo $_GET['unique_id']; ?>">
                  <input type="hidden" name="action" value="verify_institute">
                  <hr class="mb-4">
                  <button name="submit" class="btn btn-primary btn-lg btn-block" type="submit">Procced</button>
                </form>
              </div>
            </div>



        <!--        Scripts        -->
        <script src="<?php echo HOST; ?>/assets/js/jquery-3.3.1.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/bootstrap.min.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/aos.js"></script>
        <!--        Custom Scripts       -->
        <script src="<?php echo HOST; ?>/assets/js/main.js"></script>
        <script>
          // Bootstrap form validation code
          (function() {
             'use strict';

             window.addEventListener('load', function() {
               // Fetch all the forms we want to apply custom Bootstrap validation styles to
               var forms = document.getElementsByClassName('needs-validation');

               // Loop over them and prevent submission
               var validation = Array.prototype.filter.call(forms, function(form) {
                 form.addEventListener('submit', function(event) {
                   if (form.checkValidity() === false) {
                     event.preventDefault();
                     event.stopPropagation();
                     console.log("error");
                   }
                   form.classList.add('was-validated');
                 }, false);
               });
             }, false);
           })();
        </script>
    </body>
</html>