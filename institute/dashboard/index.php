<?php

# Project init file
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

if (!sessionSetFor('institute')) {
    session_destroy();
    header("Location: ".HOST."/institute/login.php");
}

if (isset($_GET['logout'])) {
  session_destroy();
  header("Location: ".HOST."/institute/login.php");
}

require ROOT."/app/helpers/institute_check.php";

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Hod;
use Scholarly\Teacher;
use Scholarly\Department;
use Scholarly\Course;
use Scholarly\Batch;

$db = new Handler();
Department::setDb($db);
Course::setDb($db);
Batch::setDb($db);

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$where1 = [
  'status' => '1',
  'unique_id' => $_SESSION['unique_id']
];
$where2 = [
  'status' => '1',
  'student_id' => '~'.substr($_SESSION['unique_id'],0,3)
];
// $where3 = [
//   'unique_id' => $_SESSION['unique_id']
// ];
$activeEmps = count($db->getData('emp_data',$where1));
$activeStudents = count($db->getData('students',$where2));
//$activeCourses = count($db->getData('dept_map',$where3));
$fetched_desigs = $institute->getAllDesignations();

$fetched_dept_ids = $institute->getDeptIds();

$dept_array = [];
$course_array = [];
$batch_array = [];
$student_array = [];
$employee_array = [];
$teacher_array = [];

$fetched_course_ids = 0;
$d = 0;

if (count($fetched_dept_ids)) {
  foreach ($fetched_dept_ids as $dept_id) {

    $dept = new Department($dept_id);
    $dept->fetchDetails();
    $dept_array[$dept->getId()] = $dept->getName();
    $c = $dept->getCourseIds();

    print_r($c);

    $d += count($c);
    

    // if (count($fetched_course_ids)) {
    //   foreach ($fetched_course_ids as $course_id) {

    //     $course = new Course($course_id);
    //     $course->fetchDetails();

    //     $course_array[$course->getId()] = $course->getName();

    //     $fetched_batch_ids = $course->getBatchIds();

    //     if (count($fetched_batch_ids)) {
    //       foreach ($fetched_batch_ids as $batch_id) {
    //         $batch = new Batch($batch_id);
    //         $batch->fetchDetails();

    //         $fetched_student_ids = $batch->getAllStudentIds();

    //         if ($fetched_student_ids) {
    //           foreach ($fetched_student_ids as $student_id) {
    //             $student_array[] = $student_id;
    //           }
    //         }
    //       }
    //     }

    //   }
    // }
  }
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Home </title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
    </script>
    <style type="text/css">

    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>

        <div class="right_col" role="main">
        <div class="">
            <div class="row">
              <div class="col-md-12">
                <div class="x_panel" >
                  <div class="x_title">
                    <h2><i class="fas fa-home"></i> &nbsp;&nbsp;Home</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div>
                        <div class="container-fluid">

                            <div class="row">
                              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                  <div class="tile-stats">
                                    <div class="icon"><i class="fa fa-graduation-cap" style="font-size: 3.5em"></i></div>
                                    <div class="count"><?php echo $activeStudents; ?></div>
                                    <h3>Active Students</h3>
                                    <p>(All Current Sessions)</p>
                                  </div>
                                </div>
                                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="tile-stats">
                                      <div class="icon"><i class="fa fa-briefcase" style="font-size: 3.5em"></i></div>
                                      <div class="count"><?php echo $activeEmps; ?></div>
                                      <h3>Active Employee</h3>
                                      <p>&nbsp;</p>
                                    </div>
                                  </div>
                                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="tile-stats">
                                      <div class="icon"><i class="fa fa-book" style="font-size: 3.5em"></i></div>
                                      <div class="count"><?php echo $d;?></div>
                                      <h3>Active Courses</h3>
                                      <p>&nbsp;</p>
                                    </div>
                                  </div>
                            </div>

                            <div class="row">


                            </div>


                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        </div>


      </div>
    </div>

    <?php require_once('components/scripts.php'); ?>
    <script type="text/javascript">


        $(document).ready(function () {


        });


    </script>
  </body>
</html>