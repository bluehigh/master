<?php
    
# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

if (!sessionSetFor('institute')) {
    session_destroy();
    header("Location: ".HOST."/institute/dashboard/login.php");
}

require ROOT."/app/helpers/institute_check.php";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Manage Courses </title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
    </script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>
        <div class="right_col" role="main">
          <div class="">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel" >
                  <div class="x_title">
                    <h2>Manage Courses</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12">
                                    <table id="courseTable" class="display" style="width:100%">
                                      <thead>
                                        <tr>
                                          <th>Department</th>
                                          <th>Course Name</th>
                                          <th>Course Code</th>
                                          <th>Span</th>
                                          <th>Status</th>
                                          <th>Manage</th>
                                        </tr>
                                      </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php require_once('components/scripts.php'); ?>
    <script type="text/javascript">
        var editor;
        $(document).ready(function () {
          editor = new $.fn.dataTable.Editor({
            ajax: host+"/app/api/admin/course_data.php",
            table: "#courseTable",
            fields: [{
                label: "Course Name:",
                name: "course.course_name"
              },{
                label: "Course Code:",
                name: "course.course_code"
              }, {
                label: "Course Span:",
                name: "course.course_span"
              }, {
                label: "Department:",
                name: "course.dept_id",
                type: 'select'
              }, {
                label: "Staus:",
                name: "course.status_code",
                type: 'select'
              }
            ]
          });
          $('#courseTable').DataTable( {
            dom: "Bfrtip",
            ajax: host+"/app/api/admin/course_data.php?action=view",
            responsive: true,
            "columns": [
              { "data": "dept.name" },
              { "data": "course.course_name" },
              { "data": "course.course_code" },
              { "data": "course.course_span" },
              { "data": "status.name" },
              { "data": "course.manage",
                "render" : function(data, type, row) {
                    return "<a href='"+host+"/institute/dashboard/semesters.php?course_id="+data+"'><button type='button' class='btn'>Manage</button></a>";
                }
              }
            ],
             select: 'single',
             buttons: [
              { extend: "create", editor: editor },
              { extend: "edit", editor: editor }
            ]
          } );
          editor.on('onInitEdit', function() {
            editor.enable('course.status_code');
          });
          editor.on('onInitCreate', function() {
            editor.disable('course.status_code');
          });
          editor.on('onInitEdit', function() {
            editor.disable('course.dept_id');
          });
          editor.on('onInitCreate', function() {
            editor.enable('course.dept_id');
          });
          editor.on('onInitEdit', function() {
            editor.disable('course.course_span');
          });
          editor.on('onInitCreate', function() {
            editor.enable('course.course_span');
          });
        });
    </script>
  </body>
</html>