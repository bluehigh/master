<?php
    
# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';
properRedirectTo("student");

session_start();

if (isset($_GET['logout'])) {
    session_destroy();
    header("Location: ".HOST."/student/login.php");
}


?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        
        <!--        Bootstrap css        -->
        <link href="<?php echo HOST; ?>/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="<?php echo HOST; ?>/assets/css/bootstrap-grid.css" rel="stylesheet" type="text/css">
        
        <!--            aos            -->
        <link href="<?php echo HOST; ?>/assets/css/aos.css" rel="stylesheet">
        
        <!--        `Custom css        -->
        <link href="<?php echo HOST; ?>/assets/css/global.css" rel="stylesheet" type="text/css">
        <link href="<?php echo HOST; ?>/assets/css/student_login.css" rel="stylesheet" type="text/css">
    </head>
    
    <body>
        <div id="preloader_parent">
              <div id="preloader-content">
                  <img src="<?php echo HOST; ?>/assets/images/preloader.gif">
              </div>
        </div>
        <div class="container-fluid" id="landing">
           <div class="row">
                <div class="col-md-8" id="landing_logo_parent">
                    <div class="row justify-content-center">
                        <div class="col-5" id="main_logo" data-aos="slide-up" data-aos-duration="1000">
                            <img src="<?php echo HOST; ?>/assets/images/student_login_main_logo.png" class="img-fluid">
                        </div>
                    </div>
                </div> 
                <div class="col-md-4" id="form_section" data-aos="fade-left" data-aos-delay="1400">
                    <div class="row justify-content-center">
                      
                    </div>
                     <div class="row justify-content-center">
                        <div class="col-12 center">
                            <!-- <h6 class="college_name blue">Bengal Institute of Technology</h6> -->
                        </div>
                    </div>
                    <div class="row justify-content-center" id="form_parent">
                        <!--                login form              -->
                        <div class="col-10 center" id="login_form_parent">
                            <input type="text" name="college_id_login" id="college_id_login" class="input" placeholder="Student ID" readonly  onfocus="this.removeAttribute('readonly');" <?php if (isset($_GET['unique_id'])) { echo "value=".$_GET['unique_id']; }?>>
                            <input type="password" name="password_login" id="password_login" class="input" placeholder="Password"   readonly  onfocus="this.removeAttribute('readonly');">
                            <button type="button" class="btn" id="login_btn">Login</button>
                            <div class="container-fluid form_links_parent">
                                <div class="row center">
                                    <div class="col-6 center">
                                        <a href="#" class="black links form_links otp_received">OTP Recieved?</a>
                                    </div>
                                    <div class="col-6 center">
                                        <a href="#" class="black links form_links reset_link">Reset Password</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--                /login form              -->
                        <!--                      reset form                          -->
                        <div class="col-10 center" id="reset_form_parent">
                            <input type="text" name="college_id_reset" id="college_id_reset" class="input" placeholder="Student ID">
                            <input type="text" name="email_reset" id="email_reset" class="input" placeholder="Registered Email ID">
                            <button type="button" class="btn login_btn" id="reset_btn">Send OTP</button>
                            <div class="container-fluid form_links_parent">
                                <div class="row center">
                                    <div class="col-6 center">
                                        <a href="#" class="black links form_links otp_received">OTP Recieved?</a>
                                    </div>
                                    <div class="col-6 center">
                                        <a href="#" class="black links form_links normal_login">Normal Login</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--                      /reset form                          -->
                        <!--                      otp form                          -->
                        <div class="col-10 center" id="otp_form_parent">
                            <input type="text" name="college_id_otp" id="college_id_otp" class="input" placeholder="Student ID">
                            <input type="password" name="otp" id="otp" class="input" placeholder="Enter OTP Here">
                            <button type="button" class="btn" id="procced_btn">Procced</button>
                            <div class="container-fluid form_links_parent">
                                <div class="row center">
                                    <div class="col-6 center">
                                        <a href="#" class="black links form_links normal_login">Normal Login</a>
                                    </div>
                                    <div class="col-6 center">
                                        <a href="#" class="black links form_links reset_link">Reset Password</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--                      /otp form                          -->
                        <!--                      change passwprd form                          -->
                        <div class="col-10 center" id="change_password_form_parent">
                            <input type="password" name="new_password" id="new_password" class="input" placeholder="New Password">
                            <input type="password" name="retype_password" id="retype_password" class="input" placeholder="Retype Password">
                            <button type="button" class="btn save_btn">Save</button>
                            <div class="container-fluid form_links_parent">
                                <div class="row center">
                                    <div class="col-12 center">
                                        <a href="#" class="black links form_links normal_login">Normal Login</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--                      /change password form                          -->
                        <div class="container">
                                <div class="row justify-content-center">
                                    <div class="center" id="error_display">
                                       
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
           </div>
        </div>
                
        <!--        Scripts        -->
        <script src="<?php echo HOST; ?>/assets/js/jquery-3.3.1.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/bootstrap.min.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/aos.js"></script>
        
        <!--        Custom Scripts       -->
        <script src="<?php echo HOST; ?>/assets/js/init.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/main.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/student_login.js"></script>
    <!--        <script src="assets/js/main.js"></script>-->
    </body>
</html>