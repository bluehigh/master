<?php
# Project init file 
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';
?>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
        <!--        Bootstrap css        -->
        <link href="<?php echo HOST; ?>/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="<?php echo HOST; ?>/assets/css/bootstrap-grid.css" rel="stylesheet" type="text/css">
        
         <!--            aos            -->
        <link href="<?php echo HOST; ?>/assets/css/aos.css" rel="stylesheet">
        
        <!--        `Custom css        -->
        <link href="<?php echo HOST; ?>/assets/css/global.css" rel="stylesheet" type="text/css">
        <link href="<?php echo HOST; ?>/assets/css/main.css" rel="stylesheet" type="text/css">
        <link href="<?php echo HOST; ?>/assets/css/student_login_parked.css" rel="stylesheet" type="text/css">
    </head>
    
    <body>
        <div id="preloader_parent">
              <div id="preloader-content">
                  <img src="<?php echo HOST; ?>/assets/images/preloader.gif">
              </div>
        </div>
        <div class="container-fluid" id="landing">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6" id="main_logo">
                            <img src="<?php echo HOST; ?>/assets/images/scholarly_logo_student_panel.png" class="img-fluid">
                        </div>
                    </div>
                    <div class="row justify-content-center" id="landing_design" data-aos="slide-up" data-aos-duration="1000">
                        <div class="col-md-7">
                            <img src="<?php echo HOST; ?>/assets/images/landing_design_1.png" class="img-fluid">
                        </div>
                    </div>
                    <div class="row justify-content-center" id="landing_design_button_parent" data-aos="slide-up" data-aos-duration="1000">
                        <div class="col-md-9">
                            <button type="button" class="btn" id="notice_view_btn">E-Notice Board</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" data-aos="zoom-in" data-aos-delay="1400">
                    <!--                Form Section                  -->
                    <div id="form_parent">
                        <div class="row justify-content-center">
                            <div class="col-md-6 center">
                                <img src="<?php echo HOST; ?>/assets/images/student_login_logo.png" class="img-fluid">
                            </div>
                            <div class="col-12 center">
                                <h6 class="college_name form_college_name">Bengal Institute of Technology</h6>
                            </div>
                            <!--                      login form                          -->
                            <div class="col-11" id="login_form_parent">
                                <input type="text" name="college_id_login" id="college_id_login" class="input" placeholder="College ID">
                                <input type="password" name="password_login" id="password_login" class="input" placeholder="Password">
                                <button type="button" class="btn" id="login_btn">Login</button>
                                <div class="container-fluid form_links_parent">
                                    <div class="row center">
                                        <div class="col-6 center">
                                            <a href="#" class="red links form_links otp_received">OTP Recieved?</a>
                                        </div>
                                        <div class="col-6 center">
                                            <a href="#" class="red links form_links reset_link">Reset Password</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--                      /login form                          -->
                            <!--                      reset form                          -->
                            <div class="col-11" id="reset_form_parent">
                                <input type="text" name="college_id_reset" id="college_id_reset" class="input" placeholder="College ID">
                                <input type="text" name="email_reset" id="email_reset" class="input" placeholder="Registered Email ID">
                                <button type="button" class="btn login_btn" id="reset_btn">Send OTP</button>
                                <div class="container-fluid form_links_parent">
                                    <div class="row center">
                                        <div class="col-6 center">
                                            <a href="#" class="red links form_links otp_received">OTP Recieved?</a>
                                        </div>
                                        <div class="col-6 center">
                                            <a href="#" class="red links form_links normal_login">Normal Login</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--                      /reset form                          -->
                            <!--                      otp form                          -->
                            <div class="col-11" id="otp_form_parent">
                                <input type="text" name="college_id_otp" id="college_id_otp" class="input" placeholder="College ID">
                                <input type="password" name="otp" id="otp" class="input" placeholder="Enter OTP Here">
                                <button type="button" class="btn" id="procced_btn">Procced</button>
                                <div class="container-fluid form_links_parent">
                                    <div class="row center">
                                        <div class="col-6 center">
                                            <a href="#" class="red links form_links normal_login">Normal Login</a>
                                        </div>
                                        <div class="col-6 center">
                                            <a href="#" class="red links form_links reset_link">Reset Password</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--                      /otp form                          -->
                            <!--                      change passwprd form                          -->
                            <div class="col-11" id="change_password_form_parent">
                                <input type="password" name="new_password" id="new_password" class="input" placeholder="New Password">
                                <input type="password" name="retype_password" id="retype_password" class="input" placeholder="Retype Password">
                                <button type="button" class="btn save_btn" id="save_btn">Save</button>
                                <div class="container-fluid form_links_parent">
                                    <div class="row center">
                                        <div class="col-12 center">
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--                      /change password form                          -->
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="center" id="error_display">
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--                /Form Section                  -->
                </div>
            </div>
        </div>
        
        <!--        Scripts        -->
        <script src="<?php echo HOST; ?>/assets/js/jquery-3.3.1.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/bootstrap.min.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/aos.js"></script>
        
        <!--        Custom Scripts       -->
        <script src="<?php echo HOST; ?>/assets/js/main.js"></script>
        <script src="<?php echo HOST; ?>/assets/js/student_login.js"></script>
    </body>
</html>