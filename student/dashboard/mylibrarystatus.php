<?php
# Project init file
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

if (!sessionSetFor('student')) {
    session_destroy();
    header("Location: ".HOST."/student/login.php");
}

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Student;
use Scholarly\Teacher;
use Scholarly\Department;
use Scholarly\Course;
use Scholarly\Batch;
use Scholarly\Semester;
use Scholarly\Routine;
use Scholarly\Subject;

$db = new Handler();
Department::setDb($db);
Semester::setDb($db);
Course::setDb($db);
Batch::setDb($db);
Routine::setDb($db);
Subject::setDb($db);

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$student = new Student($_SESSION['student_id']);
$student->setDb($db);
$student->fetchDetails();

$student_batch_id = $student->getBatchId();

$current_batch_id = false;




?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>View Routine </title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <style type="text/css">
      .clearall {
        background: red!important;
        color: white!important;
        border: 1px solid red!important; 
      }
    </style>
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
    </script>
    <script type="text/javascript"></script>
    
    <script src="<?php echo HOST."/assets/js/custom_time.js";?>"></script>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>
        <div class="right_col" role="main">
            <div class="">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel" >
                      <div class="x_title">
                        <h2>Schedule</h2>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <div>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12">
                                    <div class="">
                                          <div class="x_panel">
                                            
                                            <div class="x_content">

                                             

                                              <div class="col-md-12">
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                 <table id="BookTransactionTable" class="display" style="width:100%">
                                                    <thead>
                                                      <tr>
                                                        <th>Book Name</th>
                                                        <th>Issue Date</th>
                                                        <th>Expected Return Date</th>
                                                      </tr>
                                                    </thead>
                                                  </table>

                                              </div>

                                              <div class="clearfix"></div>

                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>            
            </div>
        </div>

      </div>
    </div>
    
    <?php require_once('components/scripts.php'); ?>
    <script type="text/javascript">
        $(document).ready(function() {
           table = $('#BookTransactionTable').DataTable( {
                 dom: "Bfrtip",
                 ajax: host+'/app/api/student/my_books_data.php?action=view',
                 responsive: true,
                 "columns": [
                   {"data" : "bookname"},
                   {"data" : "issue_date"},
                   {"data" : "expected_return_date"},
                   
                 ],
                 buttons: []
              });

        } );

    </script>
  </body>
</html>