<?php
# Project init file
require $_SERVER['DOCUMENT_ROOT'].'/app/init.php';

session_start();

if (!sessionSetFor('student')) {
    session_destroy();
    header("Location: ".HOST."/student/login.php");
}

use Debojyoti\PdoConnect\Handler;
use Scholarly\Institute;
use Scholarly\Student;
use Scholarly\Teacher;
use Scholarly\Department;
use Scholarly\Course;
use Scholarly\Batch;
use Scholarly\Semester;
use Scholarly\Routine;
use Scholarly\Subject;

$db = new Handler();
Department::setDb($db);
Semester::setDb($db);
Course::setDb($db);
Batch::setDb($db);
Routine::setDb($db);
Subject::setDb($db);

$institute = new Institute($_SESSION['unique_id']);
$institute->setDb($db);
$institute->fetchDetails();

$student = new Student($_SESSION['student_id']);
$student->setDb($db);
$student->fetchDetails();

$student_batch_id = $student->getBatchId();

$current_batch_id = false;

$fetched_dept_ids = $institute->getDeptIds();

if (count($fetched_dept_ids)) {
  foreach ($fetched_dept_ids as $dept_id) {
    $dept = new Department($dept_id);
    $dept->fetchDetails();
    $fetched_course_ids = $dept->getCourseIds();

    if (count($fetched_course_ids)) {
      foreach ($fetched_course_ids as $course_id) {
        $course = new Course($course_id);
        $course->fetchDetails();

        $fetched_batch_ids = $course->getBatchIds();
        if (count($fetched_batch_ids)) {
          foreach ($fetched_batch_ids as $batch_id) {
            if ($batch_id == $student_batch_id) {
              $current_dept = $dept;
              $current_course = $course;
              $current_batch_id = $batch_id;
              break 3;
            }
          }
        }
      }
    }
  }
}

// Calculate current semester
$batch = new Batch($current_batch_id);
$batch->fetchDetails();
$current_sem_id = $current_course->getCurrentSemFor($batch);

$sem = new Semester($current_sem_id);
$sem->fetchDetails();

$sub_ids = $sem->getSubIds();

$sub_code_array = [];
$sub_name_array = [];

if (count($sub_ids)) {
  foreach ($sub_ids as $sub_id) {
    $sub = new Subject($sub_id);
    $sub->fetchDetails();
    $sub_name_array[$sub_id] = $sub->sub_name;
    $sub_code_array[$sub_id] = $sub->sub_code;
  }
}

$teacher_ids = $current_course->getAllEmpIds();

$teacher_name_array = [];

if (count($teacher_ids)) {
  foreach ($teacher_ids as $teacher_id) {
    $teacher = new Teacher($teacher_id);
    $teacher->setDb($db);
    $teacher->fetchDetails();
    $teacher_name_array[$teacher->empid] = $teacher->name;
  }
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>View Routine </title>

    <!-- General css files -->
    <?php require_once('components/styles.php'); ?>
    <!-- custom css -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <style type="text/css">
      .clearall {
        background: red!important;
        color: white!important;
        border: 1px solid red!important; 
      }
    </style>
    <script type="text/javascript">
        var unique_id = <?php echo $_SESSION['unique_id']; ?>;
    </script>
    <script type="text/javascript"></script>
    
    <script src="<?php echo HOST."/assets/js/custom_time.js";?>"></script>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php require_once('components/sidebar.php'); ?>
        <div class="right_col" role="main">
            <div class="">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel" >
                      <div class="x_title">
                        <h2>Schedule</h2>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <div>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12">
                                    <div class="">
                                          <div class="x_panel">
                                            
                                            <div class="x_content">

                                              <div class="col-md-2 col-sm-12">
                                                <!-- required for floating -->
                                                <!-- Nav tabs -->
                                                <ul class="nav nav-tabs tabs-left">
                                                  <li class="active"><a href="#monday" data-toggle="tab">Monday</a>
                                                  </li>
                                                  <li><a href="#tuesday" data-toggle="tab">Tuesday</a>
                                                  </li>
                                                  <li><a href="#wednesday" data-toggle="tab">Wednesday</a>
                                                  </li>
                                                  <li><a href="#thursday" data-toggle="tab">Thursday</a>
                                                  </li>
                                                  <li><a href="#friday" data-toggle="tab">Friday</a>
                                                  </li>
                                                  <li><a href="#saturday" data-toggle="tab">Saturday</a>
                                                  </li>
                                                </ul>
                                              </div>

                                              <div class="col-md-10">
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                  <div class="tab-pane active" id="monday">
                                                    <p class="lead">Monday</p>
                                                    <div>
                                                      <table id="mondayTable" class="display" style="width:100%">
                                                        <thead>
                                                          <tr>
                                                            <th>Starts</th>
                                                            <th>Ends</th>
                                                            <th>Subject Code</th>
                                                            <th>Subject Name</th>
                                                            <th>Assigned Teacher</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <?php
                                                            $routine = new Routine();
                                                            $routine->setDay(1);
                                                            $routine->setSem($sem);
                                                            $all_periods = $routine->fetchDetails(); 
                                                            if (count($all_periods)) {
                                                              foreach ($all_periods as $period) {
                                                              ?>
                                                                <tr>
                                                                  <td><script type="text/javascript">document.write(convertToTime(<?php echo $period['s_time']; ?>));</script></td>
                                                                  <td><script type="text/javascript">document.write(convertToTime(<?php echo $period['e_time']; ?>));</script></td>
                                                                  <td><?php echo $sub_code_array[$period['sub_id']];?></td>
                                                                  <td><?php echo $sub_name_array[$period['sub_id']];?></td>
                                                                  <td><?php echo $teacher_name_array[$period['empid']];?></td>
                                                                </tr>
                                                              <?php
                                                              }
                                                            }
                                                            unset($all_periods);
                                                          ?>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                  </div>
                                                  <div class="tab-pane" id="tuesday">
                                                    <p class="lead">Tuesday</p>
                                                    <div>
                                                      <table id="tuesdayTable" class="display" style="width:100%">
                                                        <thead>
                                                          <tr>
                                                            <th>Starts</th>
                                                            <th>Ends</th>
                                                            <th>Subject Code</th>
                                                            <th>Subject Name</th>
                                                            <th>Assigned Teacher</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <?php
                                                            $routine = new Routine();
                                                            $routine->setDay(2);
                                                            $routine->setSem($sem);
                                                            $all_periods = $routine->fetchDetails(); 
                                                            if (count($all_periods)) {
                                                              foreach ($all_periods as $period) {
                                                              ?>
                                                                <tr>
                                                                  <td><script type="text/javascript">document.write(convertToTime(<?php echo $period['s_time']; ?>));</script></td>
                                                                  <td><script type="text/javascript">document.write(convertToTime(<?php echo $period['e_time']; ?>));</script></td>
                                                                  <td><?php echo $sub_code_array[$period['sub_id']];?></td>
                                                                  <td><?php echo $sub_name_array[$period['sub_id']];?></td>
                                                                  <td><?php echo $teacher_name_array[$period['empid']];?></td>
                                                                </tr>
                                                              <?php
                                                              }
                                                            }
                                                            unset($all_periods);
                                                          ?>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                  </div>
                                                  <div class="tab-pane" id="wednesday">
                                                    <p class="lead">Wednesday</p>
                                                    <div>
                                                      <table id="wednesdayTable" class="display" style="width:100%">
                                                        <thead>
                                                          <tr>
                                                            <th>Starts</th>
                                                            <th>Ends</th>
                                                            <th>Subject Code</th>
                                                            <th>Subject Name</th>
                                                            <th>Assigned Teacher</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <?php
                                                            $routine = new Routine();
                                                            $routine->setDay(3);
                                                            $routine->setSem($sem);
                                                            $all_periods = $routine->fetchDetails(); 
                                                            if (count($all_periods)) {
                                                              foreach ($all_periods as $period) {
                                                              ?>
                                                                <tr>
                                                                  <td><script type="text/javascript">document.write(convertToTime(<?php echo $period['s_time']; ?>));</script></td>
                                                                  <td><script type="text/javascript">document.write(convertToTime(<?php echo $period['e_time']; ?>));</script></td>
                                                                  <td><?php echo $sub_code_array[$period['sub_id']];?></td>
                                                                  <td><?php echo $sub_name_array[$period['sub_id']];?></td>
                                                                  <td><?php echo $teacher_name_array[$period['empid']];?></td>
                                                                </tr>
                                                              <?php
                                                              }
                                                            }
                                                            unset($all_periods);
                                                          ?>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                  </div>
                                                  <div class="tab-pane" id="thursday">
                                                    <p class="lead">Thursday</p>
                                                    <div>
                                                      <table id="thursdayTable" class="display" style="width:100%">
                                                        <thead>
                                                          <tr>
                                                            <th>Starts</th>
                                                            <th>Ends</th>
                                                            <th>Subject Code</th>
                                                            <th>Subject Name</th>
                                                            <th>Assigned Teacher</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <?php
                                                            $routine = new Routine();
                                                            $routine->setDay(4);
                                                            $routine->setSem($sem);
                                                            $all_periods = $routine->fetchDetails(); 
                                                            if (count($all_periods)) {
                                                              foreach ($all_periods as $period) {
                                                              ?>
                                                                <tr>
                                                                  <td><script type="text/javascript">document.write(convertToTime(<?php echo $period['s_time']; ?>));</script></td>
                                                                  <td><script type="text/javascript">document.write(convertToTime(<?php echo $period['e_time']; ?>));</script></td>
                                                                  <td><?php echo $sub_code_array[$period['sub_id']];?></td>
                                                                  <td><?php echo $sub_name_array[$period['sub_id']];?></td>
                                                                  <td><?php echo $teacher_name_array[$period['empid']];?></td>
                                                                </tr>
                                                              <?php
                                                              }
                                                            }
                                                            unset($all_periods);
                                                          ?>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                  </div>
                                                  <div class="tab-pane" id="friday">
                                                    <p class="lead">Friday</p>
                                                    <div>
                                                      <table id="fridayTable" class="display" style="width:100%">
                                                        <thead>
                                                          <tr>
                                                            <th>Starts</th>
                                                            <th>Ends</th>
                                                            <th>Subject Code</th>
                                                            <th>Subject Name</th>
                                                            <th>Assigned Teacher</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <?php
                                                            $routine = new Routine();
                                                            $routine->setDay(5);
                                                            $routine->setSem($sem);
                                                            $all_periods = $routine->fetchDetails(); 
                                                            if (count($all_periods)) {
                                                              foreach ($all_periods as $period) {
                                                              ?>
                                                                <tr>
                                                                  <td><script type="text/javascript">document.write(convertToTime(<?php echo $period['s_time']; ?>));</script></td>
                                                                  <td><script type="text/javascript">document.write(convertToTime(<?php echo $period['e_time']; ?>));</script></td>
                                                                  <td><?php echo $sub_code_array[$period['sub_id']];?></td>
                                                                  <td><?php echo $sub_name_array[$period['sub_id']];?></td>
                                                                  <td><?php echo $teacher_name_array[$period['empid']];?></td>
                                                                </tr>
                                                              <?php
                                                              }
                                                            }
                                                            unset($all_periods);
                                                          ?>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                  </div>
                                                  <div class="tab-pane" id="saturday">
                                                    <p class="lead">Saturday</p>
                                                    <div>
                                                      <table id="saturdayTable" class="display" style="width:100%">
                                                        <thead>
                                                          <tr>
                                                            <th>Starts</th>
                                                            <th>Ends</th>
                                                            <th>Subject Code</th>
                                                            <th>Subject Name</th>
                                                            <th>Assigned Teacher</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <?php
                                                            $routine = new Routine();
                                                            $routine->setDay(6);
                                                            $routine->setSem($sem);
                                                            $all_periods = $routine->fetchDetails(); 
                                                            if (count($all_periods)) {
                                                              foreach ($all_periods as $period) {
                                                              ?>
                                                                <tr>
                                                                  <td><script type="text/javascript">document.write(convertToTime(<?php echo $period['s_time']; ?>));</script></td>
                                                                  <td><script type="text/javascript">document.write(convertToTime(<?php echo $period['e_time']; ?>));</script></td>
                                                                  <td><?php echo $sub_code_array[$period['sub_id']];?></td>
                                                                  <td><?php echo $sub_name_array[$period['sub_id']];?></td>
                                                                  <td><?php echo $teacher_name_array[$period['empid']];?></td>
                                                                </tr>
                                                              <?php
                                                              }
                                                            }
                                                            unset($all_periods);
                                                          ?>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>

                                              <div class="clearfix"></div>

                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>            
            </div>
        </div>

      </div>
    </div>
    
    <?php require_once('components/scripts.php'); ?>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#mondayTable').DataTable();
            $('#tuesdayTable').DataTable();
            $('#wednesdayTable').DataTable();
            $('#thursdayTable').DataTable();
            $('#fridayTable').DataTable();
            $('#saturdayTable').DataTable();
        } );

    </script>
  </body>
</html>