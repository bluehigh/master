  /*

    Javascript file for student login

*/
var host = window.location.protocol+'//'+window.location.host;

//  Hide all other forms at first load
$('#otp_form_parent').hide();
$('#reset_form_parent').hide();
$('#change_password_form_parent').hide();

/*  General function to handle form toggle  */
function formToggle(show_form) {
    $('#otp_form_parent').hide();
    $('#reset_form_parent').hide();
    $('#change_password_form_parent').hide();
     $('#login_form_parent').hide();
    $('#'+show_form).show();
}

//  If otp_received link is clicked
$('.otp_received').click(function() {
    errorToggle(0);
    formToggle('otp_form_parent');
});

//  If normal_login link is clicked
$('.normal_login').click(function() {
    errorToggle(0);
    formToggle('login_form_parent');
});

//  If normal_login link is clicked
$('.reset_link').click(function() {
    errorToggle(0);
    formToggle('reset_form_parent');
});

//  Function to display error (toggle=0/1) 1=display
function errorToggle(toggle,msg=null) {
    if(toggle) {
        $("#error_display").show();
        $("#error_display").html(msg);
    } else {
         $("#error_display").hide();
    }
}


/*  Handle Ajax requests */
$("#login_btn").click(function() {
  var empid_login = $('#employee_id_login').val();
  var password_login = $('#password_login').val();

  if(password_login == '' || empid_login == '')
  {
    errorToggle(1,"Fields cannot be empty");
  }
   else if (password_login.length<8) {
     errorToggle(1,"Minimum password length is 8");
    }
   else {

     //var params = {'login':'login','employee_id':empid_login,'password':password_login};
     var params = {'login':'login',
                    'empid':empid_login,
                    'password':password_login,
                    'collegeUniqueID':'121'
     };
     $.ajax({
       url: host+'/app/api/employee/emp_activity.php',
       type:  'post',
       data: params,
       dataType:  'json',
       error: function(data){
         errorToggle(1,data.responseText);
         console.log(data);
       },
       success: function(data){
         console.log(data);
         if(data.auth!=true){
           errorToggle(1,"Incorrect Username or password");
         }else{
           console.log('auth success');
           window.location = host+"/employee/emp_route.php";
         }
       }
     });
   }
   //errorToggle(1,"Fill all the fields");
});


/* Handle reset password Ajax starts */
$("#reset_btn").click(function () {
  var college_id_reset = $("#college_id_reset").val();
  var email_to_reset = $("#email_reset").val();

  var email_pattern_check = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i

  if (!($.isNumeric(college_id_reset))) {
    errorToggle(1,"Login ID || NUMERIC ONLY");
  }
  else if (!email_pattern_check.test(email_to_reset)) {
    errorToggle(1,"Enter a valid Email");
  }
  else {
    errorToggle(0);
    var params = {'reset':'reset',
                  'college_id':college_id_reset,
                  'email':email_to_reset,
                  'collegeUniqueID':'121'
    };

    $.ajax({
      url:  'app/api/student_login_api.php',
      method: 'post',
      dataType: 'json',
      data: params,
      error: function(data){
        console.log(data.responseText);
        errorToggle(1,data.responseText);
      },
      success: function (data) {
        console.log(data);

      }
    });
  }

})
/* Handle reset password Ajax ends */
