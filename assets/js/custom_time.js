function convertToSeconds(time) {
  // Expecting format 10:00
  hour = time.split(':')[0];
  mins = time.split(':')[1];
  seconds = (hour*3600)+(mins*60);
  return seconds;
}

function convertToTime(seconds) {
  hour = ((seconds/3600).toString()).split('.')[0];
  mins = (seconds/3600);
  mins = (mins - hour);
  mins = Math.round(mins*60);
  if (hour.length == 1) {
    hour = "0"+hour;
  }
  if (mins.toString().length == 1) {
    mins = "0"+mins;
  }
  return hour+":"+mins;
}