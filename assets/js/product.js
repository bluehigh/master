$('#search_institute').keyup(function() {
	search_for = $(this).val();
	
	if (search_for != '' && search_for.length >= 3) {
		$.ajax({
			url: host+"/app/api/institute_search.php",
			method: "GET",
			data: {
				"search_for": search_for
			},
			success:function(data) {
				// console.log(JSON.parse(data));
				result = JSON.parse(data);
				len = result.length;
				// console.log(len);
				$("#search_institute_result").html('');
				$.each(result, function(index, value ) {
					institute = '<a href="'+host+'/institute/index.php?unique_id='+value['unique_id']+'"><div class="result">'+value['name']+'</div></a>';
					$("#search_institute_result").append(institute);	
				});
			}
		});
	} else {
		$("#search_institute_result").html('');
	}
});

disp_msgs = [
				'Search Institutes . . .',
				'Try by Name, Code, Id',
				'Even University'
			]
len_of_msgs = disp_msgs.length;
counter = 1;
setInterval(function() {
	if (counter>=len_of_msgs) {
		counter = 0;
	}
	msg = disp_msgs[counter++];
	$('#search_institute').attr('placeholder',msg);	
} ,3000);

