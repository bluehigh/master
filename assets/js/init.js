var host = window.location.protocol+'//'+window.location.host; 

function validateEditorFields(editor) {
  fields = editor.fields();
  for (i = 0; i < fields.length; i++ ) {
    field = editor.field(fields[i]);
    if (field.val().length < 1) {
      field.error( 'Field cannot be empty' );
      return false;
    }
  }
}
